function absPathFTLib  = importFTLib(pathFTLib)

%% INFO 
% Adds all basic FTLib folders to the Matlab "path" and generats the global
% variable "g_FTLib_path" containing the absolute path to the FTLib folder.
% If no input is given it is assumed, that the FTLib folder is in the
% same directory as pwd.
% The input can alse be the path to a folder containing the folder FTLib.

%% IN
% pathFTLib = pwd - char array/ string - path to FTLib folder; can be the FTLib folder or one directory above

%% OUT
% absPathFTLib - str - absolut path to the FTLib folder

% AUTHOR: JL 2022

%% EX
% importFTLib() - load FTLib from pwd
% importFTLib('<path>') - load FTLib from given path
% importFTLib('<path>\FTLib') - same a above
% importFTLib('<path>/FTLib') - same a above
% importFTLib('<path>\\FTLib') - same a above
% importFTLib('<path>//FTLib') - same a above
    

    % set default values, when not given
    if nargin < 1, pathFTLib = pwd; end

    fprintf('%s IMPORT FTLib from %s\n', datestr(now, 'HH:MM:SS'), pathFTLib);
    
    assert(isfolder(pathFTLib), "ASSERT. Given path is no folder.")

    global g_FTLib_path; % TODO: better solution than global ?
    
    %% find FTLib folder
    pathFTLib = replace(pathFTLib,{'//', '\\', '/', '\'}, filesep);
    pathparts = strsplit(pathFTLib,filesep);
    if pathparts(end) == "FTLib"
        dirListing = dir(pathFTLib); 
        pathFTLib = dirListing(1).folder;
    else
        %% find FTLib folder in given folder
        dirListing = dir(pathFTLib); 
        dirListing = dirListing([dirListing.isdir]);
        dirListing = dirListing(cellfun(@(x)isequal(x,"FTLib"), {dirListing.name}));
        
        assert(numel(dirListing) > 0, 'ASSERT. No FTlib folder found.');
        assert(numel(dirListing) == 1, 'ASSERT. Multiple FTlib folders found.');
    
        absPathFTLib = [dirListing.folder, '\', dirListing.name];
    end

    disp(absPathFTLib);

    g_FTLib_path = absPathFTLib;

    %% add all basic FTLib folders to path
    path([absPathFTLib], path); 
    path([absPathFTLib, '\plotting'], path); 
    path([absPathFTLib, '\dt_funcs'], path); 
    path([absPathFTLib, '\PS_Bunch_gen'], path); 
    path([absPathFTLib, '\general_funcs'], path); 
    path([absPathFTLib, '\field_win_funcs'], path); 
    path([absPathFTLib, '\field_funcs'], path); 
    path([absPathFTLib, '\runs_and_checks'], path); 
end
