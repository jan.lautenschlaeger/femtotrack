% restoredefaultpath 
clc  % clean up cmd
clear  % clean up vars
close all  % clean up figs

absFTLibPath = importFTLib(pwd);
path([absFTLibPath, '\PS_Bunch_gen\Anna'], path); 
path([absFTLibPath, '\plotting\oldPlots'], path); 
inDir = [pwd '\in'];
outDir = [pwd '\out'];

%% FemtoTrack V4 %%
run_time = tic;

%% ======== SETTINGS ======================================================  % SC1_-_w_orig_params_-_on_opt_axis
% - - - - - PARTICLES - - - - -
N_p = 23;  % e- per bunch
N_b = 1000;  % number of bunches (bunches do not interact with each other by definition)

bunchInfo = genEqualParticleNumberDist(N_p, N_b);
phasespace = genPhasespaceOnNanotip([outDir '\particles'], N_p * N_b);

% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = false;
genSettings.interactionType = 'BunchRel';  % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -
scaling_fac = 1;
scaling = [scaling_fac, scaling_fac, scaling_fac];
field_emitter_1 = importHDF5_CST([inDir '\emitter_1.h5'], 'StaticE', 'emitter_1', [0, 0, 0], scaling, 0, 0, 10^-6);
field_emitter_2 = importHDF5_CST([inDir '\emitter_2.h5'], 'StaticE', 'emitter_2', [0, 0, 0], scaling, 0, 0, 10^-6);
field_emitter_2 = addLocalFieldOverwrite(field_emitter_1, field_emitter_2); 
field_emitter_3 = importHDF5_CST([inDir '\emitter_3.h5'], 'StaticE', 'emitter_3', [0, 0, 0], scaling, 0, 0, 10^-6);
field_emitter_3 = addLocalFieldOverwrite(field_emitter_2, field_emitter_3); 
field_injector_1 = importHDF5_CST([inDir '\injector_1.h5'], 'StaticE', 'injector_1', [0, 0, 0], scaling, 0, 0, 10^-3);
field_injector_1 = addLocalFieldOverwrite(field_emitter_3, field_injector_1); 
field_injector_2 = importHDF5_CST([inDir '\injector_2.h5'], 'StaticE', 'injector_2', [0, 0, 0], scaling, 0, 0, 10^-3);
field_injector_2 = addLocalFieldOverwrite(field_injector_1, field_injector_2); 
field_injector_3 = importHDF5_CST([inDir '\injector_3.h5'], 'StaticE', 'injector_3', [0, 0, 0], scaling, 0, 0, 10^-3);
field_injector_3 = addLocalFieldOverwrite(field_injector_2, field_injector_3);

fieldList = {field_emitter_1, field_emitter_2, field_emitter_3, field_injector_1, field_injector_2, field_injector_3};

genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TBT

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = false; 
genSettings.killBoxList = {};
genSettings.visBoxList = {};

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';  % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
window.margin = [2, 2; 2, 2; 2, 2] / 2;  % in indices (), min 1 in each
window.maxWindowSize = [inf, inf, inf];  % TBT

% - - - - - PLOTTING - - - - - 
plotOptions.plotOverTime = false;
plotOptions.energyIneV = true;

% - - - - - TIME SETTINGS - - - - -
timeInfo.dt = 1e-13;  % with adaptive time steps, this value is used outside of any field mesh
timeInfo.Nt = 6500;
genSettings.dt_update_res = 1;  % resolution of time step updates (give step width), applies to adaptive time steps

genSettings.showInfo = false;
genSettings.saveEverNStep = 10;
genSettings.solver = 'General_CInterpo';  %'General', 'General_CInterpo', 'HPC'

% ========================================================================= 
% Advanced Settings - better do not touch, when new

% time settings
% timeInfo.dtFunc = @ (phasespace, windowedField, dtVarargin) adaptiveTimeSteps(phasespace, windowedField, dtVarargin, 0.1); % adaptiveTimeSteps or equalTimeSteps;  % for General
timeInfo.dtFunc = @ (varargin) adaptiveTimeSteps_CFL(0.1, varargin); % adaptiveTimeSteps or equalTimeSteps;  % for General
% timeInfo.dtFunc = @equalTimeSteps;
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
plotOptions.visBoxList = genSettings.visBoxList;
plotOptions.killBoxList = genSettings.killBoxList;

plotOptions.solver = genSettings.solver;
if genSettings.doInteraction
    plotOptions.eeInter = genSettings.interactionType;
else
    plotOptions.eeInter = 'noInteraction';
end
plotOptions.modeWin = window.mode;

%% CHECK & FIX
window.updateRate = abs(max(1, fix(window.updateRate)));
genSettings.dt_update_res = abs(max(1, fix(genSettings.dt_update_res)));
pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
if pass, fprintf('INFO. Checks all passed.\n'); end

%% SOLVER
switch genSettings.solver
    case 'General'
        [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'General_CInterpo'
        [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC'
        [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC_CInterpo'
        [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'None'
        warning('Solver Skipped.')
    otherwise
        error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
end
fprintf('INFO. run time: %g sec\n', toc(run_time));

%% PLOTTING
% close all
plotDebugTimes(debugTimes, plotOptions, 1);
% plotParticleDebug(simState, plotOptions, 2);
% plotTrajectories(simState, fieldList, plotOptions, 3);
plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
plotBeamProps(simState, 5);
% plotGroupDist(bunchInfo, plotOptions, 6);
% plotKinEnergy(simState, plotOptions, 7);
plotDt(simState, plotOptions, 8);

fprintf('INFO. run time: %g sec\n', toc(run_time));
disp('FINISHED');