function lowField = addLocalFieldOverwrite(highField, lowField, replaceList, transferAllBoxes)

%% INFO
% This function sets up a relation between the two fields such that
% particles in inside the higher field ignor the lower field.
% This is down by addin the field ignor box to the lower field.

%     P1     P2    P3
%   --F1--
% --......---F3--==F2==---
%     ^ignorBox      ^overlapping fields

% Three particles p1,p2,p3. Three field f1,f2,f3 with f1 above f3 (cut out), 
% f2 and f3 on the same level, f3 is larger then f1 or f2.
% P1 sees only F1
% P2 sees only F3
% P3 sess F2 + F3

%% IN
% highField - struct -  dominant field 
% lowField - struct - suppressed field
% transferAllBoxes - boolean - if true transfer all irgorBoxes from the
%                                highField to lowField (needed?)

%% OUT
% lowField - struct - changed suppressed field

% TODO
% - check if field overlap, if not: do not add to ignoreBoxList

    % set default values, when not given
    if nargin < 3, replaceList = false; end
    if nargin < 4, transferAllBoxes = false; end

    if replaceList
        oldList = [];
    else
        oldList = lowField.ignoreBoxList;
    end

    if transferAllBoxes
        lowField.ignoreBoxList = [oldList, highField.meshBounds];
    else
        lowField.ignoreBoxList = [oldList, highField.ignoreBoxList, highField.meshBounds];
    end
end