function field = scaleFieldData(field, scalers)

%% INFO
% This function scles the field data of a given field. 

%% IN
% field - field struct - field which data to scale
% scalers - float (1,3) - scaling factor for x,y,z of the field data 

%% OUT
% field - field struct - field with scaled field data

% AUTHOR: JL 2022

    % field - field struct - to be modefies field
    % scalers - (3,1) - scaling for x,y,z of the data

    field.data.x = field.data.x * scalers(1);
    field.data.y = field.data.y * scalers(2);
    field.data.z = field.data.z * scalers(3);
end