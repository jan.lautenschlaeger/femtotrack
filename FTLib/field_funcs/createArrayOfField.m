function fieldList = createArrayOfField(field, numOfCopys, eachCopyTrans, useFieldSizeTrans)

%% INFO
% Creat a group of copys of a given fields with a given offset between succeeding copys.
% The offset is either given in absolut units (in m) or using the
% dimentions of the field.

%% IN
% field - struct - field to copy
% numOfCopys - int - number of copys, including the original
% eachCopyTrans - (3,1) float - translation of a copy relative to the previous
%                           field copy in m or in units of the field dimensions
% useFieldSizeTrans == false - boolean - if true use the field dimensions
%                                        as unit for translating succeeding
%                                        field copies

%% OUT
% fieldList - (numOfCopys,1) list{struct} - list of the array of fields

% AUTHOR: JL 2022

    % set default values, when not given
    if nargin < 4, useFieldSizeTrans = false; end

    %% Check input
    numOfCopys = abs(fix(numOfCopys)); 
    eachCopyTrans = eachCopyTrans(1:3);
    assert(islogical(useFieldSizeTrans), 'ASSERT. useFieldSizeTrans is not boolean.');
    assert(isFieldStruct(field), 'ASSERT. Given field input is no field struct.');
    
    fieldList = {};
       
    for ii = 0:(numOfCopys-1)
        offsetTrans = eachCopyTrans * ii;
        if useFieldSizeTrans
            offsetTrans = offsetTrans .* (field.meshBounds(:,2)-field.meshBounds(:,1));
        end
        fieldList = [fieldList, scaleTransFieldMesh(field, [1,1,1], offsetTrans)];
    end

end