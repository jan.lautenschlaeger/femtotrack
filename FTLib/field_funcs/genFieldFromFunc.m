function [field] = genFieldFromFunc(x, y, z, dataFunc, fieldType, trans, scalers, useMeshPos, frq, phaseShift)
    tic;
    %% INFO
    % Generates analytical field from a function handle
    
    % Generate the field struct by using a grid constructed by x,y,z and a
    % generator function.

    %% IN
    % x,y,z - (1,n{x,y,z}) floats - defines the grid spacing in x,y,z direction
    % dataFunc - FunctionHandle - generator funtion like rand(), ones(), zeros(),...
    % fieldType - FieldType - defines the type of field: staticE, ...
    % trans - (1,3) float - translation of the complet field in world space
    % scalers - (1,3) float - scaling of the field
    % useMeshPos - boolen - when true, use the x,y,z positons for the
    %                       generation instead of nx,ny,nz
    % frq - float - frequency of a dynamic field 
    % phaseShift - float - phaseshift of the dynamic field in rad (2π*rad=360°)
    
    %% OUT
    % field - struct - generated field struct

    %% INFO
    % field - struct - containg all the info a the field
    %    .mesh                  - x,y,z hex mesh grid node positions
    %       .{x,y,z}
    %    .data                  - x,y,z field strength over 3D grid (in Si Units)
    %       .{x,y,z}
    %    .{nx,ny,nz}            - x,y,z number of grid nodes
    %    .{meshX,meshY,meshZ}   - result of ndgrid() with mesh x,y,z
    %    .meanCellSize          - x,y,z mean grid spacing
    %    .meshBounds            - x,y,z min/max mesh limits
    %    .type                  - field type as string (from IN)
    %    .trans                 - x,y,z offset to origin (from IN)
    %    .frq                   - dynamic field frequnecy (from IN)
    %    .name                  - field name (from IN)
    %    .phaseShift            - field phaseshift (from IN)
    %    .ignoreBoxList         - list of boxes the field is ignored inside
    %    .meshIsEquiDist        - true if x,y,z mesh has equidistant spacing
    %       .{x,y,z}
    %    .meshIndexLUT          - acceleration structure field interpolation 
    %       .{x,y,z}
    %       .{xSpacing,ySpacing,zSpacing} - 

    %% SI UNITS
    % E-Field -> V/m
    % B-Field -> T
    % Frq -> Hz
    % Mesh spacing -> m

    % set default values, when not given
    if nargin < 6, trans = [0,0,0]; end
    if nargin < 7, scalers = [1,1,1]; end
    if nargin < 8, useMeshPos = false; end
    if nargin < 9, frq = 0; end
    if nargin < 10, phaseShift = 0; end

    field.nx = size(x,2);
    field.ny = size(y,2);
    field.nz = size(z,2);
    fprintf('INFO. [nx,ny,nz] = [%i,%i,%i]\n', field.nx, field.ny, field.nz);

    field.mesh.x = x + trans(1);
    field.mesh.y = y + trans(2);
    field.mesh.z = z + trans(3);
    % [field.meshX, field.meshY, field.meshZ] = meshgrid(field.mesh.x, field.mesh.y, field.mesh.z);
    [field.meshX, field.meshY, field.meshZ] = ndgrid(field.mesh.x, field.mesh.y, field.mesh.z);
    field.meanCellSize = [mean(diff(x)), mean(diff(y)), mean(diff(z))];

    field.meshBounds = [min(field.mesh.x), max(field.mesh.x); min(field.mesh.y), max(field.mesh.y); min(field.mesh.z), max(field.mesh.z)];
    
    %% generat the field data
    if useMeshPos
        [field.data.x,field.data.y,field.data.z] = dataFunc(field.meshX, field.meshY, field.meshZ);
        field.data.x = field.data.x * scalers(1);
        field.data.y = field.data.y * scalers(2);
        field.data.z = field.data.z * scalers(3);
    else
        field.data.x = dataFunc(field.nx, field.ny, field.nz) * scalers(1);
        field.data.y = dataFunc(field.nx, field.ny, field.nz) * scalers(2);
        field.data.z = dataFunc(field.nx, field.ny, field.nz) * scalers(3);
    end

    field.type = fieldType;
    field.trans = trans;
    field.frq = frq;
    field.name = 'AnalyticalField';
    field.phaseShift = phaseShift;
    field.ignoreBoxList = {};


    %% generate ADDITONAL PREPROCESS
    % TODO: equdistand mesh/ grid 
    eta = 0.001;
    % field.meshIsEquiDist = all(and(field.meanCellSize < ([diff(field.x)',diff(field.y)',diff(field.z)'] +eta), field.meanCellSize > ([diff(field.x)',diff(field.y)',diff(field.z)'] -eta)),1);

    field.meshIsEquiDist.x = all(and(field.meanCellSize(1) < (diff(field.mesh.x)' +field.meanCellSize(1)*eta), field.meanCellSize(1) > (diff(field.mesh.x)' -field.meanCellSize(1)*eta)),1);
    field.meshIsEquiDist.y = all(and(field.meanCellSize(2) < (diff(field.mesh.y)' +field.meanCellSize(1)*eta), field.meanCellSize(2) > (diff(field.mesh.y)' -field.meanCellSize(1)*eta)),1);
    field.meshIsEquiDist.z = all(and(field.meanCellSize(3) < (diff(field.mesh.z)' +field.meanCellSize(1)*eta), field.meanCellSize(3) > (diff(field.mesh.z)' -field.meanCellSize(1)*eta)),1);

    if field.meshIsEquiDist.x
        field.meshIndexLUT.xSpacing = field.meanCellSize(1);
    else
        field.meshIndexLUT.xSpacing = min(diff(field.mesh.x));
    end
    field.meshIndexLUT.x = field.mesh.x(1):field.meshIndexLUT.xSpacing:field.mesh.x(end);
    field.meshIndexLUT.x = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.x), field.meshIndexLUT.x);

    if field.meshIsEquiDist.y
        field.meshIndexLUT.ySpacing = field.meanCellSize(2);
    else
        field.meshIndexLUT.ySpacing = min(diff(field.mesh.y));
    end
    field.meshIndexLUT.y = field.mesh.y(1):field.meshIndexLUT.ySpacing:field.mesh.y(end);
    field.meshIndexLUT.y = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.y), field.meshIndexLUT.y);

    if field.meshIsEquiDist.z
        field.meshIndexLUT.zSpacing = field.meanCellSize(3);
    else
        field.meshIndexLUT.zSpacing = min(diff(field.mesh.z));
    end
    field.meshIndexLUT.z = field.mesh.z(1):field.meshIndexLUT.zSpacing:field.mesh.z(end);
    field.meshIndexLUT.z = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.z), field.meshIndexLUT.z);

    fprintf('INFO. Gen Field Time: %g sec\n', toc);
end

