function [field] = genZeroField(box)

%box: definition domain of the zero field [xmin,xmax, ymin,ymax, zmin,
%zmax]  in meter

genFieldFromFunc({[box(1,1),box(1,2)]...
                 [box(2,2),box(2,2)]...
                 [box(3,1),box(3,2)]}...
    , @zeros, 'staticE')



end