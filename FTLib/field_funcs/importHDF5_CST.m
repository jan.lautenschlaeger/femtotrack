function field = importHDF5_CST(filePath, fieldType, fieldName, trans, scalersField, frq, phaseShift, unitScalFac)
    tic;

    %% INFO
    % Import a three dim. vector field from a HDF5 (.h5) file 
    % currently this importer requires the file structure created by CST
    % supported are static E, D, B, H and potential, as well as dynamic E, D, B and H.

    % When importing a D or H field the field data will automatically be 
    % converted to a E or B field via eps0 or mu0. Any additional global 
    % material properties have to be done manually via the 'scalersField' input.

    %% IN 
    % fieldpath - char array - file path to the .h5 file (with extention)
    % fieldType - char array - combi of 'Static' or 'Dynamic' and 'E', 'B' or 'H'; Exp: 'StaticB', 'DynamicE'
    % fieldName - string - user defined name 
    % trans - float(1,3) - x,y,z position offset from the origin
    % scalersField - float(1,3) - x,y,z sacling of the field strength
    % frq - float - frequency of a dynamic field 
    % phaseShift - float - phaseshift of the dynamic field in rad (2π*rad=360°)
    % unitScalFac - float - mesh scaling factor; Exp: mm -> 10^-3, nm -> 10^-9

    %% OUT
    % field - struct - containg all the info a the field
    %    .mesh                  - x,y,z hex mesh grid node positions
    %       .{x,y,z}
    %    .data                  - x,y,z field strength over 3D grid (in Si Units)
    %       .{x,y,z}
    %    .{nx,ny,nz}            - x,y,z number of grid nodes
    %    .{meshX,meshY,meshZ}   - result of ndgrid() with mesh x,y,z
    %    .meanCellSize          - x,y,z mean grid spacing
    %    .meshBounds            - x,y,z min/max mesh limits
    %    .type                  - field type as string (from IN)
    %    .trans                 - x,y,z offset to origin (from IN)
    %    .frq                   - dynamic field frequnecy (from IN)
    %    .name                  - field name (from IN)
    %    .phaseShift            - field phaseshift (from IN)
    %    .ignoreBoxList         - list of boxes the field is ignored inside
    %    .meshIsEquiDist        - true if x,y,z mesh has equidistant spacing
    %       .{x,y,z}
    %    .meshIndexLUT          - acceleration structure field interpolation 
    %       .{x,y,z}
    %       .{xSpacing,ySpacing,zSpacing} - 


    % get h5 info
    info = h5info(filePath);
    dataNames = string({info.Datasets.Name});
    meshField = contains(dataNames, 'Mesh');

    % set default values, when not given
    if nargin < 3
        temp = split(filePath, {'\','/','//','\\'});
        fieldName = temp{end}; 
    end
    if nargin < 4, trans = [0,0,0]; end
    if nargin < 5, scalersField = [1,1,1]; end
    if nargin < 6, frq = 0; end
    if nargin < 7, phaseShift = 0; end
    if nargin < 8
        try
            unitScalFac = info.Datasets(find(meshField,1)).Attributes(2).Value;
            fprintf('INFO. Mesh Scale: %s\n', unitScalFac);
            unitScalFac = convPrefixToFactor(unitScalFac(1)); % TODO: problem 'm' == Meters, not milli (aka no prefix)
            fprintf('INFO. Mesh Scale Factor: %s\n', unitScalFac);
        catch e
            warning('WARNING. Failed to extract mesh scaling factor from H5 file. Set to 1.');
            warning(e);
            unitScalFac = 1;
        end
    end

    %% import field data
    dataName = getFieldDataName(fieldType);
    assert(dataName == dataNames(~meshField), 'ERROR. Field type mismatch');
    h5fieldData = h5read(filePath, join(['/', dataName]));
    
    %% import mesh data
    mesh.x = h5read(filePath,'/Mesh line x')';
    mesh.y = h5read(filePath,'/Mesh line y')';
    mesh.z = h5read(filePath,'/Mesh line z')'; 

    %% Convert D --> E and H --> B
    switch fieldType
        case {'StaticE', 'DynamicE','StaticB', 'DynamicB'}
            field.type = fieldType;
        case 'StaticH'
            field.type = 'StaticB';
            fieldType = 'StaticB';
            scalersField = scalersField .* Constants.mu0;
        case 'DynamicH'
            field.type = 'DynamicB';
            fieldType = 'DynamicB';
            scalersField = scalersField .* Constants.mu0;
        case 'StaticD'
            field.type = 'StaticE';
            fieldType = 'StaticE';
            scalersField = scalersField ./ Constants.eps0;
        case 'DynamicD'
            field.type = 'DynamicE';
            fieldType = 'DynamicE';
            scalersField = scalersField ./ Constants.eps0;
        otherwise
    end

    %% scale field and potential conversion
    switch fieldType
        case {'StaticE', 'StaticB', 'DynamicE', 'DynamicB'}
            try
                field.data.x = (h5fieldData.x.re + 1i * h5fieldData.x.im) * scalersField(1);
                field.data.y = (h5fieldData.y.re + 1i * h5fieldData.y.im) * scalersField(2);
                field.data.z = (h5fieldData.z.re + 1i * h5fieldData.z.im) * scalersField(3);
            catch
                field.data.x = h5fieldData.x * scalersField(1);
                field.data.y = h5fieldData.y * scalersField(2);
                field.data.z = h5fieldData.z * scalersField(3);
            end

            if (phaseShift ~= 0 && (fieldType == "DynamicE" || fieldType == "DynamicB"))
                field.data.x = applyGPhaseShift(field.data.x, phaseShift);
                field.data.y = applyGPhaseShift(field.data.y, phaseShift);
                field.data.z = applyGPhaseShift(field.data.z, phaseShift);
            end
        case 'StaticPot'
            eField = potential2EField(h5fieldData,mesh.x,mesh.y,mesh.z);            
            field.data.x = eField.x * scalersField(1);
            field.data.y = eField.y * scalersField(2);
            field.data.z = eField.z * scalersField(3);
            mesh.x = mesh.x(1:end-1);
            mesh.y = mesh.y(1:end-1);
            mesh.z = mesh.z(1:end-1);
        otherwise
            error('ERROR. Field type does not exist.')
    end

    %% gen additonal data
    field.nx = max(size(mesh.x));
    field.ny = max(size(mesh.y));
    field.nz = max(size(mesh.z));
    fprintf('INFO. [nx,ny,nz] = [%i,%i,%i]\n', field.nx, field.ny, field.nz);

    field.mesh.x = mesh.x * unitScalFac + trans(1);
    field.mesh.y = mesh.y * unitScalFac + trans(2);
    field.mesh.z = mesh.z * unitScalFac + trans(3);
    % [field.meshX, field.meshY, field.meshZ] = meshgrid(field.mesh.x, field.mesh.y, field.mesh.z); % needed for interp3
    [field.meshX, field.meshY, field.meshZ] = ndgrid(field.mesh.x, field.mesh.y, field.mesh.z);     % needed for griddedInterpolant 
    field.meanCellSize = [mean(diff(field.mesh.x)), mean(diff(field.mesh.y)), mean(diff(field.mesh.z))];

    field.meshBounds = [min(field.mesh.x), max(field.mesh.x); min(field.mesh.y), max(field.mesh.y); min(field.mesh.z), max(field.mesh.z)];
    
    field.trans = trans;
    field.frq = frq;
    field.name = fieldName;
    field.phaseShift = phaseShift;
    field.ignoreBoxList = {};

    %% ADDITONAL PREPROCESS
    % TODO: equidistant mesh/ grid 
    eta = 0.01;
    % field.meshIsEquiDist = all(and(field.meanCellSize < ([diff(field.x)',diff(field.y)',diff(field.z)'] +eta), field.meanCellSize > ([diff(field.x)',diff(field.y)',diff(field.z)'] -eta)),1);

    field.meshIsEquiDist.x = all(and(field.meanCellSize(1) < (diff(field.mesh.x)' +field.meanCellSize(1)*eta), field.meanCellSize(1) > (diff(field.mesh.x)' -field.meanCellSize(1)*eta)),1);
    field.meshIsEquiDist.y = all(and(field.meanCellSize(2) < (diff(field.mesh.y)' +field.meanCellSize(1)*eta), field.meanCellSize(2) > (diff(field.mesh.y)' -field.meanCellSize(1)*eta)),1);
    field.meshIsEquiDist.z = all(and(field.meanCellSize(3) < (diff(field.mesh.z)' +field.meanCellSize(1)*eta), field.meanCellSize(3) > (diff(field.mesh.z)' -field.meanCellSize(1)*eta)),1);

    if field.meshIsEquiDist.x
        field.meshIndexLUT.xSpacing = field.meanCellSize(1);
    else
        field.meshIndexLUT.xSpacing = min(diff(field.mesh.x));
    end
    field.meshIndexLUT.x = field.mesh.x(1):field.meshIndexLUT.xSpacing:field.mesh.x(end);
    field.meshIndexLUT.x = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.x), field.meshIndexLUT.x);

    if field.meshIsEquiDist.y
        field.meshIndexLUT.ySpacing = field.meanCellSize(2);
    else
        field.meshIndexLUT.ySpacing = min(diff(field.mesh.y));
    end
    field.meshIndexLUT.y = field.mesh.y(1):field.meshIndexLUT.ySpacing:field.mesh.y(end);
    field.meshIndexLUT.y = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.y), field.meshIndexLUT.y);

    if field.meshIsEquiDist.z
        field.meshIndexLUT.zSpacing = field.meanCellSize(3);
    else
        field.meshIndexLUT.zSpacing = min(diff(field.mesh.z));
    end
    field.meshIndexLUT.z = field.mesh.z(1):field.meshIndexLUT.zSpacing:field.mesh.z(end);
    field.meshIndexLUT.z = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.z), field.meshIndexLUT.z);


    fprintf('INFO. Import HDF5 Time: %g sec\n', toc);
end

function dataName = getFieldDataName(fieldType)
    switch fieldType
        case {'StaticE', 'DynamicE'}
            dataName = 'E-Field';
        case {'StaticB', 'DynamicB'}
            dataName = 'B-Field';
        case {'StaticH', 'DynamicH'}
            dataName = 'H-Field';
        otherwise
            
    end
end

function fieldData = applyGPhaseShift(fieldData, phaseShift)
    [theta,rho] = cart2pol(real(fieldData),imag(fieldData));
    [re,im] = pol2cart(theta+phaseShift,rho);
    fieldData = re + 1i*im;
end