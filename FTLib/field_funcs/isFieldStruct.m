function isField = isFieldStruct(field)
%% INFO
% Check if input is a valid field struct for femtoTrack, aka the struct has all
% necessary fields. 

%% IN
% field - struct - field to check

%% OUT
% isField - boolean - true if it is a struct representig a field in femtoTrack

% AUTHOR: JL 2022

    isField = true;
    isField = isField & isfield(field, 'mesh');
    if isField
        isField = isField & isfield(field.mesh, 'x');
        isField = isField & isfield(field.mesh, 'y');
        isField = isField & isfield(field.mesh, 'z');
    end
    isField = isField & isfield(field, 'data');
    if isField
        isField = isField & isfield(field.data, 'x');
        isField = isField & isfield(field.data, 'y');
        isField = isField & isfield(field.data, 'z');
    end
    isField = isField & isfield(field, 'nx');
    isField = isField & isfield(field, 'ny');
    isField = isField & isfield(field, 'nz');
    isField = isField & isfield(field, 'meshX');
    isField = isField & isfield(field, 'meshY');
    isField = isField & isfield(field, 'meshZ');
    isField = isField & isfield(field, 'meanCellSize');
    isField = isField & isfield(field, 'meshBounds');
    isField = isField & isfield(field, 'type');
    isField = isField & isfield(field, 'trans');
    isField = isField & isfield(field, 'frq');
    isField = isField & isfield(field, 'name');
    isField = isField & isfield(field, 'phaseShift');
    isField = isField & isfield(field, 'ignoreBoxList');
    isField = isField & isfield(field, 'meshIsEquiDist');
    if isField
        isField = isField & isfield(field.meshIsEquiDist, 'x');
        isField = isField & isfield(field.meshIsEquiDist, 'y');
        isField = isField & isfield(field.meshIsEquiDist, 'z');
    end
    isField = isField & isfield(field, 'meshIndexLUT');
    if isField
        isField = isField & isfield(field.meshIndexLUT, 'x');
        isField = isField & isfield(field.meshIndexLUT, 'y');
        isField = isField & isfield(field.meshIndexLUT, 'z');
        isField = isField & isfield(field.meshIndexLUT, 'xSpacing');
        isField = isField & isfield(field.meshIndexLUT, 'ySpacing');
        isField = isField & isfield(field.meshIndexLUT, 'zSpacing');
    end
end