function eField = potential2EField(pot,x,y,z)
   
%% INFO
% This function convertes a potential field to an electro static field.

%% IN
% pot - float (nx,ny,nz) - potential field
% x,y,z - float (n,1) - meah/grid spacing in x,y,z

%% OUT
% eField - float (nx,ny,nz) - electro static field


    [X,Y,Z] = ndgrid(x,y,z);

    eField.x = - diff(pot,1,1) ./ X(1:end-1,:,:);
    eField.y = - diff(pot,1,2) ./ Y(:,1:end-1,:);
    eField.z = - diff(pot,1,3) ./ Z(:,:,1:end-1);

    eField.x = eField.x(:,1:end-1,1:end-1);
    eField.y = eField.y(1:end-1,:,1:end-1);
    eField.z = eField.z(1:end-1,1:end-1,:);
end