function field = cutFieldMesh(field, range, info, doInterpo)

%% INFO
% This function cute a input field into a smaller field.

%% IN
% field - field struct - to be modefies field
% range - (3,2) float or int - remaining range; if outside do nothing
% info - {'cell','dist'} - select between RANGE given as cell indices or distnce in meters
% doInterpo - boolean - when INFO is 'dist' interpolate data from surunding cells 

%% OUT
% field - struct - cut field


    % set default values, when not given
    if nargin < 2
        error('ERROR. Missing parameter: range')
    end
    if nargin < 3
        info = 'cell';
    end
    if nargin < 4, doInterpo = false; end
    
    
    switch info
        case 'cell'
            range = abs(fix(range));
            field.mesh.x = field.mesh.x(range(1,1):range(1,2));
            field.mesh.y = field.mesh.y(range(2,1):range(2,2));
            field.mesh.z = field.mesh.z(range(3,1):range(3,2));

            field.data.x = field.data.x(range(1,1):range(1,2), range(2,1):range(2,2), range(3,1):range(3,2));
            field.data.y = field.data.y(range(1,1):range(1,2), range(2,1):range(2,2), range(3,1):range(3,2));
            field.data.z = field.data.z(range(1,1):range(1,2), range(2,1):range(2,2), range(3,1):range(3,2));

        case 'dist'
            if doInterpo
                cIndXmin = getClosestIndex(range(1,1), field.mesh.x);
                cIndXmax = getClosestIndex(range(1,2), field.mesh.x);
                cIndYmin = getClosestIndex(range(2,1), field.mesh.y);
                cIndYmax = getClosestIndex(range(2,2), field.mesh.y);
                cIndZmin = getClosestIndex(range(3,1), field.mesh.z);
                cIndZmax = getClosestIndex(range(3,2), field.mesh.z);

                field.mesh.x = field.mesh.x(cIndXmin:cIndXmax);
                field.mesh.x = field.mesh.y(cIndYmin:cIndYmax);
                field.mesh.x = field.mesh.z(cIndZmin:cIndZmax);

                field.data.x = field.data.x(cIndXmin:cIndXmax, cIndYmin:cIndYmax, cIndZmin:cIndZmax);
                field.data.y = field.data.y(cIndXmin:cIndXmax, cIndYmin:cIndYmax, cIndZmin:cIndZmax);
                field.data.z = field.data.z(cIndXmin:cIndXmax, cIndYmin:cIndYmax, cIndZmin:cIndZmax);
            else
                error('ERROR. TODO')
            end
        otherwise
            error('ERROR. cutting option not known, try: cell or dist')
    end
    
    % update dependet info
    field.nx = max(size(field.mesh.x));
    field.ny = max(size(field.mesh.y));
    field.nz = max(size(field.mesh.z));
    
    [field.meshX, field.meshY, field.meshZ] = ndgrid(field.mesh.x, field.mesh.y, field.mesh.z);
    field.meanCellSize = [mean(diff(field.mesh.x)), mean(diff(field.mesh.y)), mean(diff(field.mesh.z))];

    field.meshBounds = [min(field.mesh.x), max(field.mesh.x); min(field.mesh.y), max(field.mesh.y); min(field.mesh.z), max(field.mesh.z)];
end