function field = scaleTransFieldMesh(field, scales, trans)
    
%% INFO
% This function scales and translates the mesh/grid of the given field.

%% IN
% field - field struct - mesh of field which gets scales and translated
% scales - float (1,3) - scale factor of the mesh
% trans - float (1,3) - translation of the mesh

%% OUT
% field - field struct - field with scaled mesh coordinates

% AUTHOR: JL 2022


    % set default values, when not given
    if nargin < 3, trans = [0,0,0]; end

    field.mesh.x = field.mesh.x * scales(1) + trans(1);
    field.mesh.y = field.mesh.y * scales(2) + trans(2);
    field.mesh.z = field.mesh.z * scales(3) + trans(3);

    [field.meshX, field.meshY, field.meshZ] = ndgrid(field.mesh.x, field.mesh.y, field.mesh.z);
    field.meanCellSize = [mean(diff(field.mesh.x)), mean(diff(field.mesh.y)), mean(diff(field.mesh.z))];

    field.meshBounds = [min(field.mesh.x), max(field.mesh.x); min(field.mesh.y), max(field.mesh.y); min(field.mesh.z), max(field.mesh.z)];


    %% generate ADDITONAL PREPROCESS
    % TODO: equdistand mesh/ grid 
    eta = 0.001;
    % field.meshIsEquiDist = all(and(field.meanCellSize < ([diff(field.x)',diff(field.y)',diff(field.z)'] +eta), field.meanCellSize > ([diff(field.x)',diff(field.y)',diff(field.z)'] -eta)),1);

    field.meshIsEquiDist.x = all(and(field.meanCellSize(1) < (diff(field.mesh.x)' +field.meanCellSize(1)*eta), field.meanCellSize(1) > (diff(field.mesh.x)' -field.meanCellSize(1)*eta)),1);
    field.meshIsEquiDist.y = all(and(field.meanCellSize(2) < (diff(field.mesh.y)' +field.meanCellSize(1)*eta), field.meanCellSize(2) > (diff(field.mesh.y)' -field.meanCellSize(1)*eta)),1);
    field.meshIsEquiDist.z = all(and(field.meanCellSize(3) < (diff(field.mesh.z)' +field.meanCellSize(1)*eta), field.meanCellSize(3) > (diff(field.mesh.z)' -field.meanCellSize(1)*eta)),1);

    if field.meshIsEquiDist.x
        field.meshIndexLUT.xSpacing = field.meanCellSize(1);
    else
        field.meshIndexLUT.xSpacing = min(diff(field.mesh.x));
    end
    field.meshIndexLUT.x = field.mesh.x(1):field.meshIndexLUT.xSpacing:field.mesh.x(end);
    field.meshIndexLUT.x = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.x), field.meshIndexLUT.x);

    if field.meshIsEquiDist.y
        field.meshIndexLUT.ySpacing = field.meanCellSize(2);
    else
        field.meshIndexLUT.ySpacing = min(diff(field.mesh.y));
    end
    field.meshIndexLUT.y = field.mesh.y(1):field.meshIndexLUT.ySpacing:field.mesh.y(end);
    field.meshIndexLUT.y = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.y), field.meshIndexLUT.y);

    if field.meshIsEquiDist.z
        field.meshIndexLUT.zSpacing = field.meanCellSize(3);
    else
        field.meshIndexLUT.zSpacing = min(diff(field.mesh.z));
    end
    field.meshIndexLUT.z = field.mesh.z(1):field.meshIndexLUT.zSpacing:field.mesh.z(end);
    field.meshIndexLUT.z = arrayfun(@(testPos) getClosestIndex(testPos, field.mesh.z), field.meshIndexLUT.z);
end