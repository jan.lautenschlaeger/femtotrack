function field = addIgnorBoxes2Field(field, ignoreBoxList, replaceList)

%% INFO
% Add or replace the ifnorBoxList of the given field.

%% IN
% field - struct - field 
% ignorBoxList - list of (3,2) float - list of ignoreBoxes
% replaceList

%% OUT
% field - struct - changed suppressed field

    % set default values, when not given
    if nargin < 3, replaceList = false; end

    if replaceList
        field.ignoreBoxList = ignoreBoxList;
    else
        field.ignoreBoxList = [field.ignoreBoxList, ignoreBoxList];
    end

end