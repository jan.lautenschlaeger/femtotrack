classdef possibleOptions

%% INFO
% Definiton of all possible inputes for some of the string unoutes for
% different options.

   properties (Constant)
        windowMode = {'None', 'Adaptive', 'PreCalc'};
        solver = {'General', 'General_CInterpo', 'HPC'};
        solver_wWin = {'General', 'HPC'};
        fieldTypes = {'None', 'StaticE', 'StaticB', 'DynamicE', 'DynamicB'};
        saveFieldInWindow = {'Class', 'BunchRel'};
   end
end