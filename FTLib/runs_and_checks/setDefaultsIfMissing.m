function [phasespace, bunchInfo, fieldList, window, timeInfo, genSettings] = setDefaultsIfMissing(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings)

% INFO

% This function add any missing parameters the user has forgetten 
% and to set them to a defaut value. 

    % if not set deactivate the SELECTIVELY KILL OF PARTICLES
    if ~isfield(genSettings, 'killPartOutsideFields'), genSettings.killPartOutsideFields = false; end
    
    if ~isfield(genSettings, 'killPartInKillBox'), genSettings.killPartInKillBox = false; end
    

    if ~isfield(genSettings, 'killSlowParts'), genSettings.killSlowParts = false; end
    if ~isfield(genSettings, 'killBoxList'), genSettings.killBoxList = {}; end
    if genSettings.killSlowParts
        if ~isfield(genSettings, 'slowVelCutOffFactor'), genSettings.slowVelCutOffFactor = 0.0; end
        if ~isfield(genSettings, 'numberOfFastPart'), genSettings.numberOfFastPart = 1; end
        if ~isfield(genSettings, 'axisNumber'), genSettings.axisNumber = 123; end
    end

    if exist('window','var')
        if ~isfield(window, 'mode'), window.mode = 'Adaptive'; end
        if ~isfield(window, 'updateRate'), window.updateRate = 1; end
        if ~isfield(window, 'velMargin'), window.velMargin = 0; end
        if ~isfield(window, 'margin'), window.margin = [2,2; 2,2; 2,2]; end
        if ~isfield(window, 'maxWindowSize'), window.maxWindowSize = [inf, inf, inf]; end
    end
    
    if ~isfield(genSettings, 'doInteraction'), genSettings.doInteraction = false; end
    if ~isfield(genSettings, 'interactionType'), genSettings.interactionType = 'BunchRel'; end
    if ~isfield(genSettings, 'interactionEnergieCutoff_eV'), genSettings.interactionEnergieCutoff_eV = inf; end
    
    if ~isfield(genSettings, 'saveFields'), genSettings.saveFields = false; end
    if ~isfield(genSettings, 'saveFieldInWindow'), genSettings.saveFieldInWindow = false; end

    if ~isfield(genSettings, 'dt_update_res'), genSettings.dt_update_res = 1; end
    if ~isfield(genSettings, 'showInfo'), genSettings.showInfo = false; end
    if ~isfield(genSettings, 'saveEverNStep'), genSettings.saveEverNStep = 1; end

    if ~isfield(genSettings, 'visBoxList'), genSettings.visBoxList = {}; end
end