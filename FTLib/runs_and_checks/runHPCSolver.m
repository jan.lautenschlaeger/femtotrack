function [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings)

% !!! THIS CODE NEED THE Matlab Parallel Computing Toolbox !!! 
version = [1,1,0];

%% INFO
% This function inplements a parallel 3D electric and magnetic partical
% field tracker/solver for bunches for charged particles using the 
% Matlab 'parfor' loop. It works with static and dynamic fields as well as
% an particle-particle-field-interaction.
% This solver can only use a constant time step dt.

%% IN
% phasespace - struct - contains inital phasespace for all particles
% bunchInfo - struct - info on the particle bunches
% fieldList - struct - external fields
% window - struct - window info for fields
% timeInfo - struct - time info for Exp. dt
% genSettings - struct - additonal settings/options

%% OUT
% simState - struct - all the inforamatin of the simulation after each step
% debugTimes - float (NT,6) - mean time info how long each part of the solver took each step


    global g_FTLib_path;
    path([g_FTLib_path, '\FTLib_HPCSolver'], path);
    
%     if isfield(timeInfo, 'dt')
%         dt = timeInfo.dt;
%         Nt = fix(timeInfo.endT/dt);
%         timeInfo.Nt = Nt;
%     elseif isfield(timeInfo, 'Nt')
%         Nt = fix(timeInfo.Nt);
%         dt = timeInfo.endT/timeInfo.Nt;
%         timeInfo.dt = dt;
%     else
%         error('ERROR. The HPC solver currently only supports fix time steps. These can be set either by timeInfo.Nt or timeInfo.dt.');
%     end

    dt = timeInfo.dt;
    Nt = timeInfo.Nt;
    fprintf('INFO. dt = %g, Nt = %i\n', timeInfo.dt, timeInfo.Nt);

    % time settings
    saveEverNStep = 1; % genSettings.saveEverNStep
    
    %% Convert to HPC solver data structures
    [pos, mom, ~] = convPhasespace2SimData(phasespace, bunchInfo);
    bunches = bunchInfo.numBunches;     % == workers
    
    %% set up save data structures
    savePos = zeros(max(bunchInfo.bunchSizes), 3, bunchInfo.numBunches, 0);
    saveMom = zeros(max(bunchInfo.bunchSizes), 3, bunchInfo.numBunches, 0);
    saveDebugTime_FieldInt = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_eeI = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_Boris = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_upWindow = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_saveData = zeros(timeInfo.Nt,bunchInfo.numBunches);
    
    savePos(:,:,:, 1) = pos(:,:,:);
    saveMom(:,:,:, 1) = mom(:,:,:);
    
    %% MAIN LOOP
    tStartTotal = tic; 
    fprintf('Start HPC Solver ...\n')
    bunchSizes = bunchInfo.bunchSizes;
    ticBytes(gcp);
    
    parfor bIdx = 1:bunches
        
        % init window
        switch window.mode
            case 'Adaptive'
                windowInfo = updateWindow_HPC(fieldList, pos(:,:,bIdx), mom(:,:,bIdx), window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
            case 'PreCalc'
                windowInfo = window.windows{1};
            case 'None'
                windowInfo = genFullwindowData(fieldList);
            otherwise
                error('ERROR. Window Mode does not exist.');
        end

        for stepCounter = 2:Nt
            currentTime = dt * stepCounter;
    
            %% field interpolation
            tic;
            if window.mode == "Adaptiv" || window.mode == "PreCalc"
                [E, B] = interpolateFields_wWindow_HPC(fieldList, pos(:,:,bIdx), bunchSizes(bIdx), currentTime, windowInfo);
            else
                [E, B] = interpolateFields_HPC(fieldList, pos(:,:,bIdx), bunchSizes(bIdx), currentTime);
            end
            saveDebugTime_FieldInt(stepCounter,bIdx) = toc;
    
            %% ee interaction
            tic;
            if genSettings.doInteraction
                [pPE, pPB] = calcEEInteractionFields_HPC(pos(:,:,bIdx), mom(:,:,bIdx), bunchSizes(bIdx));
            else
                pPE = 0;
                pPB = 0;
            end
            saveDebugTime_eeI(stepCounter,2,bIdx) = toc;
    
            %% Boris kick
            tic;
            [pos(:,:,bIdx), mom(:,:,bIdx)] = doBorisPush_HPC(pos(:,:,bIdx), mom(:,:,bIdx), Constants.eCharge, Constants.eMass, E+pPE, B+pPB, dt);
            saveDebugTime_Boris(stepCounter,bIdx) = toc;

            %% update window 
            tic;
            switch window.mode
                case 'Adaptive'
                    if mod(stepCounter, window.updateRate) == 0
                        windowInfo = updateWindow_HPC(fieldList, pos(:,:,bIdx), mom(:,:,bIdx), window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
                    end
                case 'PreCalc'
                    windowInfo = updatePreCalcWindow(window, currentTime);
                case 'None'
                    
                otherwise
                    error('ERROR. Window Mode does not exist.');
            end
            saveDebugTime_upWindow(stepCounter,bIdx) = toc;
        
            %% save current sim state
            tic;
            if mod(stepCounter, saveEverNStep) == 0
                savePos(:,:,bIdx, stepCounter) = pos(:,:,bIdx);
                saveMom(:,:,bIdx, stepCounter) = mom(:,:,bIdx);
            end
            saveDebugTime_saveData(stepCounter,bIdx) = toc;
            
        end
    end
    if genSettings.showInfo, tocBytes(gcp); end
    totalSimTime = toc(tStartTotal);
    fprintf('INFO. total Sim Time: %g sec\n', totalSimTime)

    %% POST PROCESSING
    tic;
    saveDebugTime.FieldInt = saveDebugTime_FieldInt;
    saveDebugTime.eeI = saveDebugTime_eeI;
    saveDebugTime.Boris = saveDebugTime_Boris;
    saveDebugTime.upWindow = saveDebugTime_upWindow;
    saveDebugTime.saveData = saveDebugTime_saveData;
    [simState, debugTimes] = genHPCPostProcessing(savePos, saveMom, saveDebugTime, bunchInfo, fieldList, window, timeInfo, genSettings);
    simState(1).global.version = version;
    fprintf('INFO. Post Processing Time: %g sec\n', toc);

end