function [simState, debugTimes] = runGeneralSolver(initPhasespace, bunchInfo, fieldList, window, timeInfo, genSettings)

version = [2, 2, 0];

%% INFO
% This function implements a charged particle solver using the Boris
% method. Each step external windowed E and B fields get interpolated to 
% the particle positions, the particle-particle interaction gets
% calculated using a Lorentz trafo into a mean stationary frame for each 
% particle bunch. After the Boris push the field windows get updated and
% the current state of the simulation gets saved.

%% IN
% phasespace - struct - containd the inintal phasespace
% bunchInfo - struct - containg the bunching of the particles
% fieldList - list of field structs - list of external, hexagonal E and B fields
% window - struct - info about the field windows and how to update them
% timeInfo - struct - time informaltion lich the end time or the time step dt
% genSettings - struct - other settings

%% OUT
% simState - (1, Nt) struct - state of the simulation for each time step
% debugTimes - (Nt, 6) float - timing info about each step in the solver

%% EXTRA INFO
% simState
%   (1).global
%         .fields
%         .timeInfo
%         .bunchInfo
%         .genSettings                                              
%         .genSettings
%   (i).phaseSpace
%   (i).curTime
%   (i).dt
%   (i).windowInfo
%   (i).windowedField
%   (i).debugTimes

    global g_FTLib_path;
    path([g_FTLib_path, '\FTLib_Solver'], path);


    %% gen simulation phasespace with additonal flags
    phasespace.N = initPhasespace.N;
    phasespace.pos = NaN(size(initPhasespace.pos));
    phasespace.mom = NaN(size(initPhasespace.mom));
    psInfo.emissionTime = initPhasespace.emissionTime;
    phasespace.alive = true(initPhasespace.N, 1);
    phasespace.killReason = zeros(initPhasespace.N, 1, 'int8');
    phasespace.wasEmitted = false(initPhasespace.N,1);

    %% SAVE data setup
    debugTimes = zeros(timeInfo.Nt, 8);
    simState = [];
    stepCounter = 2;

    
    %% more SETUP stuff
    firstEmissionTime = min(psInfo.emissionTime);
    lastEmissionTime = max(psInfo.emissionTime);

    % first update of window 
    switch window.mode
        case 'Adaptive' 
            windowInfo = updateWindow(fieldList, phasespace, window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
        case 'PreCalc'
            windowInfo = window.windows{1};
        case 'None'
            windowInfo = genFullwindowData(fieldList);
        otherwise
            error(['ERROR. Window Mode does not exist.Try :', char(join(possibleOptions.windowMode, ', ')), '.']);
    end
        
    currentTime = 0; % start time
    dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, 0, 0, -1, timeInfo.dtVarargin);
    showProgres = 0.1; % 0.1 equals every 10%
    genSettings.saveEverNStep = max(1, abs(fix(genSettings.saveEverNStep)));
    
    %% save beginning state
    % save globale info and inital situation
    if genSettings.saveFields
        simState(1).global.fields = fieldList; 
    else
        simState(1).global.fields = false;
    end
    simState(1).global.timeInfo = timeInfo;
    simState(1).global.bunchInfo = bunchInfo;
    simState(1).global.genSettings = genSettings;
    simState(1).global.window = window;
    simState(1).global.version = version;
    
    simState(1).phaseSpace = phasespace;
    simState(1).curTime = currentTime;
    simState(1).dt = dt;
    simState(1).windowInfo = windowInfo;
    simState(1).windowedField = false;
    simState(1).debugTimes = debugTimes(1, :);
    
    % total time messuremt
    tStartTotal = tic; 

    %% early exit if data is missing
    if numel(fieldList) == 0
        fprintf(2,'ERROR. Empty fieldList. Early return.')
        return
    end
    if phasespace.N == 0
        fprintf(2,'ERROR. No Partilces. Early return.')
        return
    end
    if bunchInfo.numBunches == 0
        fprintf(2,'ERROR. No Bunches. Early return.')
        return
    end
    
    %% MAIN LOOP
    fprintf('Start General Solver ...\n')
    % while currentTime <= timeInfo.endT
    for stepCounter = 1:timeInfo.Nt
        
        %% EMIT Particles
        tic;
        % check if in particle emission time window
        if and(currentTime < lastEmissionTime + dt + eps, currentTime > firstEmissionTime)
            % determen emission of partilces
            newAddParticles = xor((psInfo.emissionTime <= currentTime), phasespace.wasEmitted);
            phasespace.wasEmitted = psInfo.emissionTime <= currentTime;
            % phasespace.wasEmitted = or(phasespace.wasEmitted, newAddParticles); % TODO: faster?
            
            % copy particles from inital phasespace
            phasespace.pos(newAddParticles, :) = initPhasespace.pos(newAddParticles, :);
            phasespace.mom(newAddParticles, :) = initPhasespace.mom(newAddParticles, :);
            dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, 0, 0, -1, timeInfo.dtVarargin);
            
            % update 'Adaptive' window and time step when particles get added 
            if and(sum(newAddParticles) > 0, window.mode == "Adaptive")
                windowInfo = updateWindow(fieldList, phasespace, window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
                dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, stepCounter, currentTime, dt, timeInfo.dtVarargin);
                assert(dt > 0, 'ASSERT. time step can not be negativ.');
            end
        end
        debugTimes(stepCounter, 1) = toc;   

        %% field interpolation
        tic;
        if true % TODO
            [E, B] = interpolateFields_wWindow(fieldList, phasespace, currentTime, windowInfo);
        else
            [E, B] = interpolateFields(fieldList, phasespace, currentTime); % maybe faster when no window is needed
        end
        debugTimes(stepCounter, 2) = toc;
    
        %% ee interaction
        tic;
        if genSettings.doInteraction 
            switch genSettings.interactionType
                case 'Class'
                    % assume purely static particles, therefore only
                    % electro static fields (eeWWB = 0)
                    [eeWWE, eeWWB] = calcEEInteractionFields(phasespace, bunchInfo);
                case 'BunchRel'
                    % Calculation the electro-static field in a per bunch
                    % stretched space corresponding to the average vel. and
                    % a inverse Lorentz transformation, resulting a
                    % magnetic field
                    [eeWWE, eeWWB] = calcEEInteractionFields_wLorentz(phasespace, bunchInfo, Constants.eMass, genSettings.interactionEnergieCutoff_eV);
                case 'PairRel'
                    error('ERROR. PairRel TODO');
                otherwise
                    error('ERROR. Interaction Type does not exist.');
            end
        else
            eeWWE = zeros(bunchInfo.N, 3);
            eeWWB = zeros(bunchInfo.N, 3);
        end
        debugTimes(stepCounter, 3) = toc;
    
        %% BORIS KICK
        tic;
        phasespace = doBorisPush(phasespace, Constants.eCharge, Constants.eMass, E + eeWWE, B + eeWWB, dt);
        debugTimes(stepCounter, 4) = toc;
    
        %% CHECK INSIDE FIELDS & KILLBOX
        tic;
        if genSettings.killPartOutsideFields
            phasespace = killParticlesOutsideFields(phasespace, fieldList);
        end
        if genSettings.killPartInKillBox
            phasespace = killParticlesInKillBox(phasespace, genSettings.killBoxList);
        end
        if genSettings.killSlowParts
            phasespace = killSlowParticles(phasespace, genSettings.slowVelCutOffFactor, genSettings.numberOfFastPart, genSettings.axisNumber, Constants.eMass);
        end
        if (genSettings.killPartOutsideFields | genSettings.killPartInKillBox | genSettings.killSlowParts)
            if sum(phasespace.alive) == 0
                fprintf('WARNING. Simulation ended early, because all particles are dead. (%i/%i Steps)\n', stepCounter, timeInfo.Nt);
                
                % last save of simState
                tic;
                if mod(stepCounter, genSettings.saveEverNStep) == 0
                    simState(stepCounter/genSettings.saveEverNStep).phaseSpace = phasespace;
                    simState(stepCounter/genSettings.saveEverNStep).curTime = currentTime;
                    simState(stepCounter/genSettings.saveEverNStep).dt = dt;
                    simState(stepCounter/genSettings.saveEverNStep).windowInfo = windowInfo;
                    simState(stepCounter/genSettings.saveEverNStep).debugTimes = debugTimes(stepCounter, :);
        
                    if genSettings.saveFieldInWindow
                        simState(stepCounter/genSettings.saveEverNStep).windowedField = cutAllFields2Window(fieldList, windowInfo);
                    else 
                        simState(stepCounter/genSettings.saveEverNStep).windowedField = false;  
                    end
                end
                debugTimes(stepCounter, 8) = toc;
                
                break;
            end
        end
        debugTimes(stepCounter, 5) = toc;

        %% UPDATE WINDOW
        tic;
        if or(mod(stepCounter, genSettings.dt_update_res) == 0, mod(stepCounter, genSettings.saveEverNStep) == 0)
            switch window.mode
                case 'Adaptive'
                    windowInfo = updateWindow(fieldList, phasespace, window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
                case 'PreCalc'
                    windowInfo = updatePreCalcWindow(window, currentTime);
                case 'None'

                otherwise
                    error('ERROR. Window Mode does not exist.');
            end
            windowedField = cutAllFields2Window(fieldList, windowInfo);
        end        
        debugTimes(stepCounter, 6) = toc;
    
        %% CALC NEXT DT
        tic;
        if mod(stepCounter, genSettings.dt_update_res) == 0
            dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, stepCounter, currentTime, dt, timeInfo.dtVarargin);
            assert(dt > 0, 'ASSERT. time step can not be negativ.');
        end  
        debugTimes(stepCounter, 7) = toc;
    
        %% SAVE current sim state
        tic;
        if mod(stepCounter, genSettings.saveEverNStep) == 0
            simState(stepCounter/genSettings.saveEverNStep).phaseSpace = phasespace;
            simState(stepCounter/genSettings.saveEverNStep).curTime = currentTime;
            simState(stepCounter/genSettings.saveEverNStep).dt = dt;
            simState(stepCounter/genSettings.saveEverNStep).windowInfo = windowInfo;
            simState(stepCounter/genSettings.saveEverNStep).debugTimes = debugTimes(stepCounter, :);

            if genSettings.saveFieldInWindow
                simState(stepCounter/genSettings.saveEverNStep).windowedField = cutAllFields2Window(fieldList, windowInfo);
            else 
                simState(stepCounter/genSettings.saveEverNStep).windowedField = false;  
            end
        end
        debugTimes(stepCounter, 8) = toc;
    
        %% PROGRESS ESTIMATION
        if mod(stepCounter, timeInfo.Nt * showProgres) == 0
            progressTimeTotal = toc(tStartTotal);
            remainingSteps = timeInfo.Nt - stepCounter;  % (timeInfo.endT - currentTime) / dt;
            approxTimePerStep = progressTimeTotal / stepCounter;
            remainingTime = remainingSteps * approxTimePerStep;
            
            fprintf('%s Progress %g %% \t Elapsed time: %g sec \t Approx remaining time: %g sec \t Particles alive: %i/%i \n', datestr(now, 'HH:MM:SS'), fix(stepCounter / timeInfo.Nt * 100), round(progressTimeTotal, 3), round(remainingTime, 3), sum(phasespace.alive), bunchInfo.N);
        end
    
        currentTime = currentTime + dt;
    end
    
    % save total simulated time
    simState(1).global.timeInfo.endT = currentTime - dt;

    %% print INFO
    fprintf('INFO. Sim Steps: %g \n', stepCounter -1);
    fprintf('INFO. Last Sim Time: %g sec \n', currentTime - dt);
    fprintf('INFO. Total Time: %g sec\n', sum(debugTimes, 'all'));
    fprintf('INFO. Mean Times: Interpo: %g ms, Interac: %g ms, Boris: %g ms, Update: %g ms\n', mean(debugTimes(:, 1) * 1000), mean(debugTimes(:, 2)) * 1000, mean(debugTimes(:, 3)) * 1000, mean(debugTimes(:, 4)) * 1000);
end

function cutfieldlist = cutAllFields2Window(fieldList, windowInfo)
    
    cutfieldlist = {};
    counter = 1;

    for field = fieldList
        cutfieldlist{counter} = cutFieldMesh(field{1}, windowInfo(:, :, counter));
        counter = counter +1;
    end
end