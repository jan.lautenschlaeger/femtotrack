function [simState, debugTimes] = runFTSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings)

    % set defaults for parameters if not set earlier
    if ~exist('window','var'), window.temp = 1; end     % create a false window variable if it doesn't exist
    [phasespace, bunchInfo, fieldList, window, timeInfo, genSettings] = setDefaultsIfMissing(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);

    %% CHECK & FIX
    window.updateRate = abs(max(1,fix(window.updateRate)));
    genSettings.dt_update_res = abs(max(1,fix(genSettings.dt_update_res)));
    pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
    if pass, fprintf('INFO. Checks all passed.\n'); end
    
    solverStart = datetime('now','TimeZone','local','Format','d-MM-y HH:mm:ss.SSS Z');

    switch genSettings.solver
        case 'General'
            [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
        case 'General_CInterpo'
            [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
        case 'HPC'
            [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
        case 'HPC_CInterpo'
            [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
        case 'None'
            warning('Solver Skipped.')
        otherwise
            error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
    end

    simState(1).global.timeStamps.solverStart = solverStart;
    simState(1).global.timeStamps.solverEnd = datetime('now','TimeZone','local','Format','d-MM-y HH:mm:ss.SSS Z');
end