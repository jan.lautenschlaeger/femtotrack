function [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings)

% !!! THIS CODE NEED THE Matlab Parallel Computing Toolbox !!! 
version = [1,1,0];

%% INFO
% This function inplements a parallel 3D electric and magnetic partical
% field tracker/solver for bunches for charged particles using the 
% Matlab 'parfor' loop. It works with static and dynamic fields as well as
% an particle-particle-field-interaction.
% This solver can only use a constant time step dt.

%% IN
% phasespace - struct - contains inital phasespace for all particles
% bunchInfo - struct - info on the particle bunches
% fieldList - struct - external fields
% window - struct - window info for fields
% timeInfo - struct - time info for Exp. dt
% genSettings - struct - additonal settings/options

%% OUT
% simState - struct - all the inforamatin of the simulation after each step
% debugTimes - float (NT,6) - mean time info how long each part of the solver took each step


    global g_FTLib_path;
    path([g_FTLib_path, '\FTLib_HPCSolver'], path);
    
%     if isfield(timeInfo, 'dt')
%         dt = timeInfo.dt;
%         Nt = fix(timeInfo.endT/dt);
%         timeInfo.Nt = Nt;
%     elseif isfield(timeInfo, 'Nt')
%         Nt = fix(timeInfo.Nt);
%         dt = timeInfo.endT/timeInfo.Nt;
%         timeInfo.dt = dt;
%     else
%         error('ERROR. The HPC solver currently only supports fix time steps. These can be set either by timeInfo.Nt or timeInfo.dt.');
%     end

    dt = timeInfo.dt;
    Nt = timeInfo.Nt;
    fprintf('INFO. dt = %g, Nt = %i\n', timeInfo.dt, timeInfo.Nt);

    % time settings
    saveEverNStep = 1; % genSettings.saveEverNStep
    
    %% Convert to HPC solver data structures
    [pos, mom, alive] = convPhasespace2SimData(phasespace, bunchInfo);
    bunches = bunchInfo.numBunches;     % == workers
    
    %% set up save data structures
    savePos = zeros(max(bunchInfo.bunchSizes), 3, bunchInfo.numBunches, 0);
    saveMom = zeros(max(bunchInfo.bunchSizes), 3, bunchInfo.numBunches, 0);
    saveDebugTime_FieldInt = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_eeI = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_Boris = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_upWindow = zeros(timeInfo.Nt,bunchInfo.numBunches);
    saveDebugTime_saveData = zeros(timeInfo.Nt,bunchInfo.numBunches);
    
    savePos(:,:,:, 1) = pos(:,:,:);
    saveMom(:,:,:, 1) = mom(:,:,:);
    
    %% MAIN LOOP
    tStartTotal = tic; 
    fprintf('Start HPC Solver ...\n')
    bunchSizes = bunchInfo.bunchSizes;
    ticBytes(gcp);
    
    parfor bIdx = 1:bunches
        
        % init window
        switch window.mode
            case 'Adaptive'
                windowInfo = updateWindow_HPC(fieldList, pos(:,:,bIdx), mom(:,:,bIdx), window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
            case 'PreCalc'
                windowInfo = window.windows{1};
            case 'None'
                windowInfo = genFullwindowData(fieldList);
            otherwise
                error('ERROR. Window Mode does not exist.');
        end

        for stepCounter = 2:Nt
            currentTime = dt * stepCounter;
    
            %% field interpolation
            tic;
            E = zeros(bunchInfo.bunchSizes(bIdx),3);
            B = zeros(bunchInfo.bunchSizes(bIdx),3);
            interpoField = zeros(bunchInfo.bunchSizes(bIdx), 3);
        
            for field = fieldList 
                insideField = checkInCube(pos(:,:,bIdx), field{1}.meshBounds); % remove outside field % TODO: check against window 
                insideField = and(insideField, alive(:,:,bIdx));               % remove dead particles
                for ignorBox = field{1}.ignoreBoxList                           % remove particles inside ignor box
                    insideField = and(insideField, checkOutCube(pos(:,:,bIdx), ignorBox{1}));
                end
        
                if sum(insideField) ~= 0
                    % x Interpol
                    indexX = field{1}.meshIndexLUT.x(fix((pos(insideField,1,bIdx)-field{1}.mesh.x(1)) / field{1}.meshIndexLUT.xSpacing) +1);
                    indexX  = indexX' - (pos(insideField,1,bIdx) < field{1}.mesh.x(indexX)');
                    weightsX = (field{1}.mesh.x(indexX+1)'-pos(insideField,1,bIdx)) ./ (field{1}.mesh.x(indexX+1) - field{1}.mesh.x(indexX))';
                    
                    % y Interpol
                    indexY = field{1}.meshIndexLUT.y(fix((pos(insideField,2,bIdx)-field{1}.mesh.y(1)) / field{1}.meshIndexLUT.ySpacing) +1);
                    indexY  = indexY' - (pos(insideField,2,bIdx) < field{1}.mesh.y(indexY)');
                    weightsY = (field{1}.mesh.y(indexY+1)'-pos(insideField,2,bIdx)) ./ (field{1}.mesh.y(indexY+1) - field{1}.mesh.y(indexY))';
                    
                    % y Interpol
                    indexZ = field{1}.meshIndexLUT.z(fix((pos(insideField,3,bIdx)-field{1}.mesh.z(1)) / field{1}.meshIndexLUT.zSpacing) +1);
                    indexZ  = indexZ' - (pos(insideField,3,bIdx) < field{1}.mesh.z(indexZ)');
                    weightsZ = (field{1}.mesh.z(indexZ+1)'-pos(insideField,3,bIdx)) ./ (field{1}.mesh.z(indexZ+1) - field{1}.mesh.z(indexZ))';
                
                    w000 = weightsZ     .* weightsY     .* weightsX;
                    w001 = weightsZ     .* weightsY     .* (1-weightsX);
                    w010 = weightsZ     .* (1-weightsY) .* weightsX;
                    w011 = weightsZ     .* (1-weightsY) .* (1-weightsX);
                    w100 = (1-weightsZ) .* weightsY     .* weightsX;
                    w101 = (1-weightsZ) .* weightsY     .* (1-weightsX);
                    w110 = (1-weightsZ) .* (1-weightsY) .* weightsX;
                    w111 = (1-weightsZ) .* (1-weightsY) .* (1-weightsX);
                
                    nxy = field{1}.nx * field{1}.ny;
                    combIndex = indexX + (indexY-1) * field{1}.nx + (indexZ-1) * nxy; % TODO: Check "orientation"
                
                    interpoField(insideField,1) = w000 .* field{1}.data.x(combIndex) + ...
                                        w001 .* field{1}.data.x(combIndex+1) + ...
                                        w010 .* field{1}.data.x(combIndex+field{1}.nx) + ...
                                        w011 .* field{1}.data.x(combIndex+field{1}.nx+1) + ...
                                        w100 .* field{1}.data.x(combIndex+nxy) + ...
                                        w101 .* field{1}.data.x(combIndex+nxy+1) + ...
                                        w110 .* field{1}.data.x(combIndex+field{1}.nx+nxy) + ...
                                        w111 .* field{1}.data.x(combIndex+field{1}.nx+nxy+1);
                
                    interpoField(insideField,2) = w000 .* field{1}.data.y(combIndex) + ...
                                        w001 .* field{1}.data.y(combIndex+1) + ...
                                        w010 .* field{1}.data.y(combIndex+field{1}.nx) + ...
                                        w011 .* field{1}.data.y(combIndex+field{1}.nx+1) + ...
                                        w100 .* field{1}.data.y(combIndex+nxy) + ...
                                        w101 .* field{1}.data.y(combIndex+nxy+1) + ...
                                        w110 .* field{1}.data.y(combIndex+field{1}.nx+nxy) + ...
                                        w111 .* field{1}.data.y(combIndex+field{1}.nx+nxy+1);
                
                    interpoField(insideField,3) = w000 .* field{1}.data.z(combIndex) + ...
                                        w001 .* field{1}.data.z(combIndex+1) + ...
                                        w010 .* field{1}.data.z(combIndex+field{1}.nx) + ...
                                        w011 .* field{1}.data.z(combIndex+field{1}.nx+1) + ...
                                        w100 .* field{1}.data.z(combIndex+nxy) + ...
                                        w101 .* field{1}.data.z(combIndex+nxy+1) + ...
                                        w110 .* field{1}.data.z(combIndex+field{1}.nx+nxy) + ...
                                        w111 .* field{1}.data.z(combIndex+field{1}.nx+nxy+1);
    
                    switch field{1}.type
                        case 'StaticE'
                            E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
                        case 'StaticB'
                            B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
                        case 'DynamicE'
                            interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*currentTime + angle(interpoField));
                            E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
                        case 'DynamicB'
                            interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*currentTime + angle(interpoField));
                            B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
                        otherwise
                            error(['ERROR. Try: ', char(join(possibleOptions.fieldTypes, ', ')), '.']);
                    end
                end
            end
            saveDebugTime_FieldInt(stepCounter,bIdx) = toc;
    
            %% ee interaction
            tic;
            if genSettings.doInteraction
                [pPE, pPB] = calcEEInteractionFields_HPC(pos(:,:,bIdx), mom(:,:,bIdx), bunchSizes(bIdx));
            else
                pPE = 0;
                pPB = 0;
            end
            saveDebugTime_eeI(stepCounter,2,bIdx) = toc;
    
            %% Boris kick
            tic;
            [pos(:,:,bIdx), mom(:,:,bIdx)] = doBorisPush_HPC(pos(:,:,bIdx), mom(:,:,bIdx), Constants.eCharge, Constants.eMass, E+pPE, B+pPB, dt);
            saveDebugTime_Boris(stepCounter,bIdx) = toc;

            %% update window 
            tic;
            switch window.mode
                case 'Adaptive'
                    if mod(stepCounter, window.updateRate) == 0
                        windowInfo = updateWindow_HPC(fieldList, pos(:,:,bIdx), mom(:,:,bIdx), window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
                    end
                case 'PreCalc'
                    windowInfo = updatePreCalcWindow(window, currentTime);
                case 'None'
                    
                otherwise
                    error('ERROR. Window Mode does not exist.');
            end
            saveDebugTime_upWindow(stepCounter,bIdx) = toc;
        
            %% save current sim state
            tic;
            if mod(stepCounter, saveEverNStep) == 0
                savePos(:,:,bIdx, stepCounter) = pos(:,:,bIdx);
                saveMom(:,:,bIdx, stepCounter) = mom(:,:,bIdx);
            end
            saveDebugTime_saveData(stepCounter,bIdx) = toc;
            
        end
    end
    if genSettings.showInfo, tocBytes(gcp); end
    totalSimTime = toc(tStartTotal);
    fprintf('INFO. total Sim Time: %g sec\n', totalSimTime)

    %% POST PROCESSING
    tic;
    saveDebugTime.FieldInt = saveDebugTime_FieldInt;
    saveDebugTime.eeI = saveDebugTime_eeI;
    saveDebugTime.Boris = saveDebugTime_Boris;
    saveDebugTime.upWindow = saveDebugTime_upWindow;
    saveDebugTime.saveData = saveDebugTime_saveData;
    [simState, debugTimes] = genHPCPostProcessing(savePos, saveMom, saveDebugTime, bunchInfo, fieldList, window, timeInfo, genSettings);
    simState(1).global.version = version;
    fprintf('INFO. Post Processing Time: %g sec\n', toc);

end