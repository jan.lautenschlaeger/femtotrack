function [simState, debugTimes] = runGeneralSolver_CInterpo(initPhasespace, bunchInfo, fieldList, window, timeInfo, genSettings)

version = [2, 2, 0];

%% INFO
% This function implements a charged particle solver using the Boris
% method. Each step external windowed E and B fields get interpolated to 
% the particle positions, the particle-particle interaction gets
% calculated using a Lorentz trafo into a mean stationary frame for each 
% particle bunch. After the Boris push the field windows get updated and
% the current state of the simulation gets saved.

%% IN
% phasespace - struct - contains the initial phasespace
% bunchInfo - struct - containing the bunching of the particles
% fieldList - list of field structs - list of external, hexagonal E and B fields
% window - struct - info about the field windows and how to update them
% timeInfo - struct - time information lich the end time or the time step dt
% genSettings - struct - other settings

%% OUT
% simState - (1,Nt) struct - state of the simulation for each time step
% debugTimes - (Nt,6) float - timing info about each step in the solver

%% EXTRA INFO
% simState
%   (1).global
%         .fields
%         .timeInfo
%         .bunchInfo
%         .genSettings                                              
%         .genSettings
%   (i).phaseSpace
%   (i).curTime
%   (i).dt
%   (i).windowInfo
%   (i).windowedField
%   (i).debugTimes

    global g_FTLib_path;
    path([g_FTLib_path, '\FTLib_Solver'], path);


    %% gen simulation phasespace with additonal flags
    phasespace.N = initPhasespace.N;
    phasespace.pos = NaN(size(initPhasespace.pos));
    phasespace.mom = NaN(size(initPhasespace.mom));
    psInfo.emissionTime = initPhasespace.emissionTime;
    phasespace.alive = true(initPhasespace.N, 1);
    phasespace.killReason = zeros(initPhasespace.N, 1, 'int8');
    phasespace.wasEmitted = false(initPhasespace.N,1);

    %% init save data setup
    debugTimes = zeros(timeInfo.Nt, 8);
    simState = [];

    %% more SETUP stuff
    windowInfo = genFullWindowData(fieldList); % needed for next dt calc
    firstEmissionTime = min(psInfo.emissionTime);
    lastEmissionTime = max(psInfo.emissionTime);
        
    currentTime = 0; % start time
    dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, 0, 0, -1, timeInfo.dtVarargin);
    
    % step size of simulation current state info messages
    showProgres = 0.1; % 0.1 equals every 10% progress
    
    %% SAVE BEGINNING STATE
    % save globale info and inital situation
    if genSettings.saveFields
        simState(1).global.fields = fieldList; 
    else
        simState(1).global.fields = false;
    end
    simState(1).global.timeInfo = timeInfo;
    simState(1).global.bunchInfo = bunchInfo;
    simState(1).global.psInfo = psInfo;
    simState(1).global.genSettings = genSettings;
    simState(1).global.window = window;
    simState(1).global.version = version;
    
    simState(1).phaseSpace = phasespace;
    simState(1).curTime = currentTime;
    simState(1).dt = dt;
    simState(1).windowInfo = windowInfo;
    simState(1).windowedField = false;
    simState(1).debugTimes = debugTimes(1, :);
    
    %% early exit if data is missing
    if numel(fieldList) == 0
        fprintf(2,'ERROR. Empty fieldList. Early return.')
        return
    end
    if phasespace.N == 0
        fprintf(2,'ERROR. No Partilces. Early return.')
        return
    end
    if bunchInfo.numBunches == 0
        fprintf(2,'ERROR. No Bunches. Early return.')
        return
    end

    % total time messuremt
    tStartTotal = tic; 

    % INIT ee Interaction fields in case of no interaction active
    eeWWE = zeros(bunchInfo.N, 3);
    eeWWB = zeros(bunchInfo.N, 3);
    
    %% MAIN LOOP
    fprintf('Start General Solver with custom Interpolator ...\n')
    % while currentTime <= timeInfo.endT
    for stepCounter = 1:timeInfo.Nt

        %% EMIT Particles
        tic;
        % check if in particle emission time window
        if and(currentTime < lastEmissionTime + dt + eps, currentTime >= firstEmissionTime)
            % determen emission of partilces
            newAddParticles = xor((psInfo.emissionTime <= currentTime), phasespace.wasEmitted);
            phasespace.wasEmitted = psInfo.emissionTime <= currentTime;
            % phasespace.wasEmitted = or(phasespace.wasEmitted, newAddParticles); % TODO: faster?
            
            % copy particles from inital phasespace
            phasespace.pos(newAddParticles, :) = initPhasespace.pos(newAddParticles, :);
            phasespace.mom(newAddParticles, :) = initPhasespace.mom(newAddParticles, :);
            
            % update timestep with the newly added particles
            if sum(newAddParticles) > 0
                dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, 0, 0, -1, timeInfo.dtVarargin);
            end
        end
        debugTimes(stepCounter, 1) = toc;    
        
        %% CUSTOM FIELD INTERPOLATION
        tic;
        % reset per-particle-fields
        E = zeros(bunchInfo.N, 3);
        B = zeros(bunchInfo.N, 3);
        if ~isempty(fieldList)
            interpoField = zeros(bunchInfo.N, 3);
    
            for field = fieldList 
                % remove particles from interpolation -
                % outside field, not yet emitted, not dead, not in ignorebox
                %insideField(phasespace.alive) = checkInCube(phasespace.pos(phasespace.alive,:), field{1}.meshBounds); % remove outside field % TODO: check against window 
                insideField = checkInCube(phasespace.pos, field{1}.meshBounds); % remove outside field % TODO: check against window 
                insideField = and(insideField, phasespace.wasEmitted);          % remove no yet spawned particles
                insideField = and(insideField, phasespace.alive);               % remove dead particles
                for ignorBox = field{1}.ignoreBoxList                           % remove particles inside ignoreBox
                    insideField = and(insideField, checkOutCube(phasespace.pos, ignorBox{1}));
                end
        
                if sum(insideField) ~= 0
                    % x Interpol
                    % find index in homo. mesh and look up index of corresponding inhomo. mesh node
                    indexX = field{1}.meshIndexLUT.x(fix((phasespace.pos(insideField, 1) - field{1}.mesh.x(1)) / field{1}.meshIndexLUT.xSpacing) + 1);
                    % fix possible of by one error
                    indexX  = indexX' - (phasespace.pos(insideField, 1) < field{1}.mesh.x(indexX)');
                    % calc linear interpolation weight
                    weightsX = (field{1}.mesh.x(indexX + 1)' - phasespace.pos(insideField, 1)) ./ (field{1}.mesh.x(indexX + 1) - field{1}.mesh.x(indexX))';
                    
                    % y Interpol
                    indexY = field{1}.meshIndexLUT.y(fix((phasespace.pos(insideField, 2) - field{1}.mesh.y(1)) / field{1}.meshIndexLUT.ySpacing) + 1);
                    indexY  = indexY' - (phasespace.pos(insideField, 2) < field{1}.mesh.y(indexY)');
                    weightsY = (field{1}.mesh.y(indexY + 1)' - phasespace.pos(insideField, 2)) ./ (field{1}.mesh.y(indexY + 1) - field{1}.mesh.y(indexY))';
                    
                    % z Interpol
                    indexZ = field{1}.meshIndexLUT.z(fix((phasespace.pos(insideField, 3) - field{1}.mesh.z(1)) / field{1}.meshIndexLUT.zSpacing) + 1);
                    indexZ  = indexZ' - (phasespace.pos(insideField, 3) < field{1}.mesh.z(indexZ)');
                    weightsZ = (field{1}.mesh.z(indexZ + 1)' - phasespace.pos(insideField, 3)) ./ (field{1}.mesh.z(indexZ + 1) - field{1}.mesh.z(indexZ))';
                
                    % calc 3d linear interpolation weights for the 8 nodes
                    w000 = weightsZ       .* weightsY       .* weightsX;
                    w001 = weightsZ       .* weightsY       .* (1 - weightsX);
                    w010 = weightsZ       .* (1 - weightsY) .* weightsX;
                    w011 = weightsZ       .* (1 - weightsY) .* (1 - weightsX);
                    w100 = (1 - weightsZ) .* weightsY       .* weightsX;
                    w101 = (1 - weightsZ) .* weightsY       .* (1 - weightsX);
                    w110 = (1 - weightsZ) .* (1 - weightsY) .* weightsX;
                    w111 = (1-weightsZ)   .* (1 - weightsY) .* (1 - weightsX);
    
                    % calc kanonischer index
                    nxy = field{1}.nx * field{1}.ny;
                    combIndex = indexX + (indexY - 1) * field{1}.nx + (indexZ - 1) * nxy; 
                
                    % calc new per-particle-fields
                    interpoField(insideField, 1) = w000 .* field{1}.data.x(combIndex) + ...
                                        w001 .* field{1}.data.x(combIndex + 1) + ...
                                        w010 .* field{1}.data.x(combIndex + field{1}.nx) + ...
                                        w011 .* field{1}.data.x(combIndex + field{1}.nx + 1) + ...
                                        w100 .* field{1}.data.x(combIndex + nxy) + ...
                                        w101 .* field{1}.data.x(combIndex + nxy + 1) + ...
                                        w110 .* field{1}.data.x(combIndex + field{1}.nx + nxy) + ...
                                        w111 .* field{1}.data.x(combIndex + field{1}.nx + nxy + 1);
                
                    interpoField(insideField, 2) = w000 .* field{1}.data.y(combIndex) + ...
                                        w001 .* field{1}.data.y(combIndex + 1) + ...
                                        w010 .* field{1}.data.y(combIndex + field{1}.nx) + ...
                                        w011 .* field{1}.data.y(combIndex + field{1}.nx + 1) + ...
                                        w100 .* field{1}.data.y(combIndex + nxy) + ...
                                        w101 .* field{1}.data.y(combIndex + nxy + 1) + ...
                                        w110 .* field{1}.data.y(combIndex + field{1}.nx + nxy) + ...
                                        w111 .* field{1}.data.y(combIndex + field{1}.nx + nxy + 1);
                
                    interpoField(insideField, 3) = w000 .* field{1}.data.z(combIndex) + ...
                                        w001 .* field{1}.data.z(combIndex + 1) + ...
                                        w010 .* field{1}.data.z(combIndex + field{1}.nx) + ...
                                        w011 .* field{1}.data.z(combIndex + field{1}.nx + 1) + ...
                                        w100 .* field{1}.data.z(combIndex + nxy) + ...
                                        w101 .* field{1}.data.z(combIndex + nxy + 1) + ...
                                        w110 .* field{1}.data.z(combIndex + field{1}.nx + nxy) + ...
                                        w111 .* field{1}.data.z(combIndex + field{1}.nx + nxy + 1);
    
                    % combine fields of same tye of differnt fields per particle
                    switch field{1}.type
                        case 'StaticE'
                            E(insideField, :) = E(insideField, :) + interpoField(insideField, :);
                        case 'StaticB'
                            B(insideField, :) = B(insideField, :) + interpoField(insideField, :);
                        case 'DynamicE'
                            interpoField = abs(interpoField) .* cos(2 * pi * field{1}.frq * currentTime + angle(interpoField));
                            E(insideField, :) = E(insideField, :) + interpoField(insideField, :);
                        case 'DynamicB'
                            interpoField = abs(interpoField) .* cos(2 * pi * field{1}.frq * currentTime + angle(interpoField));
                            B(insideField, :) = B(insideField, :) + interpoField(insideField, :);
                        otherwise
                            error(['ERROR. Try: ', char(join(possibleOptions.fieldTypes, ', ')), '.']);
                    end
                end
            end
        end
        debugTimes(stepCounter, 2) = toc;
    
        %% EE INTERACTION
        tic;
        if genSettings.doInteraction
            switch genSettings.interactionType
                case 'Class'
                    % assume purely static particles, therefore only
                    % electro static fields (eeWWB = 0)
                    [eeWWE, ~] = calcEEInteractionFields(phasespace, bunchInfo);
                case 'BunchRel'
                    % Calculation the electro-static field in a per bunch
                    % stretched space corresponding to the average vel. and
                    % a inverse Lorentz transformation, resulting a
                    % magnetic field
                    [eeWWE, eeWWB] = calcEEInteractionFields_wLorentz(phasespace, bunchInfo, Constants.eMass, genSettings.interactionEnergieCutoff_eV);
                case 'PairRel'
                    error(2,'ERROR. PairRel TODO');
                otherwise
                    error(2,'ERROR. Interaction Type does not exist.');
            end
        end
        debugTimes(stepCounter, 3) = toc;
    
        %% BORIS KICK
        tic;
        phasespace = doBorisPush(phasespace, Constants.eCharge, Constants.eMass, E + eeWWE, B + eeWWB, dt);
        debugTimes(stepCounter, 4) = toc;
    
        %% CHECK & KILL PARTICLES SELECTIVELY
        tic;
        if genSettings.killPartOutsideFields
            phasespace = killParticlesOutsideFields(phasespace, fieldList);
        end
        if genSettings.killPartInKillBox
            phasespace = killParticlesInKillBox(phasespace, genSettings.killBoxList);
        end
        if genSettings.killSlowParts
            phasespace = killSlowParticles(phasespace, genSettings.slowVelCutOffFactor, genSettings.numberOfFastPart, genSettings.axisNumber, Constants.eMass);
        end
        if (genSettings.killPartOutsideFields | genSettings.killPartInKillBox | genSettings.killSlowParts)
            if sum(phasespace.alive) == 0
                fprintf(2,'WARNING. Simulation ended early, because all particles are dead. (%i/%i Steps)\n', stepCounter, timeInfo.Nt);
                
                % last save of simState
                tic;
                if mod(stepCounter, genSettings.saveEverNStep) == 0
                    simState(stepCounter/genSettings.saveEverNStep).phaseSpace = phasespace;
                    simState(stepCounter/genSettings.saveEverNStep).curTime = currentTime;
                    simState(stepCounter/genSettings.saveEverNStep).dt = dt;
                    simState(stepCounter/genSettings.saveEverNStep).windowInfo = windowInfo;
                    simState(stepCounter/genSettings.saveEverNStep).debugTimes = debugTimes(stepCounter, :);
        
                    if genSettings.saveFieldInWindow
                        simState(stepCounter/genSettings.saveEverNStep).windowedField = cutAllFields2Window(fieldList, windowInfo);
                    else 
                        simState(stepCounter/genSettings.saveEverNStep).windowedField = false;  
                    end
                end
                debugTimes(stepCounter, 8) = toc;
                
                break;
            end
        end
        debugTimes(stepCounter, 5) = toc;

        %% UPDATE WINDOW - not necessary for this solver
        tic;
%         switch window.mode
%             case 'Adaptive'
%                 if mod(stepCounter, window.updateRate) == 0
%                     windowInfo = updateWindow(fieldList, phasespace, window.margin, window.velMargin, window.maxWindowSize, Constants.eMass);
%                 end
%             case 'PreCalc'
%                 windowInfo = updatePreCalcWindow(window, currentTime);
%             case 'None'
%                 
%             otherwise
%                 error('ERROR. Window Mode does not exist.');
%         end
%         
%         if or(mod(stepCounter, genSettings.dt_update_res) == 0, mod(stepCounter, genSettings.saveEverNStep) == 0)
%             windowedField = cutAllFields2Window(fieldList, windowInfo);
%         end
        debugTimes(stepCounter, 6) = toc;
        
        %% CALC NEXT DT
        tic;
        if mod(stepCounter, genSettings.dt_update_res) == 0
            dt = timeInfo.dtFunc(phasespace, bunchInfo, fieldList, windowInfo, stepCounter, currentTime, dt, timeInfo.dtVarargin);
            assert(dt > 0, 'ASSERT. time step can not be negativ.');
        end  
        debugTimes(stepCounter, 7) = toc;
    
        %% save current sim state
        tic;
        if mod(stepCounter, genSettings.saveEverNStep) == 0
            simState(stepCounter/genSettings.saveEverNStep).phaseSpace = phasespace;
            simState(stepCounter/genSettings.saveEverNStep).curTime = currentTime;
            simState(stepCounter/genSettings.saveEverNStep).dt = dt;
            simState(stepCounter/genSettings.saveEverNStep).windowInfo = windowInfo;
            simState(stepCounter/genSettings.saveEverNStep).debugTimes = debugTimes(stepCounter, :);

            if genSettings.saveFieldInWindow
                simState(stepCounter/genSettings.saveEverNStep).windowedField = cutAllFields2Window(fieldList, windowInfo);
            else 
                simState(stepCounter/genSettings.saveEverNStep).windowedField = false;  
            end
        end
        debugTimes(stepCounter, 8) = toc;
    
        %% Progress estimation
        if mod(stepCounter, timeInfo.Nt * showProgres) == 0
            progressTimeTotal = toc(tStartTotal);
            remainingSteps = timeInfo.Nt - stepCounter;  % (timeInfo.endT - currentTime) / dt;
            approxTimePerStep = progressTimeTotal / stepCounter;
            remainingTime = remainingSteps * approxTimePerStep;
            
            fprintf('%s Progress %g %% \t Elapsed time: %g sec \t Approx remaining time: %g sec \t Particles alive: %i/%i \n', datestr(now, 'HH:MM:SS'), fix(stepCounter / timeInfo.Nt * 100), round(progressTimeTotal, 3), round(remainingTime, 3), sum(phasespace.alive), bunchInfo.N);
        end
    
        currentTime = currentTime + dt;
    end
    
    % save total simulated time
    simState(1).global.timeInfo.endT = currentTime - dt;

    %% print INFO
    fprintf('INFO. Sim Steps: %g \n', stepCounter - 1);
    fprintf('INFO. Last Sim Time: %g sec \n', currentTime - dt);
    fprintf('INFO. Total Time: %g sec\n', sum(debugTimes, 'all'));
    fprintf('INFO. Total Times: Interpo: %g sec, Interac: %g sec, Boris: %g sec, Update: %g sec\n', sum(debugTimes(:, 1)), sum(debugTimes(:, 2)), sum(debugTimes(:, 3)), sum(debugTimes(:, 4)));
    fprintf('INFO. Mean Times: Interpo: %g ms, Interac: %g ms, Boris: %g ms, Update: %g ms\n', mean(debugTimes(:, 1) * 1000), mean(debugTimes(:, 2)) * 1000, mean(debugTimes(:, 3)) * 1000, mean(debugTimes(:, 4)) * 1000);
end

function cutfieldlist = cutAllFields2Window(fieldList, windowInfo)
    
    cutfieldlist = {};
    counter = 1;

    for field = fieldList
        cutfieldlist{counter} = cutFieldMesh(field{1}, windowInfo(:, :, counter));
        counter = counter + 1;
    end
end