function pass = checkAllInputs(phasespace, bunchInfo, fieldList, windowInfo, timeInfo, genSettings, showInfo)
    
%% INFO 
% This function checks all input given by the user for general 
% correctness, for example that all particle vectors fit together.

%% IN
% phasespace    - struc - inital particle info
% bunchInfo     - struc - particle grouing info
% fieldList     - list{field} - external fields
% windowInfo    - struc - field window settings
% timeInfo      - struc - time settings
% genSettings   - struc - general settings
% showInfo = false - boolean - if true print additonal info

%% OUT
% pass - boolean - true if all settings are ok

% AUTHOR: JL 2022

    %% TODO
    % - auto fix functionality? -> box order, abs(fix(.)), ...
    % - check for data tye: int, float, ..

    % set default values, when not given
    if nargin < 7, showInfo = false; end

    tic;
    %% check phasespace
    if showInfo, fprintf('Check Phasespace ... '); end
    assert(isPhasespaceStruct(phasespace), 'ASSERT. Phasespace struct error.');
    numPart = phasespace.N;
    assert(size(phasespace.pos,2) == 3, 'ASSERT. phasespace.pos has wrong spatial dimension.');
    assert(size(phasespace.pos,1) == numPart, 'ASSERT. phasespace.pos number particle mismatch.');
    assert(size(phasespace.mom,2) == 3, 'ASSERT. phasespace.mom has wrong spatial dimension.');
    assert(size(phasespace.mom,1) == numPart, 'ASSERT. phasespace.mom number particle mismatch.');
    assert(all(phasespace.emissionTime >= 0), 'ASSERT. phasespace.emissionTime has to be great equal zero.');
    % assert(size(phasespace.alive,2) == 1, 'ASSERT. phasespace.alive has wrong dimension.');
    % assert(size(phasespace.alive,1) == numPart, 'ASSERT. phasespace.alive number particle mismatch.');
    if showInfo, fprintf('pass\n'); end

    %% check bunchInfo
    if showInfo, fprintf('Check buchInfo ... '); end
    assert(isBunchInfoStruct(bunchInfo), 'ASSERT. BunchInfo struct error.')
    numBun = bunchInfo.numBunches;
    assert(size(bunchInfo.grouping,1) == numBun, 'ASSERT. bunchInfo.grouping wrong number bunches.');
    assert(size(bunchInfo.bunchSizes,1) == numBun, 'ASSERT. bunchInfo.bunchSizes wrong number bunches.');
    diffs = bunchInfo.grouping(:,2) - bunchInfo.grouping(:,1) +1;
    assert(all(diffs == bunchInfo.bunchSizes), 'ASSERT. bunchInfo.grouping and bunchInfo.bunchSizes mismatch.');
    assert(bunchInfo.N == phasespace.N, 'ASSERT. Phasespace and BuchInfo particle number mismazch.')
    if showInfo, fprintf('pass\n'); end

    %% check phasespace <-> bunchInfo
    assert(phasespace.N == bunchInfo.N, 'ASSERT. phasespace bunchInfo partical number mismatch.');

    %% check fieldList
    if showInfo, fprintf('Check fieldList ... (checking:'); end
    for field = fieldList
        if showInfo, fprintf('%s,', field{1}.name); end
        checkField(field{1})
    end
    if showInfo, fprintf(') pass\n'); end

    %% check interaction
    if showInfo, fprintf('Check interaction ... '); end
    if genSettings.doInteraction
        assert(any(strcmp(possibleOptions.saveFieldInWindow, genSettings.interactionType)), ' > ASSERT. Particle interaction does not exist.');
    end
    if showInfo, fprintf('pass\n'); end

    %% check vis/kill box
    if showInfo, fprintf('Check Vis Boxes ... '); end
    for box = genSettings.visBoxList
        assert(checkBox(box{1}), ' > ASSERT. Box limits in wrong order');
    end
    if showInfo, fprintf('pass\n'); end

    if showInfo, fprintf('Check Kill Boxes ... '); end
    for box = genSettings.killBoxList
        assert(checkBox(box{1}), ' > ASSERT. Box limits in wrong order');
    end
    if showInfo, fprintf('pass\n'); end

    %% check solver
    if showInfo, fprintf('Check solver ... '); end
    assert(any(strcmp(possibleOptions.solver, genSettings.solver)), 'ASSERT. Solver does not exist.');
    if showInfo, fprintf('pass\n'); end

    %% check time info
    if showInfo, fprintf('Check timeInfo ... '); end
    assert(timeInfo.dt > 0, 'ASSERT. dt has to be greater 0.');
    assert(timeInfo.Nt > 0, 'ASSERT. Number of simulation streps (Nt) has to be greater 0.');
    if showInfo, fprintf('pass\n'); end

    %% check window
    if any(strcmp(possibleOptions.solver_wWin, genSettings.solver))
        assert(isWindowStruct(windowInfo), 'ASSERT. WindowInfo struct error.');

        if showInfo, fprintf('Check window ... '); end
        assert(any(strcmp(possibleOptions.windowMode, windowInfo.mode)), 'ASSERT. Winodw option does not exist.');
        assert(windowInfo.updateRate >= 1, 'ASSERT. Window update rate has to be >= 1.')

        if possibleOptions.windowMode == "Adaptive"
            assert(all(windowInfo.maxWindowSize > 0), 'ASSERT. Window max size has to be greater 0.')
        end

        if possibleOptions.windowMode == "PreCalc"
            assert(size(windowInfo.times,1) == size(windowInfo.windows,1), 'ASSERT. Preclac window data size mismatch.')
        end
        
        if showInfo, fprintf('pass\n'); end
    end

    pass = true;    
    fprintf('INFO. Check Time: %g sec\n', toc);
end



function [] = checkField(field)
    % isIncreasing = all(diff(a)) %or all(diff(a)>=0) if you want to allow 0 difference
    assert(isFieldStruct(field), 'ASSERT. Field has missing struct fields.')

    nVec = [field.nx, field.ny, field.nz];
    assert(all(nVec == size(field.data.x)), ' > ASSERT. field.data.x number of points wrong.');
    assert(all(nVec == size(field.data.y)), ' > ASSERT. field.data.y number of points wrong.');
    assert(all(nVec == size(field.data.z)), ' > ASSERT. field.data.z number of points wrong.');
    assert(all(nVec == size(field.meshX)), ' > ASSERT. field.meshX number of points wrong.');
    assert(all(nVec == size(field.meshY)), ' > ASSERT. field.meshY number of points wrong.');
    assert(all(nVec == size(field.meshZ)), ' > ASSERT. field.meshZ number of points wrong.');
    assert(nVec(1) == size(field.mesh.x,2), ' > ASSERT. field.mesh.x number of points wrong.');
    assert(nVec(2) == size(field.mesh.y,2), ' > ASSERT. field.mesh.y number of points wrong.');
    assert(nVec(3) == size(field.mesh.z,2), ' > ASSERT. field.mesh.z number of points wrong.');
    
    assert(any(strcmp(possibleOptions.fieldTypes, field.type)), ' > ASSERT. Field type does not exist.');

    assert(field.frq >= 0, ' > ASSERT. Field frequency should not be negative.');

    for box = field.ignoreBoxList
        assert(checkBox(box{1}), ' >> ASSERT. Box limits in wrong order');
    end
end

function pass = checkBox(box)
    % checke that all box limits are in increasing order in x,y,z
    pass = all(diff(box(1,:))>0) &  all(diff(box(2,:))>0) &  all(diff(box(3,:))>0);
end