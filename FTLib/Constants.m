classdef Constants

%% INFO
% List of physical constants

   properties (Constant)
        eCharge = -1.602176634*10^(-19);     % C
        eMass = 9.1093837015*10^(-31);       % kg
        eps0 = 8.854187817 * 10^(-12);          % F/m
        mu0 = 1.25663706212 * 10^(-6);          % H/m
        c = 299792458;                          % m/s
        kFac = 1/(4*pi*Constants.eps0);         % = 8.9875517923(14)×10^9 N·m^2/C^2
        eMass_eV = 0.51099895e6;               %eV
   end
end