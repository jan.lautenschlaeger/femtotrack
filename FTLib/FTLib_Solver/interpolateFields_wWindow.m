function [E, B] = interpolateFields_wWindow(fieldList, phasespace, time, windowInfo)
    
    %% INFO
    % Interpolate 3D fields at each particle positon and return the
    % combinded E and B field for each particle. This function uses a
    % window to limit the field given to the interpolation function and 
    % thus speeding up the interpolation caclulation. 
    % Overlapping field of the same type are added (Superposition principle).
    % Dynamic fields use the cos function.

    %% IN
    % fieldList     - list{fields} - list of m stucts representing fields
    % phasespace    - struc - struct containign info of n particle
    % time          - float - current time in sec
    % windowInfo    - (3,2,m) int - min,max indices for x,y,z for each field

    %% OUT
    % E - (n,3) float - x,y,z E field info for each particle
    % B - (n,3) float - x,y,z B field info for each particle

    % AUTHOR: JL 2022

    % per step setup
    E = zeros(phasespace.N,3);
    B = zeros(phasespace.N,3);

    if isempty(fieldList)
        return
    end

    interpoField = zeros(phasespace.N, 3);

    counter = 1;

    for field = fieldList
        insideField = checkInCube(phasespace.pos, field{1}.meshBounds);
        % windowBox = [field{1}.mesh.x(windowInfo(1,:,counter)); field{1}.mesh.y(windowInfo(2,:,counter)); field{1}.mesh.z(windowInfo(3,:,counter))];
        % insideField = checkInCube(phasespace.pos,windowBox); % remove outside window
        insideField = and(insideField, phasespace.wasEmitted);          % remove no yet spawned particles
        insideField = and(insideField, phasespace.alive);               % remove dead particles
        for ignorBox = field{1}.ignoreBoxList
            insideField = and(insideField, checkOutCube(phasespace.pos, ignorBox{1}));
        end

        if sum(insideField) ~= 0
            xRange = windowInfo(1,1,counter):windowInfo(1,2,counter);
            yRange = windowInfo(2,1,counter):windowInfo(2,2,counter);
            zRange = windowInfo(3,1,counter):windowInfo(3,2,counter);

            F = griddedInterpolant(field{1}.meshX(xRange, yRange, zRange), ...
                                   field{1}.meshY(xRange, yRange, zRange), ...
                                   field{1}.meshZ(xRange, yRange, zRange), ...
                                   field{1}.data.x(xRange, yRange, zRange), ...
                                   'linear', 'none');
            interpoField(insideField,1) = F(phasespace.pos(insideField,1), phasespace.pos(insideField,2), phasespace.pos(insideField,3));
            F.Values = field{1}.data.y(xRange, yRange, zRange);
            interpoField(insideField,2) = F(phasespace.pos(insideField,1), phasespace.pos(insideField,2), phasespace.pos(insideField,3));
            F.Values = field{1}.data.z(xRange, yRange, zRange);
            interpoField(insideField,3) = F(phasespace.pos(insideField,1), phasespace.pos(insideField,2), phasespace.pos(insideField,3));
            
            
            switch field{1}.type
                case 'StaticE'
                    E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
                case 'StaticB'
                    B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
                case 'DynamicE'
                    interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*time + angle(interpoField));
                    E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
                case 'DynamicB'
                    interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*time + angle(interpoField));
                    B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
                otherwise
                    error(['ERROR. Try: ', char(join(possibleOptions.fieldTypes, ', ')), '.']);
            end
        end

        counter = counter +1;
    end
end