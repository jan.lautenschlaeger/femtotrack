function [E, B] = interpolateFields_HOSVD(fieldList, phasespace, currentTime)
    
    %% TODO - JL2022

    %% INFO
    % 3D field interpolation for field using an HSOVD representation by
    % interpolating the seperate tensores.
    % Overlapping field of the same type are added (Superposition principle).

    %% IN
    % fieldList - list{fields} - list of all fields with HOSVD representation
    % phasespace - struct - current particle information
    % currentTime - float - current simulation time in sec

    %% OUT
    % E - (m,3) - x,y,z E field info for each particle
    % B - (m,3) - x,y,z B field info for each particle

    % AUTHOR: JL 2022


    % per step setup
    E = zeros(phasespace.N,3);
    B = zeros(phasespace.N,3);
    interpoField = zeros(phasespace.N, 3);

    for field = fieldList
        insideField = checkInCube(phasespace.pos, field{1}.meshBounds);

        F = griddedInterpolant(field{1}.meshX, field{1}.meshY, field{1}.meshZ, field{1}.data.x, 'linear', 'none');
        interpoField(insideField,1) = F(phasespace.pos(insideField,1), phasespace.pos(insideField,2), phasespace.pos(insideField,3));
        F.Values = field{1}.data.y;
        interpoField(insideField,2) = F(phasespace.pos(insideField,1), phasespace.pos(insideField,2), phasespace.pos(insideField,3));
        F.Values = field{1}.data.z;
        interpoField(insideField,3) = F(phasespace.pos(insideField,1), phasespace.pos(insideField,2), phasespace.pos(insideField,3));

        switch field{1}.type
            case 'StaticE'
                E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
            case 'StaticB'
                B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
            case 'DynamicE'
                interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*currentTime + angle(interpoField));
                E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
            case 'DynamicB'
                interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*currentTime + angle(interpoField));
                B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
            otherwise
                error(['ERROR. Try: ', char(join(possibleOptions.fieldTypes, ', ')), '.']);
        end
    end
end