function [pPE, pPB] = calcEEInteractionFields(phasespace, bunchInfo)
    
%% INFO
% This function calculates a simplified electron-electron field interaction
% by assuming a static situation and thus only calculating the pairwise
% electric field.

%% IN
% phasespace    - struc - position and momentum of n particles
% bunchInfo     - struc - information about particle grouping

%% OUT
% pPE - (n,3) flaot - per partical resulting electric field
% pPB - (n,3) float - per partical resulting magnetic field >> here always zero!

% AUTHOR: JL 2022

    pPE = zeros(bunchInfo.N,3);
    pPB = zeros(bunchInfo.N,3);


    for kk = 1:size(bunchInfo.grouping,1)
        range = bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2);

        % calc static electric field 
        dx = (phasespace.pos(range, 1) - phasespace.pos(range,1)');
        dy = (phasespace.pos(range, 2) - phasespace.pos(range,2)');
        dz = (phasespace.pos(range, 3) - phasespace.pos(range,3)');

        dist = sqrt((dx.*dx + dy.*dy + dz.*dz));

        dist3 = dist.*dist.*dist; % is 3-4x faster as .^3, with sum abs erro < e-31 for 642x642 matrix
        ppEFieldx = dx ./ dist3;
        ppEFieldy = dy ./ dist3;
        ppEFieldz = dz ./ dist3;

        preFac = Constants.eCharge * Constants.kFac;
        pPE(range,1) = preFac * sum(ppEFieldx, 2, 'omitnan');
        pPE(range,2) = preFac * sum(ppEFieldy, 2, 'omitnan');
        pPE(range,3) = preFac * sum(ppEFieldz, 2, 'omitnan');
    end
end