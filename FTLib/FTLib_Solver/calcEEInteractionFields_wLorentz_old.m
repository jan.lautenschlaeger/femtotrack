function [pPE, pPB] = calcEEInteractionFields_wLorentz_old(phasespace, bunchInfo, partMass)
    
%% INFO
% This function calculates the electron-electron field interaction with
% Lorentz boost. For this the electrons get transforamt to an mean stationary
% frame, in which the static electric field gets calculated. Then the field
% gets transforamted back to the global frame.

%% IN
% phasespace - {.pos, .mom} - position and momentum of NB particles
% NB - int - number of particles in bunch

%% OUT
% pPE - (NB,3) flaot - per partical resulting electric field
% pPB - (NB,3) float - per partical resulting magnetic field


    pPE = zeros(bunchInfo.N,3);
    pPB = zeros(bunchInfo.N,3);

    for kk = 1:bunchInfo.numBunches
        if sum(phasespace.alive(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2))) ~= 0
            moms = phasespace.mom(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),:);
            meanMom = sum(moms(phasespace.alive(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2)),:),1)./bunchInfo.bunchSizes(kk); % == mean() but faster
            gamma = sqrt(1 + sum(meanMom.^2,2) /(partMass*Constants.c)^2);
            velFromMom = meanMom ./ (partMass * gamma);
            boostMatrix = calcBoostMatrix(velFromMom);
            
            % scalingFactor = ones(1,3) + (gamma-1) * meanMom/sqrt(sum(meanMom.^2,2));
            scalingFactor = diag(boostMatrix);
    
            poss = phasespace.pos(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),:);
            ceneterPos = sum(poss(phasespace.alive(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2)),:),1)./bunchInfo.bunchSizes(kk); % == mean() but faster
            trafoPos = (phasespace.pos(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),:) - ceneterPos).*scalingFactor(2:4)';
    
            %% calc 'static' e field in moving frame
            NB = bunchInfo.grouping(kk,2) - bunchInfo.grouping(kk,1) +1;
            maskBunch = ones(NB) - eye(NB);
    
            dx = maskBunch .* (trafoPos(:, 1) - trafoPos(:, 1)');
            dy = maskBunch .* (trafoPos(:, 2) - trafoPos(:, 2)');
            dz = maskBunch .* (trafoPos(:, 3) - trafoPos(:, 3)');
    
            dist = sqrt((dx.^2+dy.^2+dz.^2));
    
            ppEFieldx = dx ./ dist.^3;
            ppEFieldy = dy ./ dist.^3;
            ppEFieldz = dz ./ dist.^3;
    
            pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1) = Constants.eCharge * Constants.kFac * sum(ppEFieldx, 2, 'omitnan');
            pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2) = Constants.eCharge * Constants.kFac * sum(ppEFieldy, 2, 'omitnan');
            pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3) = Constants.eCharge * Constants.kFac * sum(ppEFieldz, 2, 'omitnan');
            
    %         %% back Lorentz trafo for the fields
    %         for ii = bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2)
    %             fieldMatrix = boostMatrix * genRelFieldMatrix(pPE(ii,1), pPE(ii,2), pPE(ii,3), 0, 0, 0) * boostMatrix;
    %             pPE(ii, 1) = fieldMatrix(2,1)*Constants.c;
    %             pPE(ii, 2) = fieldMatrix(3,1)*Constants.c;
    %             pPE(ii, 3) = fieldMatrix(4,1)*Constants.c;
    %         
    %             pPB(ii, 1) = fieldMatrix(4,3);
    %             pPB(ii, 2) = fieldMatrix(2,4);
    %             pPB(ii, 3) = fieldMatrix(3,2);
    %         end
    
            %% does the same as loop ... but with const time
            % for n = 1 or 2 loop is sometime faster
            bM = boostMatrix;
            c = Constants.c;
            pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2), 1) = ((bM(2,1)*(-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(2,1)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(3,1)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(4,1))+bM(2,2)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(1,1))+bM(2,3)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(1,1))+bM(2,4)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(1,1))))*c;
            pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2), 2) = ((bM(3,1)*(-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(2,1)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(3,1)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(4,1))+bM(3,2)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(1,1))+bM(3,3)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(1,1))+bM(3,4)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(1,1))))*c;
            pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2), 3) = ((bM(4,1)*(-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(2,1)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(3,1)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(4,1))+bM(4,2)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(1,1))+bM(4,3)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(1,1))+bM(4,4)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(1,1))))*c;
            
            pPB(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2), 1) = (bM(4,1)*(-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(2,3)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(3,3)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(4,3))+bM(4,2)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(1,3))+bM(4,3)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(1,3))+bM(4,4)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(1,3)));
            pPB(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2), 2) = (bM(2,1)*(-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(2,4)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(3,4)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(4,4))+bM(2,2)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(1,4))+bM(2,3)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(1,4))+bM(2,4)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(1,4)));
            pPB(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2), 3) = (bM(3,1)*(-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(2,2)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(3,2)-pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(4,2))+bM(3,2)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),1)/c*bM(1,2))+bM(3,3)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),2)/c*bM(1,2))+bM(3,4)*(pPE(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2),3)/c*bM(1,2)));
        end
    end

%     %% remove interaction field from dead particales ? bunches?
%     pPE(~phasespace.alive,:) = 0;
%     pPE(~phasespace.alive,:) = 0;
end

function boostMatrix = calcBoostMatrix(vel)
    c = Constants.c;
    absVel = sqrt(sum(vel.^2,2));
    if absVel == 0
        boostMatrix = eye(4);
        return;
    end
    gamma = 1/sqrt(1-(absVel/c)^2);
    boostMatrix = [gamma, -gamma*vel(1)/c, -gamma*vel(2)/c, -gamma*vel(3)/c; ...
        -gamma*vel(1)/c, 1+(gamma-1)*(vel(1)/absVel)^2, (gamma-1)*vel(1)*vel(2)/absVel^2 (gamma-1)*vel(1)*vel(3)/absVel^2; ... 
        -gamma*vel(2)/c, (gamma-1)*vel(2)*vel(1)/absVel^2, 1+(gamma-1)*(vel(2)/absVel)^2, (gamma-1)*vel(2)*vel(3)/absVel^2; ...
        -gamma*vel(3)/c, (gamma-1)*vel(3)*vel(1)/absVel^2, (gamma-1)*vel(3)*vel(2)/absVel^2, 1+(gamma-1)*(vel(3)/absVel)^2];
end

function fieldMatrix = genRelFieldMatrix(xE, yE, zE, xB, yB, zB)
    c = Constants.c;
    fieldMatrix = [0,-xE/c,-yE/c,-zE/c; xE/c,0,-zB,yB; yE/c,zB,0,-xB; zE/c,-yB,xB,0];
end