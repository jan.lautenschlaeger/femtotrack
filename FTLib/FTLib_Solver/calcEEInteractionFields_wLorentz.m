function [pPE1, pPB1] = calcEEInteractionFields_wLorentz(phasespace, bunchInfo, partMass, cutOffEnergy)
    
%% INFO
% This function calculates the electron-electron field interaction with
% Lorentz boost. For this the electrons get transformed to a mean stationary
% frame, in which the static electric field is calculated. Then the field
% gets transformed back to the global frame, during this the
% magnetic fields get introduced.

%% IN
% phasespace    - struc - info of n particles
% bunchInfo     - struc - particle grouping information
% partMass      - float - partiocle mass in kg

%% OUT
% pPE - (NB,3) flaot - per partical resulting electric field
% pPB - (NB,3) float - per partical resulting magnetic field

% AUTHOR: JL 2022

    % set default values, when not given
    if nargin < 4, cutOffEnergy = inf; end

    pPE = zeros(bunchInfo.N,3);
    pPB = zeros(bunchInfo.N,3);
    pPE1 = zeros(bunchInfo.N,3);
    pPB1 = zeros(bunchInfo.N,3);

    bmMat = zeros(bunchInfo.N, 10);
    c = Constants.c;

    for kk = 1:bunchInfo.numBunches

        % determine a mask for relevant particles
        relevPartId = bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2);
        relevPartId(~(phasespace.alive(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2)) & phasespace.wasEmitted(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2)))) = [];
        numRelPart = size(relevPartId,2);
        
        meanKinEngBunch_eV = sqrt(sum(sum(phasespace.mom(relevPartId,:).^2,2))) * Constants.c / abs(Constants.eCharge) / numRelPart;
%         fprintf("meanKinEngBunch_eV: %g\n",meanKinEngBunch_eV);

        % if sum(phasespace.alive(bunchInfo.grouping(kk,1):bunchInfo.grouping(kk,2))) ~= 0
        % if isempty(relevPartId)
       
        if numRelPart > 1 & meanKinEngBunch_eV < cutOffEnergy
            % calculate average velocity and corresponding boost matrix
            moms = phasespace.mom(relevPartId,:);
            meanMom = sum(moms,1)./numRelPart; % == mean() but faster
            gamma = sqrt(1 + sum(meanMom.^2,2) /(partMass*Constants.c)^2);
            velFromMom = meanMom ./ (partMass * gamma);
            boostMatrix = calcBoostMatrix(velFromMom);
            
            % scalingFactor = ones(1,3) + (gamma-1) * meanMom/sqrt(sum(meanMom.^2,2));
            scalingFactor = diag(boostMatrix);
    
            % stretch space along mean velocity
            poss = phasespace.pos(relevPartId,:);
            ceneterPos = sum(poss,1)./numRelPart; % == mean() but faster
            trafoPos = (phasespace.pos(relevPartId,:) - ceneterPos).*scalingFactor(2:4)';
    
            %% calc 'static' e field in moving frame
            dx = (trafoPos(:, 1) - trafoPos(:, 1)');
            dy = (trafoPos(:, 2) - trafoPos(:, 2)');
            dz = (trafoPos(:, 3) - trafoPos(:, 3)');
    
            temp = dx.*dx + dy.*dy + dz.*dz;
            dist = sqrt(temp);          % the calculation is faster when split, up to ~30% for larger matrices

            dist3 = dist.*dist.*dist; % is 3-4x faster as .^3, with sum abs erro < e-31 for 642x642 matrix
            ppEFieldx = dx ./ dist3;
            ppEFieldy = dy ./ dist3;
            ppEFieldz = dz ./ dist3;
    
            preFac = Constants.eCharge * Constants.kFac;
            pPE(relevPartId,1) = preFac * sum(ppEFieldx, 2, 'omitnan');
            pPE(relevPartId,2) = preFac * sum(ppEFieldy, 2, 'omitnan');
            pPE(relevPartId,3) = preFac * sum(ppEFieldz, 2, 'omitnan');
            
            %% back Lorentz trafo for the fields
            bmMat(relevPartId,1) = boostMatrix(1,1);
            bmMat(relevPartId,2) = boostMatrix(1,2);
            bmMat(relevPartId,3) = boostMatrix(1,3);
            bmMat(relevPartId,4) = boostMatrix(1,4);
            bmMat(relevPartId,5) = boostMatrix(2,2);
            bmMat(relevPartId,6) = boostMatrix(2,3);
            bmMat(relevPartId,7) = boostMatrix(2,4);
            bmMat(relevPartId,8) = boostMatrix(3,3);
            bmMat(relevPartId,9) = boostMatrix(3,4);
            bmMat(relevPartId,10) = boostMatrix(4,4);
        end
    end

    % do back trafo for all bunches at the same time - faster
    pPE1(:, 1) = -bmMat(:,2).*(bmMat(:,2).*pPE(:,1) + bmMat(:,3).*pPE(:,2) + bmMat(:,4).*pPE(:,3)) + bmMat(:,1).*(bmMat(:,5).*pPE(:,1) + bmMat(:,6).*pPE(:,2) + bmMat(:,7).*pPE(:,3));
    pPE1(:, 2) = -bmMat(:,3).*(bmMat(:,2).*pPE(:,1) + bmMat(:,3).*pPE(:,2) + bmMat(:,4).*pPE(:,3)) + bmMat(:,1).*(bmMat(:,6).*pPE(:,1) + bmMat(:,8).*pPE(:,2) + bmMat(:,9).*pPE(:,3));
    pPE1(:, 3) = -bmMat(:,4).*(bmMat(:,2).*pPE(:,1) + bmMat(:,3).*pPE(:,2) + bmMat(:,4).*pPE(:,3)) + bmMat(:,1).*(bmMat(:,7).*pPE(:,1) + bmMat(:,9).*pPE(:,2) + bmMat(:,10).*pPE(:,3));
    
    pPB1(:, 1) = (-bmMat(:,4).*(bmMat(:,6).*pPE(:,1) + bmMat(:,8).*pPE(:,2) + bmMat(:,9).*pPE(:,3)) + bmMat(:,3).*(bmMat(:,7).*pPE(:,1) + bmMat(:,9).*pPE(:,2) + bmMat(:,10).*pPE(:,3)))/c;
    pPB1(:, 2) = (+bmMat(:,4).*(bmMat(:,5).*pPE(:,1) + bmMat(:,6).*pPE(:,2) + bmMat(:,7).*pPE(:,3)) - bmMat(:,2).*(bmMat(:,7).*pPE(:,1) + bmMat(:,9).*pPE(:,2) + bmMat(:,10).*pPE(:,3)))/c;
    pPB1(:, 3) = (-bmMat(:,3).*(bmMat(:,5).*pPE(:,1) + bmMat(:,6).*pPE(:,2) + bmMat(:,7).*pPE(:,3)) + bmMat(:,2).*(bmMat(:,6).*pPE(:,1) + bmMat(:,8).*pPE(:,2) + bmMat(:,9).*pPE(:,3)) )/c;

end

function boostMatrix = calcBoostMatrix(vel)
    c = Constants.c;
    absVel = sqrt(sum(vel.^2,2));
    if absVel == 0
        boostMatrix = eye(4);
        return;
    end
    gamma = 1/sqrt(1-(absVel/c)^2);
    boostMatrix = [gamma, -gamma*vel(1)/c, -gamma*vel(2)/c, -gamma*vel(3)/c; ...
        -gamma*vel(1)/c, 1+(gamma-1)*(vel(1)/absVel)^2, (gamma-1)*vel(1)*vel(2)/absVel^2 (gamma-1)*vel(1)*vel(3)/absVel^2; ... 
        -gamma*vel(2)/c, (gamma-1)*vel(2)*vel(1)/absVel^2, 1+(gamma-1)*(vel(2)/absVel)^2, (gamma-1)*vel(2)*vel(3)/absVel^2; ...
        -gamma*vel(3)/c, (gamma-1)*vel(3)*vel(1)/absVel^2, (gamma-1)*vel(3)*vel(2)/absVel^2, 1+(gamma-1)*(vel(3)/absVel)^2];
end

function fieldMatrix = genRelFieldMatrix(xE, yE, zE, xB, yB, zB)
    % contravariant matrix form of the electromagnetic tensor
    c = Constants.c;
    fieldMatrix = [0,-xE/c,-yE/c,-zE/c; xE/c,0,-zB,yB; yE/c,zB,0,-xB; zE/c,-yB,xB,0];
end