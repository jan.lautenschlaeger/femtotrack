function [windowInfo] = updateWindow(fieldList, phasespace, margin, velMargin, maxWindowSize, partMass)
    
%% INFO
%  This funktion updates the indices for the windows for each field
%  using the particles position. 

%% IN
% fieldList     - list{firld} - list of m stucteds representing fields
% phasespace    - struct - containign info of n particle
% margin        - (3,2) int - additional cells added to window in x,y,z min/ max direction
% velMargin     - float - if greater 0, add additonal margin in mean
%                            direction of partical movment, scaled by
%                            the mean vel times velMargin
% maxWindowSize - (3,1) int - maximum extend of window in x,y,z direction
%                             in number of cells
% partMass      - float - particle mass (necessary for velocity calculation)

%% OUT
% windowInfo - (3,2,m) int - min,max indices for x,y,z for each field

% AUTHOR: JL 2022

    %% TODO ?
    % - do a window per bunch ?
    % - allow margin and velMargin given in meters ?
    % - allow maxWindowSize given in meters ?

    windowInfo = zeros(3,2,0);
    counter = 1;
    
    %% checks and fix input
    margin = fix(margin);
    maxWindowSize = fix(maxWindowSize);

    % mean vel. vector
    meanMom = sum(phasespace.mom(phasespace.alive,:),1)./size(phasespace.mom,1); % == mean() but faster
    gamma = sqrt(1 + sum(meanMom.^2,2) /(partMass*Constants.c)^2);
    meanVel = meanMom / (partMass * gamma);

    % mean vel. vector --> changed to max (U.N.)
%     maxMom = max(phasespace.mom(phasespace.alive,3)); % max z-momentum
%     gamma = sqrt(1 +  maxMom^2/(partMass*Constants.c)^2);
%     maxVel = maxMom / (partMass * gamma);


    for field = fieldList

        % max/min particle pos.
        insideField = checkInCube(phasespace.pos, field{1}.meshBounds); % TODO: check against window ?

        if sum(insideField) ~= 0
            maxPos = max(phasespace.pos(insideField,:), [], 1);
            minPos = min(phasespace.pos(insideField,:), [], 1);
            
            % calc velocity based margin
            addmargin = fix(velMargin * meanVel ./ field{1}.meanCellSize);
            addmargin = [min(0,addmargin'), max(0,addmargin')];
    
            closestIndex(1,1) = max(1, getClosestIndex(minPos(1), field{1}.mesh.x) - margin(1,1) + addmargin(1,1));
            closestIndex(2,1) = max(1, getClosestIndex(minPos(2), field{1}.mesh.y) - margin(2,1) + addmargin(2,1));
            closestIndex(3,1) = max(1, getClosestIndex(minPos(3), field{1}.mesh.z) - margin(3,1) + addmargin(3,1));
            closestIndex(1,2) = min(field{1}.nx, getClosestIndex(maxPos(1), field{1}.mesh.x) + margin(1,2) + addmargin(1,2));
            closestIndex(2,2) = min(field{1}.ny, getClosestIndex(maxPos(2), field{1}.mesh.y) + margin(2,2) + addmargin(2,2));
            closestIndex(3,2) = min(field{1}.nz, getClosestIndex(maxPos(3), field{1}.mesh.z) + margin(3,2) + addmargin(3,2));

            % check and correct max window limits (TO BE TESTED !)
            exceedLimt = (closestIndex(:,2) - closestIndex(:,1)) - maxWindowSize';
            if any(exceedLimt > 0)
                maxLoops = max(exceedLimt);
                ceneterPos = sum(phasespace.pos(phasespace.alive,:),1)./phasespace.N; % == mean() but faster
                
                for ii = 1:maxLoops % TODO: while loop?
                    exceedLimt = (closestIndex(:,2) - closestIndex(:,1)) - maxWindowSize;
                    windowPos = [field{1}.mesh.x(closestIndex(1,:)); field{1}.mesh.y(closestIndex(2,:)); field{1}.mesh.z(closestIndex(3,:))];
                    windowCenter = sum(windowPos,2)./2;

                    posDiff = [ceneterPos' > windowCenter, ceneterPos' < windowCenter];

                    closestIndex = closestIndex + [(exceedLimt > 0),-1.*(exceedLimt > 0)] .* posDiff;
                end             

            end

            windowInfo(:,:,counter) = closestIndex;
        else
            closestIndex = [-1,-1; -1,-1; -1,-1];
        end

        windowInfo(:,:,counter) = closestIndex;
        counter = counter +1;
    
        % fprintf('[%g,%g][%g,%g][%g,%g]\n',closestIndex(1,1), closestIndex(1,2) ,closestIndex(2,1), closestIndex(2,2) ,closestIndex(3,1), closestIndex(3,2));
    end

end