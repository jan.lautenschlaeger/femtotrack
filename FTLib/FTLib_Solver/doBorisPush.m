function phasespace = doBorisPush(phasespace, partCharge, partMass, partE, partB, dt) 
    
%% INFO
% This function calculates the evolution of the positon and the momentum of 
% charged particles under the influence of a electric and a magnetic field.
% The Boris Push method is used: https://www.particleincell.com/2011/vxb-rotation/

%% IN
% phasespace    - struct - position and momentum of n particles
% partCharge    - float - charge of the particles
% partMass      - float - mass of the particles
% partE         - (n,3) flaot - per particle acting electric field
% partB         - (n,3) flaot - per particle acting magnetic field
% dt            - flaot - time step

%% OUT
% pos - (n,3) float - new position of n particles
% mom - (n,3) float - new momentum of n particles

% AUTHOR: JL 2022

    % TODO: add phasespace.alive filter ???

    % p = gamma*m*v

    % adding half magnetic impulse 
    partMonMinus = phasespace.mom + (partCharge * partE * dt) / 2;
    
    % rotation of speed according to B
    gamma = sqrt(1 + sum(partMonMinus.^2,2) /(partMass.^2 * (Constants.c)^2));
    t = partCharge * dt * partB ./ (2 * partMass * gamma); 
    pd = partMonMinus + myCross(partMonMinus, t);
    
    s = 2 .* t ./ (1 + sum(t.^2,2));
    partMonPlus = partMonMinus + myCross(pd, s);
    
    %adding second half of magnetic impulse
    partMon = partMonPlus + (partCharge * partE * dt) / 2;

    phasespace.pos = phasespace.pos + dt * (partMon ./ (gamma * partMass));
    phasespace.mom = partMon;
end

function res = myCross(a, b)
    res(:,1) = a(:,2).*b(:,3)-a(:,3).*b(:,2);
    res(:,2) = a(:,3).*b(:,1)-a(:,1).*b(:,3);
    res(:,3) = a(:,1).*b(:,2)-a(:,2).*b(:,1);
end