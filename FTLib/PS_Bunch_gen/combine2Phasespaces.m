function [phasespace, bunchInfo] = combine2Phasespaces(ps1, bInfo1, ps2, bInfo2)

%% INFO
% Combine two different phasespaces and the corresponding bunch info into
% one. The combined phasespace has first the ps1 particles and than the ps2
% particles.


%% IN
% ps1       - struct - first phasespace
% bInfo1    - struct - bunchInfo to the first phasespace
% ps2       - struct - second phasespace
% bInfo2    - struct - bunchInfo to the second phasespace

%% OUT
% phasespace    - struct - combined phasespace
% bunchInfo     - struct - combined bunchInfo

% AUTHOR: JL 2022

    %% CHECK
    assert(ps1.N == bInfo1.N, 'ASSERT. Phasespace 1 size mismatch.')
    assert(ps2.N == bInfo2.N, 'ASSERT. Phasespace 2 size mismatch.')
    
    %% COMBINE phasespace 
    phasespace.pos = [ps1.pos;ps2.pos];
    phasespace.mom = [ps1.mom;ps2.mom];
    phasespace.N = ps1.N + ps2.N;
    phasespace.emissionTime = [ps1.emissionTime; ps1.emissionTime];

    %% COMBINE bunchInfo 
    bunchInfo.N = bInfo1.N + bInfo2.N;
    bunchInfo.grouping = [bInfo1.grouping; bInfo2.grouping + bInfo1.grouping(end,2)];

    bunchInfo.numBunches = bInfo1.numBunches + bInfo2.numBunches;
    bunchInfo.bunchSizes = [bInfo1.bunchSizes, bInfo2.bunchSizes];
end