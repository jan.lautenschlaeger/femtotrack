function bunchInfo = genEqualParticleNumberDist(NB, M)
    
%% INFO
% Generate the bunch information for bunches with the same amount of
% particles in each one.

%% IN
% NB    - int - number of particles per bunch
% M     - int - number of bunches

%% OUT
% bunchInfo - struct - partical grouping information

% bunchInfo
%   .N - int - number of particles total
%   .grouping - (M, 2) int - start and end index of each bunch 
%   .numBunches - int - number of bunches
%   .bunchSizes - (M,1) int - size of each bunch

% AUTHOR: JL 2022

    NB = abs(fix(NB));
    M = abs(fix(M));

    N = NB * M;
    bunchInfo.N = N;
    bunchInfo.grouping = 1:NB:N;
    bunchInfo.grouping = [bunchInfo.grouping', (bunchInfo.grouping + NB-1)'];

    bunchInfo.numBunches = M;
    bunchInfo.bunchSizes = repmat(NB, M, 1);

    fprintf('INFO. total particles = %i\n', bunchInfo.N);
end