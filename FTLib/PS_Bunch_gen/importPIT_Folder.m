function [phasespace, bunchInfo] = importPIT_Folder(folderpathList, transPos)

%% INFO
% Import all .pit files from one or multiple folders.
% Returns the generated phasespace and bunchInfo structs.
% All .pit files are combinted into one phasespace. 
% The bunch inforamtion is generated from the seperation into the seperate PIT files. 

%% IN
% folderpathList     - string or cell array<string> - folder path
% transPos = [0,0,0] - (1,3) float - x,y,z translation of the position

%% OUT
% phasespace - struc - constructed particle phasespace
% bunchInfo - struc - file-base bunch information

% AUTHOR: JL 2022

%% EX
% IMPORT STRING
% [phasespace, bunchInfo] = importPIT([pwd '\pitFolder'])           - import all pit files from 'pitFolder' folder
% [phasespace, bunchInfo] = importPIT([pwd '\pitFolder'], [0,0,0])  - same as above

% IMPORT CELL ARRAY 
% [phasespace, bunchInfo] = importPIT({'.\pitFolder1'})                  - import all PIT files in pitFolder1
% [phasespace, bunchInfo] = importPIT({'.\pitFolder1','.\pitFolder1'})   - import all PIT files in pitFolder1 twice
% [phasespace, bunchInfo] = importPIT({'.\pitFolder1','.\pitFolder2'})   - import all PIT files in pitFolder1 and pitFolder2

%% INFO PIT-file
% Use always SI units.
% The momentum (mom) is equivalent to beta * gamma.
% Columns: pos_x  pos_y  pos_z  mom_x  mom_y  mom_z  mass  charge  charge(macro)  time


    % set default values, when not given
    if nargin < 2, transPos = [0,0,0]; end
    
    %% CHECK
    if ~iscell(folderpathList)
        folderpathList = {folderpathList};
    end
    for filepath = folderpathList
        if ~isfolder(filepath{1})
            error('ERROR. Given input is ot a folder.')
        end
    end

    [phasespace, bunchInfo] = importPIT(folderpathList, transPos);
end