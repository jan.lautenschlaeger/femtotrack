function [phasespace] = genRandInitPhasespace(N, initPosRange, initMomRange, initEmissionTimeRange)
    tic;
%% INFO
% Generates an equally distributed random phasespace with in min/max limits for the position and
% momentum. 
% All particles start at t=0

% phasespace
%   .N              - int - total number of particles
%   .pos            - (N,3) float - x,y,z posiotn
%   .mom            - (N,3) float - x,y,z rel momentum
%   .emissionTime   - (N,1) float - time a particle gets added to the sim

%% IN
% N                                  - int - number of generated particles
% initPosRange                       - (3, 2) float - x,y,z ranges to generate initial position of particles in m
% initMomRange                       - (3, 2) float - x,y,z ranges to generate initial momentum of particles in kg*m/s
% initEmissionTimeRange = zeros(N,1) - (1,2) float - rang of emission times in sec

%% OUT
% phasespace - struct - full particle phasespace discription

% AUTHOR: JL 2022

%% EX
% phasespace = genRandInitPhasespace(10, [0,1;0,1;0,1], [0.1,0.11;0.1,0.11;0.1,0.11])        - 10 random particle in space and momentum all have the emission time 0 sec
% phasespace = genRandInitPhasespace(10, [0,1;0,1;0,1], [0.1,0.11;0.1,0.11;0.1,0.11], [0,1]) - 10 random particle in space, momentum and emission time

    % set default values, when not given
    if nargin < 4, initEmissionTimeRange = false; end

    N = abs(fix(N));

    phasespace.N = N;
    phasespace.pos = rand(N, 3) .* (initPosRange(:, 2) - initPosRange(:, 1))' + initPosRange(:,1)'; % at spawn
    phasespace.mom = rand(N, 3) .* (initMomRange(:, 2) - initMomRange(:, 1))' + initMomRange(:,1)'; % at spawn
    
    if initEmissionTimeRange == false
        phasespace.emissionTime = zeros(N,1);
    else
        assert((initEmissionTimeRange(1) >= 0) & (initEmissionTimeRange(2) > 0), 'ASSERT. Particle spawn time range has to be greater equal Zero.')

        phasespace.emissionTime = rand(N, 1) .* (initEmissionTimeRange(2) - initEmissionTimeRange(1)) + initEmissionTimeRange(1);
    end

    % p = m0*v/√(1 – v^2/c^2) = m0*v/√(1 – beta^2)

    fprintf('INFO. Gen Phasespace Time: %g sec\n', toc);
end