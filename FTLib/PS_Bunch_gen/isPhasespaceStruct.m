function isPhasespace = isPhasespaceStruct(ps)
%% INFO
% Check if input is a valid Phasespace struct for femtoTrack, aka the struct has all
% necessary fields. 

%% IN
% ps - struct - phasespace to check

%% OUT
% isPhasespace - boolean - true if it is a struct representig a phasespace in femtoTrack

% AUTHOR: JL 2022

    isPhasespace = true;
    isPhasespace = isPhasespace & isfield(ps, 'N');
    isPhasespace = isPhasespace & isfield(ps, 'pos');
    isPhasespace = isPhasespace & isfield(ps, 'mom');
    isPhasespace = isPhasespace & isfield(ps, 'emissionTime');
end