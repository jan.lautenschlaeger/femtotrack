function [phasespace, bunchInfo] = combineNPhasespaces(psList, bInfoList)

%% INFO
% Combine N different phasespaces and the corresponding bunch info into
% one. The combined phasespace is orderd in the same way as the order in
% the lists.

%% IN
% psList       - list{struct} - list of phasespaces
% bInfoList    - list{struct} - list of nuch infos matching to psList

%% OUT
% phasespace    - struct - combined phasespace
% bunchInfo     - struct - combined bunchInfo

% AUTHOR: JL 2022

    phasespace = psList{1};
    bunchInfo = bInfoList{1};

    for ii = 2:numel(psList)
        [phasespace, bunchInfo] = combine2Phasespaces(phasespace, bunchInfo, psList{ii}, bInfoList{ii});
    end
end