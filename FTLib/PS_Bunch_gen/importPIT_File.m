function [phasespace, bunchInfo] = importPIT_File(filepathList, transPos)

%% INFO
% Import a .pit file or list of pit files.
% Returns the generates phasespace and bunchInfo structs.
% The bunch inforamtion is generated from the seperation 
% into the files. 

%% IN
% filepath           - string or cell array<string> - .pit file path or folder path
% transPos = [0,0,0] - (1,3) float - x,y,z translation of the position

%% OUT
% phasespace - struc - constructed particle phasespace
% bunchInfo  - struc - file-base bunch information

% AUTHOR: JL 2022

%% EX
% IMPORT STRING
% [phasespace, bunchInfo] = importPIT_File([pwd '\file.pit'])            - import file.pit from pwd folder 
% [phasespace, bunchInfo] = importPIT_File([pwd '\file.pit'], [0,0,0])   - same as above

% IMPORT CELL ARRAY 
% [phasespace, bunchInfo] = importPIT({'.\file1.pit'})                   - import file1.pit 
% [phasespace, bunchInfo] = importPIT({'.\file1.pit','.\file2.pit'})     - import file1.pit and file2.pit

%% INFO PIT-file
% Use always SI units.
% The momentum (mom) is equivalent to beta * gamma.
% Columns: pos_x  pos_y  pos_z  mom_x  mom_y  mom_z  mass  charge  charge(macro)  time

    % set default values, when not given
    if nargin < 2, transPos = [0,0,0]; end

    %% CHECK
    if ~iscell(filepathList)
        filepathList = {filepathList};
    end
    for filepath = filepathList
        if isfile(filepath{1})
            [~,~,ext] = fileparts(filepath{1});
    
            if ext ~= ".pit"
                error('ERROR. Given file is no PIT file')
            end
        else
            error('ERROR. Given input is not a file.')
        end
    end

    [phasespace, bunchInfo] = importPIT(filepathList, transPos);
end