function [phasespace,bunchInfo] = genGaussianBunch(N,x_angle,x_position,dE_eV,dphi_deg)
% Generates one single Gaussian Bunch with N particles and boolean
% run allows for a parameter sweep

% phasespace
%   .N              - int - total number of particles
%   .pos            - (N,3) float - x,y,z posiotn
%   .mom            - (N,3) float - x,y,z rel momentum
%   .emissionTime   - (N,1) float - time a particle gets added to the sim

%bunchinfo
bunchInfo.N=N;
bunchInfo.numBunches=1;
bunchInfo.grouping(1,1)=1;
bunchInfo.grouping(1,2)=N;
bunchInfo.bunchSizes(1,1)=N;



lambda0=2*10^-6;
T_laser=lambda0/Constants.c;

beta0=0.31;

gamma=(1-beta0^2)^(-1/2);
WtotJ=gamma*Constants.eMass*Constants.c^2;
p0=( WtotJ^2/Constants.c^2 -Constants.eMass^2 * Constants.c^2 )^0.5;

E0_ev=gamma*Constants.eMass_eV;
Ekin_ev=(gamma-1)*Constants.eMass_eV;

CoastingBeamPeriods=0;
stddev.CoastingBeamEspread=1;%eV

offsets.z_pos=0;
offsets.x_pos=x_position;
offsets.y_pos=0;
offsets.offset_E_ev=40;
offsets.x_angle=x_angle;%1e-5/(beta0*gamma) * (run-1);
offsets.y_angle=0;

CS.alpha_hat_x=0;
CS.beta_hat_x=32e-6;
CS.gamma_hat_x=1/CS.beta_hat_x;
CS.emittance_x=10e-12;

CS.alpha_hat_y=0;
CS.beta_hat_y=32e-6;
CS.gamma_hat_y=1/CS.beta_hat_y;
CS.emittance_y=10e-12;

CS.beta_hatZ=12e-6;
stddev.sigma_z=10e-9;

arge1_deg=125; %center at z=0 at
Phi_S_init_deg=240;
%%%%%%%%%%%%


stddev.sigma_E_ev=10;

stddev.alpha_x=CS.alpha_hat_x*sqrt(CS.emittance_x/CS.beta_hat_x);   % Focusing Angle
stddev.zeta_x=sqrt(CS.gamma_hat_x*CS.emittance_x);              % uncorrelated angle spread
stddev.sigma_x=sqrt(CS.beta_hat_x*CS.emittance_x);              % uncorrelated position spread  

stddev.alpha_y=CS.alpha_hat_y*sqrt(CS.emittance_y/CS.beta_hat_y);   % Focusing Angle
stddev.zeta_y=sqrt(CS.gamma_hat_y*CS.emittance_y);              % uncorrelated angle spread
stddev.sigma_y=sqrt(CS.beta_hat_y*CS.emittance_y);              % uncorrelated position spread  

stddev.sigma_zp=1/(beta0^2*gamma^2) *stddev.sigma_E_ev/E0_ev;
offsets.zp_offset=1/(beta0^2*gamma^2) *offsets.offset_E_ev/E0_ev;

stddev.sigma_t=stddev.sigma_z/(beta0*Constants.c);





    if CoastingBeamPeriods==0
        %bunched beam
        %offsets.t0=T_laser*(-Phi_S_init_deg+arge1_deg+360)/360; %oll!
        offsets.t0=T_laser*(Phi_S_init_deg-arge1_deg+dphi_deg+180)/360;   %signs corrected (180°for z=0 at center for individual cells)
        stddev.sigma_E_ev=Constants.eMass_eV*beta0^2*gamma^3 * stddev.sigma_z/CS.beta_hatZ;    %matched  
    else
        offsets.t0=0;
        stddev.sigma_E_ev=stddev.CoastingBeamEspread;
    end


    R.x=stddev.sigma_x*randn(N,1)+offsets.x_pos;    
    R.xp=stddev.alpha_x/stddev.sigma_x *R.x + stddev.zeta_x*randn(N,1)+offsets.x_angle; 

    R.y=stddev.sigma_y*randn(N,1)++offsets.y_pos; 
    R.yp=stddev.alpha_y/stddev.sigma_y *R.y + stddev.zeta_y*randn(N,1)+offsets.y_angle; 

    R.z=offsets.z_pos;
    
    R.zp=ones(N,1);
    R.zp=R.zp+offsets.zp_offset+stddev.sigma_zp*randn(N,1); 


    if CoastingBeamPeriods==0
        %bunched
        R.time=offsets.t0+stddev.sigma_t*randn(N,1);
    else
        %coasting
        %time_interval_between_particles=T_laser/N_particles;
        %time=5* T_laser/N_particles *(1:N_particles)';
        R.time=offsets.t0+CoastingBeamPeriods* T_laser * rand(N,1);
    end




    phasespace.N=N;
    
%     phasespace.pos(:,1)=stddev.sigma_x*randn(N,1)+offsets.x_pos;
%     phasespace.pos(:,2)=stddev.sigma_y*randn(N,1)+offsets.y_pos;
%     phasespace.pos(:,3)=0;
%     
%     phasespace.mom(:,1)=stddev.alpha_x/stddev.sigma_x * (phasespace.pos(:,1)-offsets.x_pos) + stddev.zeta_x*randn(N,1)+offsets.x_angle*p0;
%     phasespace.mom(:,2)=stddev.alpha_y/stddev.sigma_y * (phasespace.pos(:,1)-offsets.y_pos) + stddev.zeta_y*randn(N,1)+offsets.y_angle*p0;
%     phasespace.mom(:,3)=p0;


    %% TODO - UWE!
    phasespace.emissionTime = R.time;
    phasespace.pos(:,1)=R.x;
    phasespace.pos(:,2)=R.y;
    phasespace.pos(:,3)=R.z;

    phasespace.mom(:,1)=R.xp*p0;
    phasespace.mom(:,2)=R.yp*p0;
    phasespace.mom(:,3)=R.zp*p0;

end