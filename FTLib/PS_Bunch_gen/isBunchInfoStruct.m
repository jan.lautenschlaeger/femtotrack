function isBunchInfo = isBunchInfoStruct(bI)
%% INFO
% Check if input is a valid field struct for femtoTrack, aka the struct has all
% necessary fields. 

%% IN
% field - struct - field to check

%% OUT
% isField - boolean - true if it is a struct representig a field in femtoTrack

% AUTHOR: JL 2022

    isBunchInfo = true;
    isBunchInfo = isBunchInfo & isfield(bI, 'N');
    isBunchInfo = isBunchInfo & isfield(bI, 'grouping');
    isBunchInfo = isBunchInfo & isfield(bI, 'numBunches');
    isBunchInfo = isBunchInfo & isfield(bI, 'bunchSizes');
end