function [phasespace, bunchInfo] = importPIT(filepathList, transPos, useEmissionTime, printAllFileNames)

%% INFO
% Import a .pit file, a folder with PIT files or a list of PIT-files and folders.
% This function returns the phasespace and corresponding bunchInfo structs imported from PIT files. 
% If a folder path is given, all .pit files are combinted into one 
% phasespace. The bunch inforamtion is generated from the seperation 
% into the files.

%% IN
% filepathList              - string or cell array<string> - .pit file path, folder path or list of files and folders
% transPos = [0,0,0]        - (1,3) float - x,y,z translation of the position for all particles
% useSpawnTime = true       - boolean - use the emission time information from the PIT file
% printAllFileNames = false - boolean - if true print list of all file names with additonal info

%% OUT
% phasespace    - struc - imported particle phasespace from PIT all PIT files
% bunchInfo     - struc - file-based bunch information

% AUTHOR: JL 2022

%% EX
% IMPORT STRING
% [phasespace, bunchInfo] = importPIT([pwd '\file.pit'])            - import file.pit in pwd folder (gen 1 bunch)
% [phasespace, bunchInfo] = importPIT([pwd '\file.pit'], [0,0,0])   - same as above
% [phasespace, bunchInfo] = importPIT([pwd '\pitFolder'])           - import all pit files from 'pitFolder' folder
% [phasespace, bunchInfo] = importPIT([pwd '\pitFolder'], [0,0,0])  - same as above

% IMPORT CELL ARRAY 
% [phasespace, bunchInfo] = importPIT({'.\file1.pit'})                   - import file1.pit (gen 1 bunch)
% [phasespace, bunchInfo] = importPIT({'.\file1.pit','.\file2.pit'})     - import file1.pit and file2.pit (gen 2 bunches)
% [phasespace, bunchInfo] = importPIT({'.\pitFolder1'})                  - import all PIT files in pitFolder1
% [phasespace, bunchInfo] = importPIT({'.\pitFolder1','.\pitFolder1'})   - import all PIT files in pitFolder1 twice
% [phasespace, bunchInfo] = importPIT({'.\pitFolder1','.\pitFolder2'})   - import all PIT files in pitFolder1 and pitFolder2
% [phasespace, bunchInfo] = importPIT({'.\file1.pit','.\pitFolder1'})    - import file1.pit and all PIT files in pitFolder1

%% INFO PIT-file
% Use always SI units.
% The momentum (mom) is equivalent to beta * gamma.
% Columns: pos_x  pos_y  pos_z  mom_x  mom_y  mom_z  mass  charge  charge(macro)  time


    % set default values, when not given
    if nargin < 2, transPos = [0,0,0]; end
    if nargin < 3, useEmissionTime = true; end
    if nargin < 4, printAllFileNames = false; end

    fullFileNames = {};
    if ~iscell(filepathList)
        filepathList = {filepathList};
    end
    
    %% gen full path to all files
    for filepath = filepathList
        if isfolder(filepath{1})
            tempFileName = dir( fullfile(filepath{1},'*.pit'));
            fullFileNames = [fullFileNames, fullfile(filepath{1},{tempFileName.name})];
        elseif isfile(filepath{1})
            fullFileNames = [fullFileNames, filepath];
            [~,~,ext] = fileparts(filepath{1});
    
            if ext ~= ".pit"
                error('ERROR. no PIT file')
            end
        else
            error('ERROR. Import pit file')
        end
    end

    %% import settings
    varNames = {'pos_x','pos_y','pos_z','mom_x','mom_y','mom_z','mass','charge','charge_macro', 'time'} ;
    varTypes = {'double','double','double','double','double','double','double','double','double','double'} ;
    delimiter = ' ';
    dataStartLine = 5;
    extraColRule = 'ignore';
        
    opts = delimitedTextImportOptions('VariableNames',varNames,...
                                    'VariableTypes',varTypes,...
                                    'Delimiter',delimiter,...
                                    'DataLines', dataStartLine,...
                                    'ExtraColumnsRule',extraColRule); 

    N = 0;
    posData = zeros(0,3);
    momData = zeros(0,3);
    massData = zeros(0,1);
    chargeData = zeros(0,2);
    timeData = zeros(0,1);

    bunchInfo.grouping = zeros(0,2);
    bunchInfo.bunchSizes = zeros(0,1);
    bunchInfo.numBunches = 0;

    for filename = fullFileNames
        data = readmatrix(filename{1}, opts);

        N = N + size(data,1);
        posData = [posData; data(:,1),data(:,2),data(:,3)];
        momData = [momData; data(:,4),data(:,5),data(:,6)];
        massData = [massData; data(:,7)];
        chargeData = [chargeData; data(:,8),data(:,9)];
        timeData = [timeData; data(:,10)];

        bunchInfo.grouping = [bunchInfo.grouping; N-size(data,1)+1, N];
        bunchInfo.bunchSizes = [bunchInfo.bunchSizes; size(data,1)];
        bunchInfo.numBunches = bunchInfo.numBunches +1;

        if printAllFileNames
            fprintf('%s PIT File Import: %s (%g particles)\n', datestr(now, 'HH:MM:SS'), filename{1}, size(data,1));
        end
    end
    
    phasespace.N = N;
    phasespace.pos = posData + transPos;
    momScaling = massData .* Constants.c;
    phasespace.mom = momScaling .* momData;
    % phasespace.alive = true(N,1);   

    bunchInfo.N = N;

    if useEmissionTime
        phasespace.emissionTime = timeData;
    else
        phasespace.emissionTime = zeros(N,1);
    end

    if size(fullFileNames,2) > 1
        fprintf('%s INFO. %i PIT files imported. (Total particles: %i)\n', datestr(now, 'HH:MM:SS'), size(fullFileNames,2), N);
    else
        fprintf('%s INFO. %i PIT file imported. (Total particles: %i)\n', datestr(now, 'HH:MM:SS'), size(fullFileNames,2), N);
    end
end