function [phasespace] = genPhasespaceOnNanotip(outDir, N, c_theta, sigma_E)
%% INFO
% Generates a phasespace on a hemisphere
% with a cosine angle distribution
% and a gaussian energy distribution.

% phasespace
%   .N              - int - total number of particles
%   .pos            - (N,3) float - x,y,z posiotn
%   .mom            - (N,3) float - x,y,z rel momentum
%   .emissionTime   - (N,1) float - time a particle gets added to the sim

%% IN
% outDir -
% N - int - number of particles

%% OUT
% phasespace - struct - inital particle information

% AUTHOR: AB 2022

    %%
    tic;
    if ~exist(outDir, 'dir')
       mkdir(outDir)
    end

    %% constants
    c_0 = 299792458;  % speed of light
    m_e = 9.1093837015e-31;  % electron mass
    q = 1.602176634e-19;  % electron charge magnitude

    %% settings
    debug_mode = false;  % to debug or not to debug
    save_video = false;  % Save plots as animation

    r0 = 20.5e-9;  % radius of emission hemisphere (emitter tip + some distance)
    z0 = 400e-9;  % elevation of emission hemisphere center

    %c_theta = 0.7;  % c = 14.2 / theta_rms. Use (theoretically) 0.512668 for cos(theta)-dist, 0.696455 for cos^2(theta)-dist
    % theta_rms of cos(theta)-dist:     sqrt(1/2 * (pi^2 - 8))
    %   -> theta_rms ~= 55.3965° ~= 2 * 27.6983°
    % theta_rms of cos^2(theta)-dist:   sqrt(1/24 * pi * (pi^2 - 6))
    %   -> theta_rms ~= 40.7779° ~= 2 * 20.389°

    mean_E = 1.5;  % mean Energy in eV
    %sigma_E = 0.3;  % standard deviation of energy in eV. Use 0.3 for Glassbox, 1.5 for Shoebox
    
    sigma_t = 140 / 2.355 * 10^-15;  % TODO update if needed
%    sigma_t = 0;

    %% creating phasespace
    % coordinates
    phi = 2 * pi * rand(N, 1);  % azimuth angle
    theta = c_theta * randCos(N);  % polar angle
    if debug_mode
        theta_rms = rad2deg(population_variance(theta) .^ 0.5);
        fprintf('DEBUG. theta_rms: %g° or 2 * %g°\n', theta_rms, theta_rms / 2);
    end
    % unity vectors
    e_x = sin(theta) .* cos(phi);
    e_y = sin(theta) .* sin(phi);
    e_z = (1 - e_x .^ 2 - e_y .^ 2) .^ 0.5;
    norm_e = sqrt(e_x .^ 2 + e_y .^ 2 + e_z .^ 2);

    % energies
    E_kin = (mean_E + sigma_E * randn(N, 1)) * (q);  % E in Joule

    % Easily generating a truncated normal distribution is not possible without
    % the (non-free) module 'makedist'
    % (https://de.mathworks.com/help/stats/prob.normaldistribution.truncate.html),
    % so we just take another sample if E is negative or equal zero.
    ii = 1;
    while ii <= length(E_kin)
        if (E_kin(ii) <= 0)
            E_kin(ii) = (mean_E + sigma_E * randn(1, 1)) * (q);
            ii = ii - 1;
        end
        ii = ii + 1;
    end

    % momenta (p = sqrt(T^2 / c^2 + 2 T m_0) = beta gamma m_0 c)
    p_x = e_x .* sqrt(E_kin .^2 / c_0 ^ 2 + 2 .* E_kin * m_e);
    p_y = e_y .* sqrt(E_kin .^2 / c_0 ^ 2 + 2 .* E_kin * m_e);
    p_z = e_z .* sqrt(E_kin .^2 / c_0 ^ 2 + 2 .* E_kin * m_e);
    norm_p = sqrt(p_x .^ 2 + p_y .^ 2 + p_z .^ 2);
    
    % emission times
    time = sigma_t * randn(N, 1);
    time = time + abs(min(time)); 

    % putting phasespace together
    phasespace.N = N;
    phasespace.pos = [e_x * r0, e_y * r0, e_z * r0 + z0];
    phasespace.mom = [p_x, p_y, p_z];
    % phasespace.alive = true(N, 1);
    phasespace.emissionTime = time;

    %%
    if debug_mode
        bins = 50;
        fprintf(['DEBUG. phi = ' repmat('%g ', 1, numel(phi)) '\n'], phi)
        fprintf(['DEBUG. theta = ' repmat('%g ', 1, numel(theta)) '\n'], theta)
        figure('Name', 'phi');
        hist(phi, bins)
        figure('Name', 'theta');
        hist(theta, bins)
        hold on
        x = - pi/2 : 0.1 : pi/2;
        plot(c_theta * x, N/bins * pi/2 * cos(x))
        hold off

        fprintf(['DEBUG. e_x = ' repmat('%g ', 1, numel(e_x)) '\n'], e_x)
        fprintf(['DEBUG. e_y = ' repmat('%g ', 1, numel(e_y)) '\n'], e_y)
        fprintf(['DEBUG. e_z = ' repmat('%g ', 1, numel(e_z)) '\n'], e_z)
        fprintf(['DEBUG. norm_e = ' repmat('%g ', 1, numel(norm_e)) '\n'], norm_e)
        figure('Name', 'e_x');
        hist(e_x, bins)
        figure('Name', 'e_y');
        hist(e_y, bins) 
        figure('Name', 'e_z');
        hist(e_z, bins)

        fprintf(['DEBUG. E_kin = ' repmat('%g ', 1, numel(E_kin)) '\n'], E_kin)
        figure('Name', 'E_kin');
        hist(E_kin / q, bins)

        fprintf(['DEBUG. p_x = ' repmat('%g ', 1, numel(p_x)) '\n'], p_x)
        fprintf(['DEBUG. p_y = ' repmat('%g ', 1, numel(p_y)) '\n'], p_y)
        fprintf(['DEBUG. p_z = ' repmat('%g ', 1, numel(p_z)) '\n'], p_z)
        fprintf(['DEBUG. norm_p = ' repmat('%g ', 1, numel(norm_p)) '\n'], norm_p)
        figure('Name', 'p_x');
        hist(p_x, bins)
        figure('Name', 'p_y');
        hist(p_y, bins)
        figure('Name', 'p_z');
        hist(p_z, bins)
        figure('Name', 'norm_p');
        hist(norm_p, bins)

        figure('Name', 'phasespace')
        % make a sphere
        latitude = 0.0 : 0.1 : pi / 2;
        longitude = 0 : 0.1 : 2 * pi + 1;
        [Xc, Yc] = meshgrid(longitude, latitude);
        xc = r0 .* sin(Xc) .* cos(Yc);
        yc = r0 .* cos(Xc) .* cos(Yc);
        zc = r0 .* sin(Yc) + z0;
        mysurface = surf(xc, yc, zc);
        set(mysurface, 'FaceColor', [0.5 0.5 0.5], 'FaceAlpha', 0.9, 'FaceLighting', 'gouraud', 'EdgeColor', 'none')
        %
        hold on
        % coordinates
        plot3(e_x * r0, e_y * r0, e_z * r0 + z0, '.');
        % velocity vectors
        quiver3(e_x * r0, e_y * r0, e_z * r0 + z0, p_x, p_y, p_z)
        hold off
        axis tight
        xlabel('x [m]')
        ylabel('y [m]')
        zlabel('z [m]')
        box on

        if save_video
            frame = getframe(1);
            im = frame2im(frame);
            [imind, cm] = rgb2ind(im, 64);
            imwrite(imind, cm, [outDir '\' 'Video.gif'], 'gif', 'Loopcount', inf);
        end
    end

    fprintf('INFO. Gen Phasespace Time: %g sec\n', toc);
end



function [at] = randCos(n)
    % Create Random Cosine Dist f = 0.5 cos(theta) in the interval (-pi / 2, pi / 2)
    unif = 2 * (rand(n, 1) - 0.5);
    at = asin(unif);
end



function var = population_variance(a, b)
    % Compute the second central moment (population variance).
    %
    % Math here https://en.wikipedia.org/wiki/Variance#Population_variance
    % or here 10.1103/physrevstab.6.034202
    
    n = size(a, 1);
    if nargin < 2
        var = sum(a .^ 2) / n - (sum(a) / n) .^ 2;
    else
        var = sum(a .* b) / n - sum(a) * sum(b) / n ^ 2;
    end
end