function [phasespace] = genPhasespaceOnOpticalAxis(outDir, N)
%% INFO
% Generates a certain symmetric phasespace on a hemisphere.

% phasespace
%   .N
%   .pos
%   .mom

%% IN
% outDir -
% N - int - number of particles

%% OUT
% phasespace - struct - inital particle information

% AUTHOR: ?? 2022

    %%
    tic;
    if ~exist(outDir, 'dir')
       mkdir(outDir)
    end

    %% constants
    c_0 = 299792458;  % speed of light
    m_e = 9.1093837015e-31;  % electron mass
    q = 1.602176634e-19;  % electron charge magnitude

    %% settings

    plot = false;  % to plot or not to plot
    save_video = true;  % Save plots as animation


    r0 = 20.5e-9;  % radius of emission hemisphere (emitter tip + some distance)
    z0 = 400e-9;  % elevation of emission hemisphere center

    Delta_theta = 0.7;

    mean_E = 1.5;  % mean Energy in eV
    sigma_E = 0.3;  % standard deviation of energy in eV

    %% creating phasespace
    phi = zeros(N, 1);  % azimuth angle
    theta = zeros(N, 1);  % polar angle
    % unity vectors
    e_x = sin(theta) .* cos(phi);
    e_y = sin(theta) .* sin(phi);
    e_z = (1 - e_x .^ 2 - e_y .^ 2) .^ 0.5;
    norm_e = sqrt(e_x .^ 2 + e_y .^ 2 + e_z .^ 2);

    % energy distribution
    E_kin = (mean_E + sigma_E * randn(N, 1)) * (q);  % E in Joule

    % Easily generating a truncated normal distribution is not possible without
    % the (non-free) module 'makedist'
    % (https://de.mathworks.com/help/stats/prob.normaldistribution.truncate.html),
    % so we just take another sample if E is negative or equal zero.
    ii = 1;
    while ii <= length(E_kin)
        if (E_kin(ii) <= 0)
            E_kin(ii) = (mean_E + sigma_E * randn(1, 1)) * (q);
            ii = ii - 1;
        end
        ii = ii + 1;
    end

    % particle momentum (p = sqrt(T^2 / c^2 + 2 T m_0) = beta gamma m_0 c)
    p_x = e_x .* sqrt(E_kin .^2 / c_0 ^ 2 + 2 .* E_kin * m_e);
    p_y = e_y .* sqrt(E_kin .^2 / c_0 ^ 2 + 2 .* E_kin * m_e);
    p_z = e_z .* sqrt(E_kin .^2 / c_0 ^ 2 + 2 .* E_kin * m_e);
    norm_p = sqrt(p_x .^ 2 + p_y .^ 2 + p_z .^ 2);

    %
    phasespace.N = N;
    phasespace.pos = [e_x * r0, e_y * r0, e_z * r0 + z0];
    phasespace.mom = [p_x, p_y, p_z];
    phasespace.alive = true(N, 1);

    %%
    if plot
        fprintf(['DEBUG. phi = ' repmat('%g ', 1, numel(phi)) '\n'], phi)
        fprintf(['DEBUG. theta = ' repmat('%g ', 1, numel(theta)) '\n'], theta)
        figure('Name', 'phi');
        hist(phi, 50)
        figure('Name', 'theta');
        hist(theta, 50)

        fprintf(['DEBUG. e_x = ' repmat('%g ', 1, numel(e_x)) '\n'], e_x)
        fprintf(['DEBUG. e_y = ' repmat('%g ', 1, numel(e_y)) '\n'], e_y)
        fprintf(['DEBUG. e_z = ' repmat('%g ', 1, numel(e_z)) '\n'], e_z)
        fprintf(['DEBUG. norm_e = ' repmat('%g ', 1, numel(norm_e)) '\n'], norm_e)
        figure('Name', 'e_x');
        hist(e_x, 50)
        figure('Name', 'e_y');
        hist(e_y, 50) 
        figure('Name', 'e_z');
        hist(e_z, 50)

        fprintf(['DEBUG. E_kin = ' repmat('%g ', 1, numel(E_kin)) '\n'], E_kin)
        figure('Name', 'E_kin');
        hist(E_kin / q, 50)

        fprintf(['DEBUG. p_x = ' repmat('%g ', 1, numel(p_x)) '\n'], p_x)
        fprintf(['DEBUG. p_y = ' repmat('%g ', 1, numel(p_y)) '\n'], p_y)
        fprintf(['DEBUG. p_z = ' repmat('%g ', 1, numel(p_z)) '\n'], p_z)
        fprintf(['DEBUG. norm_p = ' repmat('%g ', 1, numel(norm_p)) '\n'], norm_p)
        figure('Name', 'p_x');
        hist(p_x, 50)
        figure('Name', 'p_y');
        hist(p_y, 50)
        figure('Name', 'p_z');
        hist(p_z, 50)
        figure('Name', 'norm_p');
        hist(norm_p, 50)

        figure('Name', 'phasespace')
        % make a sphere
        latitude = 0.0 : 0.1 : pi / 2;
        longitude = 0 : 0.1 : 2 * pi + 1;
        [Xc, Yc] = meshgrid(longitude, latitude);
        xc = r0 .* sin(Xc) .* cos(Yc);
        yc = r0 .* cos(Xc) .* cos(Yc);
        zc = r0 .* sin(Yc) + z0;
        mysurface = surf(xc, yc, zc);
        set(mysurface, 'FaceColor', [0.5 0.5 0.5], 'FaceAlpha', 0.9, 'FaceLighting', 'gouraud', 'EdgeColor', 'none')
        %
        hold on
        % coordinates
        plot3(e_x * r0, e_y * r0, e_z * r0 + z0, '.');
        % velocity vectors
        quiver3(e_x * r0, e_y * r0, e_z * r0 + z0, p_x, p_y, p_z)
        hold off
        axis tight
        xlabel('x [m]')
        ylabel('y [m]')
        zlabel('z [m]')
        box on

        if save_video
            frame = getframe(1);
            im = frame2im(frame);
            [imind, cm] = rgb2ind(im, 64);
            imwrite(imind, cm, [outDir '\' 'Video.gif'], 'gif', 'Loopcount', inf);
        end
    end

    fprintf('INFO. Gen Phasespace Time: %g sec\n', toc);
end



function [at] = randCos(n)
% Create Random Cosine Dist f = 0.5 cos(theta) in the interval (-pi / 2, pi / 2)
unif = 2 * (rand(n, 1) - 0.5);
at = asin(unif);
end


