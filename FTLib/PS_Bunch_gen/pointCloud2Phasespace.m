function phasespace = pointCloud2Phasespace(pos, mom, emissionTime)
%% INFO
% Genrat a phasespace struct from a given point cloud. If momentum or
% emission times are not given, there are defaulted to zero. 

% phasespace
%   .N              - int - total number of particles
%   .pos            - (N,3) float - x,y,z posiotn
%   .mom            - (N,3) float - x,y,z rel momentum
%   .emissionTime   - (N,1) float - time a particle gets added to the sim

%% IN
% pos - (N,3) float -                       position of point cloud
% mom = zeros(N,3) - (N,3) float -          rel. momentum
% emissionTime = zeros(N,1) - (N,1) float - emission times

%% OUT
% phasespace - struct - full particle phasespace discription

% AUTHOR: JL 2022

    % set default values, when not given
    if nargin < 2, mom = zeros(size(pos,1),3); end
    if nargin < 3, emissionTime = zeros(size(pos,1),1); end

    assert(numel(pos) == numel(mom), 'ASSERT. Phase space generation input size mismatch.');
    assert(size(pos,1) == size(emissionTime,1), 'ASSERT. Phase space generation input size mismatch.');

    phasespace.N = size(pos, 1);
    phasespace.pos = pos;
    phasespace.mom = mom;
    phasespace.emissionTime = emissionTime;
end