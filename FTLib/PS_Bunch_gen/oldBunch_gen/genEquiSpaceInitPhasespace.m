function [phasespace] = genEquiSpaceInitPhasespace(N, initPosRange, initMomRange, initEmissionTimeRange)
    tic;
%% INFO
% Genertes a phasespace partilce with equal position spacing and random
% momentum between min/ max. 

% phasespace
%   .N              - int - total number of particles
%   .pos            - (N,3) float - x,y,z posiotn
%   .mom            - (N,3) float - x,y,z rel momentum
%   .emissionTime   - (N,1) float - time a particle gets added to the sim

%% IN
% N             - int - number pf generated particles
% initPosRange  - (3,2) float - x,y,z ranges to generate inital positon of particles in m
% initMomRange  - (3,2) float - x,y,z ranges to generate inital momentum of particles in kg*m/s
% initEmissionTimeRange = zeros(N,1) - (1,2) float - rang of emission times in sec

%% OUT
% phasespace - struct - inital position and momentum information

% AUTHOR: JL 2022

%% EX
% phasespace = genEquiSpaceInitPhasespace(10, [0,1;0,1;0,1], [0.1,0.11;0.1,0.11;0.1,0.11])        - 10 particle with equal spaceing and random momentum all have the emission time 0 sec
% phasespace = genEquiSpaceInitPhasespace(10, [0,1;0,1;0,1], [0.1,0.11;0.1,0.11;0.1,0.11], [0,1]) - 10 particle with equal spaceing, random momentum and emission time


    % set default values, when not given
    if nargin < 4, initEmissionTimeRange = false; end

    N = abs(fix(N));
    equiN = fix(N/3);
    diffN = N-equiN;

    spacingX = (0:(equiN-1))/(equiN-1);
    spacingY = (0:(equiN-1))/(equiN-1);
    spacingZ = (0:((equiN+diffN)-1))/(diffN+equiN-1);

    spacingX = ones(1,N);
    spacingY = ones(1,N);
    spacingZ = (0:(N-1))/(N-1);
    spacing = [spacingX', spacingY', spacingZ'];

    phasespace.N = N;
    phasespace.pos = spacing .* (initPosRange(:,2) - initPosRange(:,1))' + initPosRange(:,1)';
    phasespace.mom = rand(N,3) .* (initMomRange(:,2) - initMomRange(:,1))' + initMomRange(:,1)';
    
    if initEmissionTimeRange == false
        phasespace.emissionTime = zeros(N,1);
    else
        assert((initEmissionTimeRange(1) > 0) & (initEmissionTimeRange(2) > 0), 'ASSERT. Particle spawn time range has to be greater Zero.')
        phasespace.emissionTime = rand(N, 1) .* (initEmissionTimeRange(2) - initEmissionTimeRange(1)) + initEmissionTimeRange(1);
    end

    fprintf('INFO. Gen EquiSpace Phasespace Time: %g sec\n', toc);
end