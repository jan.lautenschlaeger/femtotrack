function [phasespace, bunchInfo] = genCollisionPhasespace(M, NB, dist, spread, initMomY)
    
%% INFO
% This function generates the phasespace and bunch info for the
% experimantal setup of a resting particale at (0,0,0) and a group of
% particales moving in its direction.

%% IN
% M         - int - number for bunches
% NB        - int - number moving particles; each bunch has NB+1 particles
% dist      - float - distance in y of the moving particales
% spread    - float - size of x distribution of moving particales from -spread/2 to spread/2
% initMomY  - float - inital y momentum

%% OUT
% phasespace    - struct - inital phasespace for all particles
% bunchInfo     - struct - coresponding bunch info
 
% AUTHOR: JL 2022

    tic;
    M = abs(fix(M));
    N = abs(fix(M+M*NB));

    % phasespace.pos = rand(N,3) .* (initPosRange(:,2) - initPosRange(:,1))' + initPosRange(:,1)';
    % phasespace.mom = rand(N,3) .* (initMomRange(:,2) - initMomRange(:,1))' + initMomRange(:,1)';
    % phasespace.alive = ones(N,1);

    phasespace.N = N;
    phasespace.pos = [];
    phasespace.mom = [];
    bunchInfo.grouping = [];
    bunchInfo.bunchSizes = [];

    for i = 1:M
        phasespace.pos = [phasespace.pos; 0,0,0];
        phasespace.mom = [phasespace.mom; 0,0,0];

        for ii = 1:NB
            phasespace.pos = [phasespace.pos; (rand()-0.5)/2 *spread,dist,0];
            phasespace.mom = [phasespace.mom; 0, initMomY,0];
        end

        bunchInfo.grouping = [bunchInfo.grouping; (i-1)*(NB+1)+1, i*(NB+1)];
        bunchInfo.bunchSizes = [bunchInfo.bunchSizes, NB+1];
    end
    
    bunchInfo.numBunches = M;
    bunchInfo.N = N;

    % p = m0*v/√(1 – v^2/c^2) = m0*v/√(1 – beta^2)

    fprintf('INFO. Gen Phasespace Time: %g sec\n', toc);
end