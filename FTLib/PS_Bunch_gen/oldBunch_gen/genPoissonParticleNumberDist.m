function bunchInfo = genPoissonParticleNumberDist(M, meanVal, maxPartPerBunch, fillRest)

%% INFO
% Generates the bunch info for a POISSON distributed number of particles per bunch. 

%% IN
% M                     - int - (max) number of bunches
% meanVal               - float - mean value of distribution
% maxPertPerBunch = 50  - int - max bunch size to consider in calculating the distribution
% fillRest = true       - boolean - if false the distribution gets matched as good as
%                            possible with less then M bunches; if true the 
%                            remaining bunches get distributed such that the 
%                            RMS error is minimal

%% OUT
% bunchInfo - struct - bunch info

% AUTHOR: JL 2022


    % set default values, when not given
    if nargin < 3, maxPartPerBunch = 50; end
    if nargin < 4, fillRest = true; end

    M = abs(fix(M));
    maxPartPerBunch = abs(fix(maxPartPerBunch));
    N = 0;
    bunchInfo.grouping = [];

    for ii = 1:maxPartPerBunch
        prob = poissonDist(ii, meanVal);
        numBunches = fix(prob * M);

        for jj = 1:numBunches
            bunchInfo.grouping = [bunchInfo.grouping; [N+1, N+ii+1]];
            N = N + ii +1;
        end
    end

    if fillRest
        for kk = 1:(M-size(bunchInfo.grouping,1))
            % compare curretn bunch dist to ideal dist
            probs = poissonDist(1:maxPartPerBunch, meanVal);

            bunchDist = zeros(1,maxPartPerBunch);
            diffs = bunchInfo.grouping(:,2) - bunchInfo.grouping(:,1);
            edges = unique(diffs);
            counts = histcounts(diffs(:), [edges;edges(end)+1]);
            bunchDist(edges) = counts ./ M;

            diffs = probs - bunchDist;

            [~, maxDiffIdx] = max(diffs); 
            
            % add bunch at max diff
            bunchInfo.grouping = [bunchInfo.grouping; [N+1, N+maxDiffIdx+1]];
            N = N + maxDiffIdx +1;

        end
    end

    bunchInfo.numBunches = M;
    bunchInfo.N = N;

    fprintf('INFO. total particles = %i\n', bunchInfo.N);

    bunchInfo.bunchSizes = (bunchInfo.grouping(:,2)-bunchInfo.grouping(:,1)+1);
end

function prob = poissonDist(x, mean)
    prob = exp(-mean) .* mean.^x ./ factorial(x);
end