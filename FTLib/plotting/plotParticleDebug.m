function [] = plotParticleDebug(simState, plotOptions, fig)
    fprintf('PLOT PARTICLE DEBUG ... ');

    % set default values, when not given
    if nargin < 3
        figure();
    else
        figure(fig);
    end

    xLableString = 'sim Step';

    Nt = max(size(simState));
    N = simState(1).global.bunchInfo.N;
    xSteps = 1:Nt;

    %% get info from simsate
    alive = zeros(N, 1, Nt);
    wasEmitted = zeros(N, 1, Nt);
    for iii = 1:Nt
        alive(:, :, iii) = simState(iii).phaseSpace.alive;
        wasEmitted(:, :, iii) = simState(iii).phaseSpace.wasEmitted;
    end

    alivePart = sum(alive,1);
    emittedPart = sum(wasEmitted,1);
    activePart = sum(wasEmitted,1) - sum(~alive,1);

    %% plotOptions
    if plotOptions.plotOverTime
        for iii = 1:Nt
            xSteps(iii) = simState(iii).curTime;
        end
        xLableString = 'Time in sec';
    end

    % ------------ particeles info ------------
    alivePart = permute(alivePart, [3 2 1]);
    emittedPart = permute(emittedPart, [3 2 1]);
    activePart = permute(activePart, [3 2 1]);
    changeActivePart = [diff(activePart);0];
    maxChange = max(changeActivePart);
    minChange = min(changeActivePart);

    subplot(1,1,1);
    plot(xSteps, activePart, '-', 'DisplayName', '# active particles');
    hold on;
    % plot(xSteps, emittedPart, '-', 'DisplayName', '# emitted particles');
    % plot(xSteps, alivePart, '-', 'DisplayName', '# alive particles');
    plot(xSteps, changeActivePart, '-', 'DisplayName', 'change active particles');
    hold off;
    grid on;
    xlabel(xLableString);
    ylabel('Number of Partcles');
    legend('Location', 'north');
    
    title('Particle Emission Debug', sprintf('Change: max %i/ min %i, final Part: %i/%i', maxChange, minChange, activePart(end),N));

    fprintf('END\n');
end