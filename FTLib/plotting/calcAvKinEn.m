function [avWkin] = calcAvKinEn(phaseSpaceArray,figno)
    % Calculates average energy from a phaseSpace Array and plots if figno is
    % given
    % To be used e.g. for injection energy finetuning


    if nargin > 1
        figure(figno);
        clf
        hold on
    end

    for n=1:length(phaseSpaceArray)
        clear phaseSpace;
        phaseSpace=phaseSpaceArray(1,n);    
    
        p(:,1)=phaseSpace.mom(:,1)+phaseSpace.mom(:,2)+phaseSpace.mom(:,3);
        Wkin(:,1)=(p.^2 * Constants.c^2+Constants.eMass^2 * Constants.c^4).^0.5 /(-Constants.eCharge) - Constants.eMass_eV;
    
    
        avWkin(n)=nanmean(Wkin);
            
    
        % set default values, when not given
        if nargin > 1
            scatter(phaseSpace.pos(:,3),Wkin,0.05,'k','+')
            plot([min(phaseSpace.pos(:,3)) max(phaseSpace.pos(:,3))], avWkin(n)* [1 1], LineWidth=3)    
        end  

    end

    if nargin > 1
        hold off
        xlabel('z[m]')
        ylabel('W_{kin} [eV]')
    end

    fprintf(['Average kinetic energy: ' num2str(round(avWkin(~isnan(avWkin)))) ' eV \n']);

end