function [] = plotDt(simState, plotOptions, fig)
    fprintf('PLOT DT ...');

    % set default values, when not given
    if nargin < 3
        figure();
    else
        figure(fig);
    end  

    xLableString = 'sim Step';

    Nt = max(size(simState));
    xSteps = 1:Nt;
     
    dts = [simState.dt];

    %% plotOptions
    if plotOptions.plotOverTime
        for iii = 1:Nt
            xSteps(iii) = simState(iii).curTime;
        end
        xLableString = 'Time in sec';
    end

    %% PLOTTING
    clf;
    hold on;
    % plot dt
    plot(xSteps, dts, '-');    
    hold off;
    grid on;
    xlabel(xLableString);
    ylabel('dt in sec');

    pos = zeros(Nt, 3);
    for iii = 1:Nt
        pos(iii,:) = simState(iii).phaseSpace.pos(1,:);
    end
    maxPos = max(pos);
    minPos = min(pos);
    durch = maxPos-minPos;

    fprintf('END\n');
end