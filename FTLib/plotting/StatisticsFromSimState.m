function [av] = StatisticsFromSimState(simState,plotflag)

% Creates statistical averages over phase space results in simState
% Call with reduced number of phase spaces, e.g. every 10th one by:

%simStateReduced=simState(1:10:end);
%av=StatisticsFromSimState(simStateReduced,true);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

time=[simState.curTime];
phaseSpace=[simState.phaseSpace];

for n=1:length(time)
    av.x(n)=nanmean(phaseSpace(n).pos(:,1));
    av.y(n)=nanmean(phaseSpace(n).pos(:,2));
    av.z(n)=nanmean(phaseSpace(n).pos(:,3));
    av.x_rms(n)=nanmean((phaseSpace(n).pos(:,1)-av.x(n)).^2)^0.5;
    av.y_rms(n)=nanmean((phaseSpace(n).pos(:,2)-av.y(n)).^2)^0.5;
    av.z_rms(n)=nanmean((phaseSpace(n).pos(:,3)-av.z(n)).^2)^0.5;
    
    av.px(n)=nanmean(phaseSpace(n).mom(:,1));
    av.py(n)=nanmean(phaseSpace(n).mom(:,2));
    av.pz(n)=nanmean(phaseSpace(n).mom(:,3));
    
    av.p(n)=(av.px(n)^2+av.py(n)^2+av.pz(n)^2)^0.5;
    av.Wtot(n)=((Constants.eMass*Constants.c^2)^2 + av.p(n)^2*Constants.c^2)^0.5;
    av.Wkin(n)=av.Wtot(n)-Constants.eMass*Constants.c^2;
    
    av.px_rms(n)=nanmean((phaseSpace(n).mom(:,1)-av.px(n)).^2)^0.5;
    av.py_rms(n)=nanmean((phaseSpace(n).mom(:,2)-av.py(n)).^2)^0.5;
    av.pz_rms(n)=nanmean((phaseSpace(n).mom(:,3)-av.pz(n)).^2)^0.5;
    
    av.xpx(n)=nanmean( (phaseSpace(n).pos(:,1)-av.x(n)) .* (phaseSpace(n).mom(:,1)-av.px(n)) );
    av.ypy(n)=nanmean( (phaseSpace(n).pos(:,2)-av.y(n)) .* (phaseSpace(n).mom(:,2)-av.py(n)) );    
    av.zpz(n)=nanmean( (phaseSpace(n).pos(:,3)-av.z(n)) .* (phaseSpace(n).mom(:,3)-av.pz(n)) );
    
    
    av.p0(n)=av.pz(n); %change if required
        
    if av.x_rms(n)^2 *av.px_rms(n)^2 - av.xpx(n)^2   >=0
        av.eps_geo_x(n)= 1/av.p0(n) * ( av.x_rms(n)^2 *av.px_rms(n)^2 - av.xpx(n)^2   )^0.5;
    else
        av.eps_geo_x(n)=NaN;
    end
    
    if av.y_rms(n)^2 *av.py_rms(n)^2 - av.ypy(n)^2    >0
        av.eps_geo_y(n)=1/av.p0(n) * ( av.y_rms(n)^2 *av.py_rms(n)^2 - av.ypy(n)^2   )^0.5;
    else
        av.eps_geo_y(n)=NaN;
    end
        
    if av.z_rms(n)^2 *av.pz_rms(n)^2 - av.zpz(n)^2  >0 
        av.eps_geo_z(n)=1/av.p0(n) * ( av.z_rms(n)^2 *av.pz_rms(n)^2 - av.zpz(n)^2   )^0.5;
    else
        av.eps_geo_z(n)=NaN;
    end
    
    av.eps_norm_x(n)=av.eps_geo_x(n)*av.p0(n)/(Constants.eMass*Constants.c);
    av.eps_norm_y(n)=av.eps_geo_y(n)*av.p0(n)/(Constants.eMass*Constants.c);
    av.eps_norm_z(n)=av.eps_geo_z(n)*av.p0(n)/(Constants.eMass*Constants.c);    
end


if plotflag
    figure(101)
end


if 0 
    figure(101)
    plot(time,av.x)
    hold on
    plot(time,av.y)
    hold off
    title(' x and y over time')

    figure(102)
    plot(av.z,av.x)
    title(' x over z')

    figure(103)
    plot(av.z,av.x_rms)
    hold on
    plot(av.z,av.y_rms)
    %plot(av.z,av.zz)
    hold off
    title(' rms beam size over z')

    figure(104)
    %plot(av.z,av.x_rms)
    hold on
    %plot(av.z,av.y_rms)
    plot(av.z,av.z_rms)
    hold off
    title(' rms beam size over z')


    figure(110)
    plot(av.z,-10^-3 * av.Wkin/Constants.eCharge)
    title('Wkin [keV] over z')

    figure(111)
    plot(av.z,av.eps_geo_x)
    hold on
    plot(av.z,av.eps_geo_y)
    hold off
    title('Geometric emittance over z')

    figure(112)
    plot(av.z,av.eps_norm_x)
    hold on
    plot(av.z,av.eps_norm_y)
    hold off
    title('Normalized emittance over z')
end

end

