function [Final] = GlassboxAveragesPlot(simStateArray, Scan, fig)



    fprintf('Scatter plot for the Glassbox example ');

    % set default values, when not given
    if nargin < 3
        MyFig=figure();
    else
        MyFig=figure(fig);
    end  
    clf
    
    cmap=jet(256);
    mycmap=cmap(1:floor(256/length(simStateArray)) :256,:);

    % Reduce simState
    %simState=ss;%(1:50:end);

    for run=1:length(simStateArray)
        [av(run),vecs(run)] = PrepareAveragesForPlot(simStateArray{run});
        Final.eps_x(run)=av(run).eps_x(end-15);
        Final.dt_rms(run)=av(run).dt_rms(end-15);
        Final.Brightness(run)=-run*Constants.eCharge/(4*pi^2*Final.eps_x(run)^2*Final.dt_rms(run));
        Final.alive(run)=av(run).alive(end-15);
    end

    set(MyFig,'units','normalized','outerposition',[0.1 0.1 0.8 0.5])
    
    subplot(1,2,1)
    yyaxis left
    plot(10^3*av(1).z,-10^-3*av(1).Wkin/Constants.eCharge,'LineWidth',1.0)
    ylabel('W_{kin} [keV]')
    xlim([0 20])
    %axis([0 20 0 100])

    yyaxis right
    hold on
    for run=1:length(simStateArray)
        plot(10^3*av(run).z,10^9*av(run).eps_x ,'-','color',mycmap(run,:) )
    end
    hold off
    ylim([0 1])
    ylabel('\epsilon_x^{(norm.)} [nm]')
    xlabel('z [mm]')

    subplot(1,2,2)
    yyaxis left
    hold on
    for run=1:length(simStateArray)
        plot(10^3*av(run).z,10^6*av(run).x_rms,'-','color',mycmap(run,:) )
    end
    hold off
    ylabel('x_{rms} [µm]')
    %axis([0 20 0 100])

    yyaxis right
    hold on
    for run=1:length(simStateArray)
        cmap(round(run/length(simStateArray)*256))
        semilogx(10^3*av(run).z,10^15*av(run).dt_rms,'-','color',mycmap(run,:) )
    end
    hold off
    xlabel('z [mm]')
    ylabel('\Delta t_{rms} [fs]')

    legend(int2str((1:length(simStateArray))'),'location','northwest')

    set(gca,'xscale','log')

    set(findall(gcf,'-property','FontSize'),'FontSize',16)

    
    %2nd figure
    figure(fig+1)
    yyaxis left
    plot(1:length(simStateArray),10^9*Final.eps_x,'-+')
    ylabel('\epsilon_x^{(norm.)} [nm]')
    yyaxis right
    plot(1:length(simStateArray),10^15*Final.dt_rms,'-+')
    ylabel('\Delta t_{rms} [fs]')
    xlabel('run #')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)



    %2D Result plot
    run=0;
    for i2=1:length(Scan.para2)
        for i1=1:length(Scan.para1)
               run=run+1;
               if run<=length(simStateArray)
                    Scan.Resultmatrix_eps_x(i1,i2)=Final.eps_x(run);
                    Scan.Resultmatrix_dt_rms(i1,i2)=Final.dt_rms(run);
               else
                   Scan.Resultmatrix_eps_x(i1,i2)=0;
                   Scan.Resultmatrix_dt_rms(i1,i2)=0;
               end
        end
    end

    save('./out/Scan.mat','Scan')

    figure(fig+2)
    colormap jet
    imagesc(Scan.para1vec,Scan.para2vec,Scan.Resultmatrix_eps_x')
    set(gca,'YDir','normal')
    %caxis([0 100]);
    cb=colorbar;
    cb.Label.String='Normalized RMS emittance [m]' 
    xlabel(' Cone Factor ')
    ylabel('RMS energy spread [eV]')
    %xlabel(' Laser Amplitude [V/m] ')
    %ylabel('Injection offset [eV]')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)
    saveas(gcf,'./out/SweepPlot1.png')



    figure(fig+3)
    colormap jet
    imagesc(Scan.para1vec,Scan.para2vec,Scan.Resultmatrix_dt_rms')
    set(gca,'YDir','normal')
    %caxis([0 100]);
    cb=colorbar;
    cb.Label.String='RMS bunch length [s]' 
    xlabel(' Cone Factor ')
    ylabel('RMS energy spread [eV]')
    %xlabel(' Laser Amplitude [V/m] ')
    %ylabel('Injection offset [eV]')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)

    saveas(gcf,'./out/SweepPlot2.png')


    %Brightness Plot
    figure(fig+4)
    loglog([0.01 1:2:23],[0.01*3.3e11 Final.Brightness/2.355])
    hold on
    loglog([0.01,0.13,0.33,0.94,2.2,4.4,7.4,11.5,17.5,23.5],1/(4*pi^2)*[3.83659E+11,5.32216E+12, 1.13875E+13, ...
        2.44414E+13, 4.34578E+13, 6.60065E+13, 6.483E+13, 6.1262E+13, 6.32909E+13 7.29728E+13])
    hold off
    title('Brightness= Ne/(\epsilon_n^x\epsilon_n^y\Delta t_{FWHM})')
    xlabel('Electrons per pulse')
    ylabel('5D Brightness [A/m^2/rad^2]')
    legend('Femtotrack', 'Measurement', 'Location','northwest')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)


    %alive Plot
    figure(fig+5)
    plot(1:length(simStateArray),Final.alive)
    xlabel('Electrons per pulse')
    ylabel('Electrons alive at focus')
    set(findall(gcf,'-property','FontSize'),'FontSize',16)


end