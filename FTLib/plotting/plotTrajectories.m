function [MyFig,alive] = plotTrajectories(simState, fig, scatterflag)

    fprintf('Trajectories ');

    % set default values, when not given
    if nargin < 2
        MyFig=figure();
    else
        MyFig=figure(fig);
    end  
    clf

    time=[simState.curTime];
    phaseSpace=[simState.phaseSpace];


    Nvec=phaseSpace.N;


    alive=zeros(size(time));
    p_mag_sq=zeros(Nvec(1,1),size(time,2));
    Wtot=zeros(Nvec(1,1),size(time,2));
    Wkin=zeros(Nvec(1,1),size(time,2));
    x=zeros(Nvec(1,1),size(time,2));
    y=zeros(Nvec(1,1),size(time,2));
    z=zeros(Nvec(1,1),size(time,2));
    xp=zeros(Nvec(1,1),size(time,2));
    yp=zeros(Nvec(1,1),size(time,2));
    zp=zeros(Nvec(1,1),size(time,2));

    deltaZ=zeros(Nvec(1,1),size(time,2));
    deltaW=zeros(Nvec(1,1),size(time,2));

    
    for n=1:length(time)
        alive(1,n)=nnz(phaseSpace(n).alive);
        p_mag_sq(:,n)=phaseSpace(n).mom(:,1).^2 + phaseSpace(n).mom(:,2).^2 + phaseSpace(n).mom(:,3).^2 ;
        


        av.x(n)=nanmean(phaseSpace(n).pos(:,1));
        av.y(n)=nanmean(phaseSpace(n).pos(:,2));
        av.z(n)=nanmean(phaseSpace(n).pos(:,3));
        av.x_rms(n)=nanmean((phaseSpace(n).pos(:,1)-av.x(n)).^2)^0.5;
        av.y_rms(n)=nanmean((phaseSpace(n).pos(:,2)-av.y(n)).^2)^0.5;
        av.z_rms(n)=nanmean((phaseSpace(n).pos(:,3)-av.z(n)).^2)^0.5;
        
        av.px(n)=nanmean(phaseSpace(n).mom(:,1));
        av.py(n)=nanmean(phaseSpace(n).mom(:,2));
        av.pz(n)=nanmean(phaseSpace(n).mom(:,3));
        
        av.p(n)=(av.px(n)^2+av.py(n)^2+av.pz(n)^2)^0.5;
        av.Wtot(n)=((Constants.eMass*Constants.c^2)^2 + av.p(n)^2*Constants.c^2)^0.5;
        av.Wkin(n)=av.Wtot(n)-Constants.eMass*Constants.c^2;
        
        av.px_rms(n)=nanmean((phaseSpace(n).mom(:,1)-av.px(n)).^2)^0.5;
        av.py_rms(n)=nanmean((phaseSpace(n).mom(:,2)-av.py(n)).^2)^0.5;
        av.pz_rms(n)=nanmean((phaseSpace(n).mom(:,3)-av.pz(n)).^2)^0.5;
        
        av.xpx(n)=nanmean( (phaseSpace(n).pos(:,1)-av.x(n)) .* (phaseSpace(n).mom(:,1)-av.px(n)) );
        av.ypy(n)=nanmean( (phaseSpace(n).pos(:,2)-av.y(n)) .* (phaseSpace(n).mom(:,2)-av.py(n)) );    
        av.zpz(n)=nanmean( (phaseSpace(n).pos(:,3)-av.z(n)) .* (phaseSpace(n).mom(:,3)-av.pz(n)) );
            
        av.xp(n)=av.px(n)/av.p(n);
        av.yp(n)=av.py(n)/av.p(n);
        av.zp(n)=av.pz(n)/av.p(n);
        
        av.xp_rms(n)=av.px_rms(n)/av.p(n);
        av.yp_rms(n)=av.py_rms(n)/av.p(n);
        av.zp_rms(n)=av.pz_rms(n)/av.p(n);
        
        av.W(n)=(av.p(n)^2*Constants.c^2 + Constants.eMass^2 *Constants.c^4)^0.5;
        av.beta_z(n)=av.p(n)/av.W(n) *Constants.c;
        av.deltaW_rms(n)=av.beta_z(n)^2 *av.W(n)/av.p(n) *   av.pz_rms(n);
        
        x(:,n)=phaseSpace(n).pos(:,1);
        xp(:,n)=phaseSpace(n).mom(:,1)./p_mag_sq(:,n).^0.5;
        y(:,n)=phaseSpace(n).pos(:,2);
        yp(:,n)=phaseSpace(n).mom(:,2)./p_mag_sq(:,n).^0.5;
        z(:,n)=phaseSpace(n).pos(:,3);
        
        Wtot(:,n)=(p_mag_sq(:,n)*Constants.c^2+Constants.eMass^2*Constants.c^4).^0.5;
        Wkin(:,n)=Wtot(:,n)-Constants.eMass*Constants.c^2;

        deltaZ(:,n)=z(:,n)-av.z(n);
        deltaW(:,n)=Wkin(:,n)-av.Wkin(n);
       
    end

    
    
    set(MyFig,'units','normalized','outerposition',[0.1 0.05 0.3 0.95])
    Sub1=subplot(7,1,1);
    %plot(10^12*time,10^9*x)
    if scatterflag
        scatter(10^12*time,10^9*x(1,:),0.05,'k','+')
    end

    hold on
    plot(10^12*time,10^9*av.x,'g')
    plot(10^12*time,10^9* (av.x+3*av.x_rms),'g','LineWidth',1.3)
    plot(10^12*time,10^9* (av.x-3*av.x_rms),'g','LineWidth',1.3)
    hold off
    ylabel('x [nm]')

    Sub2=subplot(7,1,2);
    if scatterflag
        scatter(10^12*time,10^3*xp,0.05,'k','+')
    end
    hold on
    plot(10^12*time,10^3*av.xp,'g')
    plot(10^12*time,10^3* (av.xp+3*av.xp_rms),'g','LineWidth',1.3)
    plot(10^12*time,10^3* (av.xp-3*av.xp_rms),'g','LineWidth',1.3)
    hold off
    ylabel('x'' [mrad]')

    Sub3=subplot(7,1,3);
    if scatterflag
        scatter(10^12*time,10^9*y,0.05,'k','+')
    end
    hold on
    plot(10^12*time,10^9*av.y,'g')
    plot(10^12*time,10^9* (av.y+3*av.y_rms),'g','LineWidth',1.3)
    plot(10^12*time,10^9* (av.y-3*av.y_rms),'g','LineWidth',1.3)
    hold off
    ylabel('y [nm]')

    Sub4=subplot(7,1,4);
    if scatterflag
        scatter(10^12*time,10^3*yp,0.05,'k','+')
    end
    hold on
    plot(10^12*time,10^3*av.yp,'g')
    plot(10^12*time,10^3* (av.yp+3*av.yp_rms),'g','LineWidth',1.3)
    plot(10^12*time,10^3* (av.yp-3*av.yp_rms),'g','LineWidth',1.3)
    hold off
    ylabel('y'' [mrad]')


    Sub5=subplot(7,1,5);
    if scatterflag    
        scatter(10^12*time,10^9*deltaZ,0.05,'k','+')
    end
    hold on
    plot(10^12*time,zeros(size(time)),'g')
    plot(10^12*time,10^9* (3*av.z_rms),'g','LineWidth',1.3)
    plot(10^12*time,10^9* (-3*av.z_rms),'g','LineWidth',1.3)
    hold off
    ylabel('\Delta z [nm]')
    %ylim([-50,50]);

    Sub6=subplot(7,1,6);
    if scatterflag
        scatter(10^12*time,10^-3*deltaW/(-Constants.eCharge),0.05,'k','+')
    end
    hold on
    plot(10^12*time,zeros(size(time)),'g')
    %plot(10^12*time,10^9* (3*av.deltaW_rms ),'g',LineWidth=1.3)
    %plot(10^12*time,10^9* (-3*av.deltaW_rms ),'g',LineWidth=1.3)
    hold off
    ylabel('\Delta W [keV]')
    ylim([-1,1]);

    Sub7=subplot(7,1,7);
    yyaxis left
    %plot(10^12*time,10^-3*Wkin/(-Constants.eCharge),'LineStyle','-','LineWidth',0.001,'Marker','none')
    if scatterflag
        scatter(10^12*time,10^-3*Wkin/(-Constants.eCharge),0.05,'k','+')    
    end
    %plot(10^12*time,-av.W*Constants.eCharge-Constants.eMass_eV)
    xlabel('time [ps]');
    ylabel('W_{kin} [keV]')

    yyaxis right
    plot(10^12*time,alive/Nvec(1,1) *100)
    ylabel('particles [%]')
    ylim([0 100])

    set(Sub1,'xtick',[])
    set(Sub2,'xtick',[])
    set(Sub3,'xtick',[])
    set(Sub4,'xtick',[])
    set(Sub5,'xtick',[])
    set(Sub6,'xtick',[])
    
    %xlabel('z [µm]')
    
    set(Sub1, 'Position',[0.12 0.85 0.75 0.13],'FontSize',12);
    set(Sub2, 'Position',[0.12 0.715 0.75 0.13],'FontSize',12);
    
    set(Sub3, 'Position',[0.12 0.58 0.75 0.13],'FontSize',12);
    set(Sub4, 'Position',[0.12 0.445 0.75 0.13],'FontSize',12);
    set(Sub5, 'Position',[0.12 0.31 0.75 0.13],'FontSize',12);
    set(Sub6, 'Position',[0.12 0.175,0.75 0.13],'FontSize',12);
    set(Sub7, 'Position',[0.12 0.07 0.75 0.1],'FontSize',12);

%     yyaxis left
%     plot(10^12*time,10^-3*Wkin/(-Constants.eCharge),'LineStyle','-','LineWidth',0.001,'Marker','none')
%     xlabel('time [ps]');
%     ylabel('W_{kin} [keV]')
% 
%     yyaxis right
%     plot(10^12*time,alive)
%     ylabel('total number of particles')

    
    fprintf('END\n');
end