function [MyFig] = plotKinEnergy(simState,  fig,  plot_how_many_max)

%% INFO
% Plot the kin, energy and the number of alive particles.

%% IN
% simState - struct - simulation particl data
% fig - int - figure number
% plot_how_many_max - float - percentage of plotted graphs between 0 and 1

%% OUT
% MyFig - Figure - Figure info object

% AUTHOR: JL 2022


    fprintf('PLOT KINETIC ENERGY AND PARTICLE SURVIVAL ');

    % set default values,  when not given
    if nargin < 2
        MyFig = figure();
    else
        MyFig = figure(fig);
    end  
    if nargin < 3,  plot_how_many_max = 0.1; end  % how many particle trajectories at maximum to plot
    clf;

    time = [simState.curTime];
    phaseSpace = [simState.phaseSpace];

    Nvec = phaseSpace.N;

    plot_how_many_max = max(min(plot_how_many_max,1),0);
    plot_N = fix(Nvec(1,1)*plot_how_many_max);
    if plot_how_many_max < 1
        fprintf(2,'INFO: Plot only %i from %i Energy Plots! ', plot_N, Nvec(1,1))
    end

    alive = zeros(size(time));
    p_mag_sq = zeros(Nvec(1,1), size(time, 2));
    Wtot = zeros(Nvec(1,1), size(time, 2));
    Wkin = zeros(Nvec(1,1), size(time, 2));

    for n = 1:length(time)
        alive(1, n) = nnz(phaseSpace(n).alive);
        p_mag_sq(:, n) = phaseSpace(n).mom(:, 1).^2 + phaseSpace(n).mom(:, 2).^2 + phaseSpace(n).mom(:, 3).^2 ;
        Wtot(:, n) = (p_mag_sq(:, n) * Constants.c^2 + Constants.eMass^2 * Constants.c^4).^0.5;
        Wkin(:, n) = Wtot(:, n)-Constants.eMass*Constants.c^2;
    end

    yyaxis left
    plot(10^12*time, 10^-3*Wkin(1:plot_N,:)/(-Constants.eCharge), 'LineStyle', '-', 'LineWidth', 0.001, 'Marker', 'none')
    xlabel('time [ps]');
    ylabel('W_{kin} [keV]')

    yyaxis right
    plot(10^12*time, alive)
    ylabel('total number of particles')

    yyaxis left % reselct the left y-axis

    fprintf('END\n');
end