function [MyFig] = GLassboxScatterplot(ss, fig)

    fprintf('Scatter plot for the Glassbox example ');

    % set default values, when not given
    if nargin < 2
        MyFig=figure();
    else
        MyFig=figure(fig);
    end  
    clf

    % Reduce simState
    simState=ss(1:50:end);

    time=[simState.curTime];
    phaseSpace=[simState.phaseSpace];

    for n=1:length(time)
        x(:,n)=phaseSpace(n).pos(:,1);
        z(:,n)=phaseSpace(n).pos(:,3);
    end

    %x=reshape(x,1,[]);  %No!!!
    %z=reshape(z,1,[]); 
    
    set(MyFig,'units','normalized','outerposition',[0.1 0.1 0.8 0.5])


    scatter(10^3*z,10^6 * x,0.05,'k','+')
    box on
    xlabel('z [mm]')
    ylabel('x [µm]')

    Sub1=axes('Position',[.5 .7 .4 .2]);
    scatter(z(:,1:7),10^6*x(:,1:7),0.05,'k','+')
    xlim([0,50e-6]);
    ylim([-10,10]);
    set(Sub1,'xtick',[])
    %set(Sub1,'ytick',[])
    ylabel('x [µm]')
    box on

    Sub2=axes('Position',[.5 .18 .4 .2]);
    scatter(z(:,59:65),10^6* x(:,59:65),0.05,'k','+')
    ylim([0.024,0.030]);
    ylim([-5,5]);
    set(Sub2,'xtick',[])
    %set(Sub2,'ytick',[])
    ylabel('x [µm]')
    box on
    set(findall(gcf,'-property','FontSize'),'FontSize',16)


%     %second figure
%     if nargin < 2
%         MyFig2=figure();
%     else
%         MyFig=figure(fig+1);
%     end  
%     clf
% 
%     yyaxis left
%     plot(10^3*av.z,-10^-3*av.Wkin/Constants.eCharge,'LineWidth',1.0)
%     ylabel('W_{kin} [keV]')
%     axis([0 20 0 100])
% 
% 
%     yyaxis right
%     plot(10^3*av.z,10^9*av.eps_x)
%     xlabel('z[mm]')
%     ylabel('\epsilon_x^{(norm.)} [nm]')
%     set(findall(gcf,'-property','FontSize'),'FontSize',16)
% 
%     %third figure
%     if nargin < 2
%         MyFig2=figure();
%     else
%         MyFig=figure(fig+2);
%     end  
%     clf
% 
%     yyaxis left
%     plot(10^3*av.z,10^6*av.x_rms,'LineWidth',1.0)
%     ylabel('x_{rms} [µm]')
%     
%     yyaxis right
%     plot(10^3*av.z,10^9*av.Deltaz)
%     xlabel('z[mm]')
%     ylabel('\epsilon_x^{(norm.)} [nm]')
%     set(findall(gcf,'-property','FontSize'),'FontSize',16)

end