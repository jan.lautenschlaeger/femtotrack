function [av,vecs] = PrepareAveragesForPlot(simState)
    
    simState=simState(1:10:end);
    
    time=[simState.curTime];
    phaseSpace=[simState.phaseSpace];

    for n=1:length(time)
        
        av.alive(1,n)=nnz(phaseSpace(n).alive);
       
        


        av.x(n)=nanmean(phaseSpace(n).pos(:,1));
        av.y(n)=nanmean(phaseSpace(n).pos(:,2));
        av.z(n)=nanmean(phaseSpace(n).pos(:,3));
        av.x_rms(n)=nanmean((phaseSpace(n).pos(:,1)-av.x(n)).^2)^0.5;
        av.y_rms(n)=nanmean((phaseSpace(n).pos(:,2)-av.y(n)).^2)^0.5;
        av.z_rms(n)=nanmean((phaseSpace(n).pos(:,3)-av.z(n)).^2)^0.5;
        
        av.px(n)=nanmean(phaseSpace(n).mom(:,1));
        av.py(n)=nanmean(phaseSpace(n).mom(:,2));
        av.pz(n)=nanmean(phaseSpace(n).mom(:,3));
        
        av.p(n)=(av.px(n)^2+av.py(n)^2+av.pz(n)^2)^0.5;
        av.Wtot(n)=((Constants.eMass*Constants.c^2)^2 + av.p(n)^2*Constants.c^2)^0.5;
        av.Wkin(n)=av.Wtot(n)-Constants.eMass*Constants.c^2;
        
        av.px_rms(n)=nanmean((phaseSpace(n).mom(:,1)-av.px(n)).^2)^0.5;
        av.py_rms(n)=nanmean((phaseSpace(n).mom(:,2)-av.py(n)).^2)^0.5;
        av.pz_rms(n)=nanmean((phaseSpace(n).mom(:,3)-av.pz(n)).^2)^0.5;
        
        av.xpx(n)=nanmean( (phaseSpace(n).pos(:,1)-av.x(n)) .* (phaseSpace(n).mom(:,1)-av.px(n)) );
        av.ypy(n)=nanmean( (phaseSpace(n).pos(:,2)-av.y(n)) .* (phaseSpace(n).mom(:,2)-av.py(n)) );    
        av.zpz(n)=nanmean( (phaseSpace(n).pos(:,3)-av.z(n)) .* (phaseSpace(n).mom(:,3)-av.pz(n)) );
            
        av.xp(n)=av.px(n)/av.p(n);
        av.yp(n)=av.py(n)/av.p(n);
        av.zp(n)=av.pz(n)/av.p(n);
        
        av.xp_rms(n)=av.px_rms(n)/av.p(n);
        av.yp_rms(n)=av.py_rms(n)/av.p(n);
        av.zp_rms(n)=av.pz_rms(n)/av.p(n);

        av.eps_x(n)=(av.x_rms(n)^2 * av.xp_rms(n)^2 - av.xpx(n)^2 / av.p(n)^2)^0.5;
        
        av.W(n)=(av.p(n)^2*Constants.c^2 + Constants.eMass^2 *Constants.c^4)^0.5;
        av.beta_z(n)=av.p(n)/av.W(n) *Constants.c;
        av.deltaW_rms(n)=av.beta_z(n)^2 *av.W(n)/av.p(n) *   av.pz_rms(n);
        
        av.dt_rms(n)=av.z_rms(n)/av.beta_z(n)/Constants.c;

        vecs=0;
%         vecs.p_mag_sq(:,n)=phaseSpace(n).mom(:,1).^2 + phaseSpace(n).mom(:,2).^2 + phaseSpace(n).mom(:,3).^2 ;
%         vecs.x(:,n)=phaseSpace(n).pos(:,1);
%         vecs.xp(:,n)=phaseSpace(n).mom(:,1)./vecs.p_mag_sq(:,n).^0.5;
%         vecs.y(:,n)=phaseSpace(n).pos(:,2);
%         vecs.yp(:,n)=phaseSpace(n).mom(:,2)./vecs.p_mag_sq(:,n).^0.5;
%         vecs.z(:,n)=phaseSpace(n).pos(:,3);
%         
%         vecs.Wtot(:,n)=(vecs.p_mag_sq(:,n)*Constants.c^2+Constants.eMass^2*Constants.c^4).^0.5;
%         vecs.Wkin(:,n)=vecs.Wtot(:,n)-Constants.eMass*Constants.c^2;
% 
%         vecs.deltaZ(:,n)=vecs.z(:,n)-av.z(n);
%         vecs.deltaW(:,n)=vecs.Wkin(:,n)-av.Wkin(n);
    end


end