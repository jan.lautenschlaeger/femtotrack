function [] = plotDebugTimes(debugTimes, plotOptions, fig)
    fprintf('PLOT DEBUG TIMES ... ');

    % set default values, when not given
    if nargin < 3
        figure();
    else
        figure(fig);
    end

    Nt = max(size(debugTimes));
    xSteps = 1:Nt;

    xLableString = 'sim step';

    %% plotOptions
%     if plotOptions.plotOverTime
%         for iii = 1:Nt
%             xSteps(iii) = simState(iii).curTime;
%         end
%         xLableString = 'Time in sec';
%     end

    %% PLOT times
    % ------------ times per step ------------
    subplot(1, 2, 1);
    plot(xSteps, debugTimes(:, 1), '-', 'DisplayName', 'Emission');
    hold on;
    plot(xSteps, debugTimes(:, 2), '-', 'DisplayName', 'Interpolation');
    plot(xSteps, debugTimes(:, 3), '-', 'DisplayName', 'ee Interaction');
    plot(xSteps, debugTimes(:, 4), '--', 'DisplayName', 'Boris Kick');
    plot(xSteps, debugTimes(:, 5), '--', 'DisplayName', 'Kill Particle');
    plot(xSteps, debugTimes(:, 6), '-.', 'DisplayName', 'Update Win');
    plot(xSteps, debugTimes(:, 7), '-.', 'DisplayName', 'Calc dt');
    plot(xSteps, debugTimes(:, 8), ':', 'DisplayName', 'Save simState');
    hold off;
    grid on;
    xlabel(xLableString);
    ylabel('Calculation Time in sec');
    % title(sprintf('griddedInter Total: %g sec', sum(debugTimes, 'all')), sprintf('%g ms, %g ms, %g ms, %g ms', mean(debugTimes(:, 1)*1000), mean(debugTimes(:, 2))*1000, mean(debugTimes(:, 3))*1000, mean(debugTimes(:, 4))*1000));
    title('Mean Times');
    legend;
    % ------------ cumulative times ------------
    
    perzentTimes = sum(debugTimes) ./ sum(sum(debugTimes));

    subplot(2, 2, 4);
    plot(xSteps, cumsum(debugTimes(:, 1)), '-', 'DisplayName', sprintf('(%02.1f %%) Emission', perzentTimes(1)*100));
    hold on;
    plot(xSteps, cumsum(debugTimes(:, 2)), '-', 'DisplayName', sprintf('(%02.1f %%) Interpolation', perzentTimes(2)*100));
    plot(xSteps, cumsum(debugTimes(:, 3)), '-', 'DisplayName', sprintf('(%02.1f %%) ee Interaction', perzentTimes(3)*100));
    plot(xSteps, cumsum(debugTimes(:, 4)), '--', 'DisplayName', sprintf('(%02.1f %%) Boris Kick', perzentTimes(4)*100));
    plot(xSteps, cumsum(debugTimes(:, 5)), '--', 'DisplayName', sprintf('(%02.1f %%) Kill Particle', perzentTimes(5)*100));
    plot(xSteps, cumsum(debugTimes(:, 6)), '-.', 'DisplayName', sprintf('(%02.1f %%) Update Win', perzentTimes(6)*100));
    plot(xSteps, cumsum(debugTimes(:, 7)), '-.', 'DisplayName', sprintf('(%02.1f %%) Calc dt', perzentTimes(7)*100));
    plot(xSteps, cumsum(debugTimes(:, 8)), ':', 'DisplayName', sprintf('(%02.1f %%) Save simState', perzentTimes(8)*100));
    hold off;
    grid on;
    xlabel(xLableString);
    ylabel('cumulative calculation time (s)');
    title('Cumulative Times');
    legend('Location', 'north');
    
    sgtitle(sprintf('Debug Times: Info: %s, %s, %s; Total Time: %g sec',  plotOptions.solver, plotOptions.eeInter, plotOptions.modeWin, sum(debugTimes, 'all')),'interpreter','none');
    
%     axes('position',[0.1,0.1,0.3,0.3])
    subplot(2, 2, 2);
    box on
    explode = {perzentTimes(1)};
    labels = {...
        sprintf('(%02.1f %%) Emission', perzentTimes(1)*100),...
        sprintf('(%02.1f %%) Interpolation', perzentTimes(2)*100),...
        sprintf('(%02.1f %%) ee Interaction', perzentTimes(3)*100),...
        sprintf('(%02.1f %%) Boris Kick', perzentTimes(4)*100),...
        sprintf('(%02.1f %%) Kill Particle', perzentTimes(5)*100),...
        sprintf('(%02.1f %%) Update Win', perzentTimes(6)*100),...
        sprintf('(%02.1f %%) Calc dt', perzentTimes(7)*100),...
        sprintf('(%02.1f %%) Save simState', perzentTimes(8)*100),...
        };
    labels = {...
        'Emission','Interpolation',...
        'ee Interaction','Boris Kick',...
        'Kill Particle','Update Win',...
        'Calc dt','Save simState'};
    pie(perzentTimes)

    lgd = legend(labels);
    lgd.Location = 'eastoutside';

    fprintf('END\n');
end