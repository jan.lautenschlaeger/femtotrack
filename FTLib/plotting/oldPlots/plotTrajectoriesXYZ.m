function [myfig] = plotTrajectoriesXYZ(simState, fieldList, plotOptions, fig, plot_how_many_max)

%% INFO
% Plot the kin, energy and the number of alive particles.

%% IN
% simState - struct - simulation particl data
% fieldList - list(stucts) - list of fields
% plotOptions - strcut - additional ploting options
% fig - int - figure number
% plot_how_many_max - float - percentage of plotted graphs between 0 and 1

%% OUT
% myfig - Figure - Figure info object

% AUTHOR: JL 2022


    fprintf('PLOT TRAJEC XYZ ... ');

    %% COLORS
    visBoxCol = [0, 0, 0];

    % set default values, when not given
    if nargin < 4
        myfig = figure();
    else
        myfig = figure(fig);
    end 
    if nargin < 5, plot_how_many_max = 0.1; end  % how many particle trajectories at maximum to plot
    clf;  



    Nt = max(size(simState));
    N = simState(1).global.bunchInfo.N;
    NB = size(simState(1).global.bunchInfo.grouping, 1);

    plot_how_many_max = max(min(plot_how_many_max,1),0);
    plot_N = fix(N*plot_how_many_max);
    if plot_how_many_max < 1
        fprintf(2,'INFO: Plot only %i from %i Trajectories! ', plot_N, N)
    end

    cols = {'r', 'g', 'b', 'c', 'm', 'y', 'k', 'w'};
    cols = {[1, 0, 0], [0, 1, 0], [0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 0]};
    colorMinMax = [0.3, 1];

    xyz = zeros(plot_N, 3, Nt);
    for iii = 1 : Nt
        xyz(:, :, iii) = simState(iii).phaseSpace.pos(1 : plot_N, :);
    end

    xyz = permute(xyz, [3 2 1]);

    simSteps = 1 : Nt;
    
    time = simSteps;
    for iii = 1 : Nt
        time(iii) = simState(iii).curTime;
    end

    %% X TRAJECTORIES
    % ------------ x over sim step ------------
    subplot(3, 3, 1);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(simSteps, xyz(:, 1, jjj) * 1e6, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    % plot visual aid boxes
    for visBox = plotOptions.visBoxList
        boxBounds = visBox{1};
        plot([simSteps(1), simSteps(end)], [boxBounds(1, 1), boxBounds(1, 1)], 'Color', visBoxCol);
        plot([simSteps(1), simSteps(end)], [boxBounds(1, 2), boxBounds(1, 2)], 'Color', visBoxCol);
    end
    hold off;
    grid on;
    xlabel('sim step');
    ylabel('x (µm)');

    % ------------ x over time ------------
    subplot(3, 3, 2);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(time * 1e9, xyz(:, 1, jjj) * 1e6, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    hold off;
    grid on;
    xlabel('t (ns)');
    ylabel('x (µm)');

    % ------------ x over z ------------
    subplot(3, 3, 3);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(xyz(:, 3, jjj) * 1e3, xyz(:, 1, jjj) * 1e6, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    hold off;
    grid on;
    xlabel('z (mm)');
    ylabel('x (µm)');

    %% Y TRAJECTORIES
    % ------------ y over sim step ------------
    subplot(3, 3, 4);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(simSteps, xyz(:, 2, jjj) * 1e6, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    % plot visual aid boxes
    for visBox = plotOptions.visBoxList
        boxBounds = visBox{1};
        plot([simSteps(1), simSteps(end)], [boxBounds(2, 1), boxBounds(2, 1)], 'Color', visBoxCol);
        plot([simSteps(1), simSteps(end)], [boxBounds(2, 2), boxBounds(2, 2)], 'Color', visBoxCol);
    end
    hold off;
    grid on;
    xlabel('sim step');
    ylabel('y (µm)');
    
    % ------------ y over time ------------
    subplot(3, 3, 5);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(time * 1e9, xyz(:, 2, jjj) * 1e6, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    hold off;
    grid on;
    xlabel('t (ns)');
    ylabel('y (µm)');
    
    % ------------ y over z ------------
    subplot(3, 3, 6);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(xyz(:, 3, jjj) * 1e3, xyz(:, 2, jjj) * 1e6, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    hold off;
    grid on;
    xlabel('z (mm)');
    ylabel('y (µm)');

    %% Z TRAJECTORIES
    % ------------ z over sim step ------------
    subplot(3, 3, 7);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(simSteps, xyz(:, 3, jjj) * 1e3, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    % plot visual aid boxes
    for visBox = plotOptions.visBoxList
        boxBounds = visBox{1};
        plot([simSteps(1), simSteps(end)], [boxBounds(3, 1), boxBounds(3, 1)], 'Color', visBoxCol);
        plot([simSteps(1), simSteps(end)], [boxBounds(3, 2), boxBounds(3, 2)], 'Color', visBoxCol);
    end
    hold off;
    grid on;
    xlabel('sim step');
    ylabel('z (mm)');
    
    % ------------ z over time ------------
    subplot(3, 3, 8);
    hold on;
    % plot trajectory
    for jjj = 1 : plot_N
        plot(time * 1e9, xyz(:, 3, jjj) * 1e3, '-');%, 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols, 2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
    end
    hold off;
    grid on;
    xlabel('t (ns)');
    ylabel('z (mm)');
    
    sgtitle('2D-trajectories');
    
    fprintf('END\n');
end