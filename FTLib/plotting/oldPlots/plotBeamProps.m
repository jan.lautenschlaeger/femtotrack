function [] = plotBeamProps(simState, fig)
    fprintf('PLOT BEAM PROPERTIES ... ');

    % set default values, when not given
    if nargin < 2
        figure();
    else
        figure(fig);
    end 
    clf;
    
    %% SETTINGS
    fontsize = 12;
    
    % which y-axes
    plot_mean_x_and_mean_y = true;  % plot transverse coordinate means
    plot_mean_z = true;  % plot transverse coordinate means
    plot_sigma_x_and_sigma_y = true;  % plot transverse beam sizes
    plot_sigma_x_prime_and_sigma_y_prime = false;  % plot transverse angular beam sizes
    plot_sigma_z = true;  % plot longitudinal beam size
    plot_sigma_delta = true;  % plot standard deviation of momentum deviation
    plot_norm_emittance = true;  % plot normalized emittance
    plot_geo_emittance = false;  % plot geometric emittance
    plot_kin_energy = true;  % plot kinetic energy
    
    % which x-axes
    plot_over_sim_steps = true;
    plot_over_time = true;
    plot_over_z = true;
    
    % scale of x-axes
    xscale_z = 'linear';  % set as 'log' or 'linear'
    
    %%
    rows = sum([plot_mean_x_and_mean_y, plot_mean_z, plot_sigma_x_and_sigma_y, plot_sigma_x_prime_and_sigma_y_prime, plot_sigma_z, plot_sigma_delta, plot_norm_emittance, plot_geo_emittance, plot_kin_energy], 'all');
    cols = sum([plot_over_sim_steps, plot_over_time, plot_over_z], 'all');
    idx = 1;  % running subplot index
    
    %% MATH
    Nt = max(size(simState));
    simSteps = 1 : max(size(simState));
    time = [simState.curTime];
    phasespace = [simState.phaseSpace];

    mean_x = zeros(1, Nt);
    mean_y = zeros(1, Nt);
    mean_z = zeros(1, Nt);
    mean_px = zeros(1, Nt);
    mean_py = zeros(1, Nt);
    mean_pz = zeros(1, Nt);
    mean_p = zeros(1, Nt);
    
    var_x = zeros(1, Nt);
    var_x_px = zeros(1, Nt);
    var_y = zeros(1, Nt);
    var_y_py = zeros(1, Nt);
    var_z = zeros(1, Nt);
    var_px = zeros(1, Nt);
    var_py = zeros(1, Nt);
    var_pz = zeros(1, Nt);
    var_x_prime = zeros(1, Nt);
    var_y_prime = zeros(1, Nt);
    var_delta = zeros(1, Nt);
    var_t = zeros(1, Nt);
    
    sigma_x = zeros(1, Nt);  % beam size in transverse direction x
    sigma_y = zeros(1, Nt);  % beam size in transverse direction y
    sigma_z = zeros(1, Nt);  % beam size in longitudinal direction z
    sigma_x_prime = zeros(1, Nt);  % angular beam size in transverse direction x
    sigma_y_prime = zeros(1, Nt);  % angular beam size in transverse direction y
    sigma_delta = zeros(1, Nt);  % standard deviation of momentum deviation
    sigma_t = zeros(1, Nt);
    
    emittance_x = zeros(1, Nt);  % geometric 2D-emittances in transverse direction x
    emittance_y = zeros(1, Nt);  % geometric 2D-emittances in transverse direction y
    emittance_x_n = zeros(1, Nt);  % normalized 2D-emittances in transverse direction x
    emittance_y_n = zeros(1, Nt);  % normalized 2D-emittances in transverse direction y
    
    e_kin = zeros(1, Nt);  % kinetic energy in eV
    
    for i = 1 : length(time)
        x = phasespace(i).pos(phasespace(i).alive, 1);
        y = phasespace(i).pos(phasespace(i).alive, 2);
        z = phasespace(i).pos(phasespace(i).alive, 3);
        px = phasespace(i).mom(phasespace(i).alive, 1);
        py = phasespace(i).mom(phasespace(i).alive, 2);
        pz = phasespace(i).mom(phasespace(i).alive, 3);
        
        x_prime = atan2(px', pz');  % atan2 returns rad, atan2d returns deg
        y_prime = atan2(py', pz');
        p = (px .^ 2 + py .^ 2 + pz .^ 2) .^ 0.5;
        delta = (p - mean(p)) / mean(p);  % delta = Delta_p / p_0 = (p - p_0) / p_0
        t = (z - mean(z)) * Constants.eMass / mean(p);
        
        mean_x(i) = mean(x);
        mean_y(i) = mean(y);
        mean_z(i) = mean(z);
        mean_px(i) = mean(px);
        mean_py(i) = mean(py);
        mean_pz(i) = mean(pz);
        mean_p(i) = mean(p);  % (mean_px(i) ^ 2 + mean_py(i) ^ 2 + mean_pz(i) ^ 2) ^ 0.5;
        
        var_x(i) = population_variance(x);
        var_x_px(i) = population_variance(x, px);
        var_y(i) = population_variance(y);
        var_y_py(i) = population_variance(y, py);
        var_z(i) = population_variance(z);
        var_px(i) = population_variance(px);
        var_py(i) = population_variance(py);
        var_pz(i) = population_variance(pz);
        var_x_prime(i) = population_variance(x_prime);
        var_y_prime(i) = population_variance(y_prime);
        var_delta(i) = population_variance(delta);
        var_t(i) = population_variance(t);
        
        sigma_x(i) = var_x(i) .^ 0.5;
        sigma_y(i) = var_y(i) .^ 0.5;
        sigma_z(i) = var_z(i) .^ 0.5;
        sigma_x_prime(i) = var_x_prime(i) .^ 0.5;
        sigma_y_prime(i) = var_y_prime(i) .^ 0.5;
        sigma_delta(i) = var_delta(i) .^ 0.5;
        sigma_t(i) = var_t(i) .^ 0.5;
        
        emittance_x(i) = phase_space_emittance_from_central_moments(var_x(i), var_px(i), var_x_px(i), mean_pz(i));  % emittance_x(i) = phase_space_emittance(x, px, pz);
        emittance_y(i) = phase_space_emittance_from_central_moments(var_y(i), var_py(i), var_y_py(i), mean_pz(i));
        emittance_x_n(i) = phase_space_emittance_from_central_moments(var_x(i), var_px(i), var_x_px(i));  % emittance_x_n(i) = phase_space_emittance(x, px);
        emittance_y_n(i) = phase_space_emittance_from_central_moments(var_y(i), var_py(i), var_y_py(i));
        
        e_kin(i) = (sqrt(mean_p(i) ^ 2 * Constants.c ^ 2 + Constants.eMass ^ 2 * Constants.c ^ 4) - Constants.eMass * Constants.c ^ 2) / (- Constants.eCharge);
    end
    
    %% PLOT MEAN
    if plot_mean_x_and_mean_y
        if plot_over_sim_steps
            % ------------ mean_x and mean_y over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, mean_x * 1e6, simSteps, mean_y * 1e6, '-')
            legend('\mu_x', '\mu_y', 'box', 'off', 'location', 'best')  % legend(..., 'location', 'eastoutside')
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('�_x, �_y (�m)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ mean_x and mean_y over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, mean_x * 1e6, time * 1e9, mean_y * 1e6, '-');
            legend('\mu_x', '\mu_y', 'box', 'off', 'location', 'best')
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('�_x, �_y (�m)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ mean_x and mean_y over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, mean_x * 1e6, mean_z * 1e3, mean_y * 1e6, '-');
            legend('\mu_x', '\mu_y', 'box', 'off', 'location', 'best')
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('�_x, �_y (�m)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end
    if plot_mean_z
        if plot_over_sim_steps
            % ------------ mean_z over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, mean_z * 1e3, '-')
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('�_z (mm)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ mean_z over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, mean_z * 1e3, '-');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('�_z (mm)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ mean_z over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, mean_z * 1e3, '-');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('�_z (mm)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end

    %% PLOT TRANSVERSE BEAM SIZE
    if plot_sigma_x_and_sigma_y
        if plot_over_sim_steps
            % ------------ sigma_x and sigma_y over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, sigma_x * 1e6, simSteps, sigma_y * 1e6, '-')
            legend('\sigma_x', '\sigma_y', 'box', 'off', 'location', 'best')
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('\sigma_x, \sigma_y (�m)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ sigma_x and sigma_y over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, sigma_x * 1e6, time * 1e9, sigma_y * 1e6, '-');
            legend('\sigma_x', '\sigma_y', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('\sigma_x, \sigma_y (�m)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ sigma_x and sigma_y over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, sigma_x * 1e6, mean_z * 1e3, sigma_y * 1e6, '-');
            legend('\sigma_x', '\sigma_y', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('\sigma_x, \sigma_y (�m)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end

    %% PLOT TRANSVERSE ANGULAR BEAM SIZE
    if plot_sigma_x_prime_and_sigma_y_prime
        if plot_over_sim_steps
            % ------------ sigma_x_prime and sigma_y_prime over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, sigma_x_prime, simSteps, sigma_y_prime, '-');
            legend('\sigma_{x''}', '\sigma_{y''}', 'box', 'off', 'location', 'best')
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('\sigma_{x''}, \sigma_{y''} (rad)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ sigma_x_prime and sigma_y_prime over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, sigma_x_prime, time * 1e9, sigma_y_prime, '-');
            legend('\sigma_{x''}', '\sigma_{y''}', 'box', 'off', 'location', 'best')
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('\sigma_{x''}, \sigma_{y''} (rad)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ sigma_x_prime and sigma_y_prime over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, sigma_x_prime, mean_z * 1e3, sigma_y_prime, '-');
            legend('\sigma_{x''}', '\sigma_{y''}', 'box', 'off', 'location', 'best')
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('\sigma_{x''}, \sigma_{y''} (rad)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end

    %% PLOT LONGITUDINAL BEAM SIZE
    if plot_sigma_z
        if plot_over_sim_steps
            % ------------ sigma_z over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, sigma_z * 1e6, '-');
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('\sigma_z (�m)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ sigma_z over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, sigma_z * 1e6, '-');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('\sigma_z (�m)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ sigma_z over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, sigma_z * 1e6, '-');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('\sigma_z (�m)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end

    %% PLOT STANDARD DEVIATION OF MOMENTUM
    if plot_sigma_delta
        if plot_over_sim_steps
            % ------------ sigma_delta over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, sigma_delta * 100, '-');
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('\sigma_{\delta} (%)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ sigma_delta over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, sigma_delta * 100, '-');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('\sigma_{\delta} (%)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ sigma_delta over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, sigma_delta * 100, '-');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('\sigma_{\delta} (%)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end

    %% PLOT EMITTANCE
    if plot_norm_emittance
        if plot_over_sim_steps
            % ------------ normalized rms emittance over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, emittance_x_n * 1e12, simSteps, emittance_y_n * 1e12, '-');
            legend('\epsilon_{x,n}', '\epsilon_{y,n}', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('\epsilon_n (pm)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ normalized rms emittance over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, emittance_x_n * 1e12, time * 1e9, emittance_y_n * 1e12, '-');
            legend('\epsilon_{x,n}', '\epsilon_{y,n}', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('\epsilon_n (pm)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ normalized rms emittance over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, emittance_x_n * 1e12, mean_z * 1e3, emittance_y_n * 1e12, '-');
            legend('\epsilon_{x,n}', '\epsilon_{y,n}', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('\epsilon_n (pm)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end
    if plot_geo_emittance
        if plot_over_sim_steps
            % ------------ geometric emittance over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, emittance_x * 1e9, simSteps, emittance_y * 1e9, '-');
            legend('\epsilon_x', '\epsilon_y', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('\epsilon (nm)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ geometric emittance over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, emittance_x * 1e9, time * 1e9, emittance_y * 1e9, '-');
            legend('\epsilon_x', '\epsilon_y', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('\epsilon (nm)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ geometric emittance over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, emittance_x * 1e9, mean_z * 1e3, emittance_y * 1e9, '-');
            legend('\epsilon_x', '\epsilon_y', 'box', 'off', 'location', 'best');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('\epsilon (nm)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end

    %% PLOT KINETIC ENERGY
    if plot_kin_energy
        if plot_over_sim_steps
            % ------------ e_kin over sim step ------------
            subplot(rows, cols, idx);
            hold on;
            plot(simSteps, e_kin * 1e-3, '-');
            hold off;
            grid on;
            xlabel('sim step');
            ylabel('E_{kin} (keV)');
            idx = idx + 1;
        end
        if plot_over_time
            % ------------ e_kin over time ------------
            subplot(rows, cols, idx);
            hold on;
            plot(time * 1e9, e_kin * 1e-3, '-');
            hold off;
            grid on;
            xlabel('t (ns)');
            ylabel('E_{kin} (keV)');
            idx = idx + 1;
        end
        if plot_over_z
            % ------------ e_kin over z ------------
            subplot(rows, cols, idx);
            hold on;
            plot(mean_z * 1e3, e_kin * 1e-3, '-');
            hold off;
            grid on;
            xlabel('z (mm)');
            ylabel('E_{kin} (keV)');
            set(gca, 'xscale', xscale_z);
            idx = idx + 1;
        end
    end
    
    set(findall(gcf, '-property', 'FontSize'), 'FontSize', fontsize);

    fprintf('END\n');
end



function emittance = phase_space_emittance(q, p_q, p_z)
    %% INFO
    % Calculate the transverse 2D-emittance in the canonical coordinates q and p_q.
    
    %% IN
    % q - Cartesian coordinates x or y.
    % p_q - Momenta in transverse direction q.
    % p_z - Momenta in longitudinal direction z.

    %% OUT
    % emittance - float - If p_z is given, return the geometric rms emittance, else return the normalized rms emittance.
    
%     m_e = 9.1093837015e-31;
%     c = 299792458;

    variance_q = population_variance(q);
    variance_p_q = population_variance(p_q);
    variance_q_times_p_q = population_variance(q, p_q);
    radicand = variance_q * variance_p_q - variance_q_times_p_q .^ 2;
    if radicand >= 0  % rounding error might produce negative (unphysical) radicand
        if nargin < 3  % if p_z is not given
            emittance = sqrt(radicand) / (Constants.eMass * Constants.c);
        else  % if p_z is given
            emittance = sqrt(radicand) / mean(p_z);
        end
    else
        emittance = 0;
    end
end



function emittance = phase_space_emittance_from_central_moments(var_q, var_pq, var_q_pq, mean_pz)
    %% INFO
    % Calculate the transverse 2D-emittance in the canonical coordinates q and p_q.
    
    %% IN
    % var_q - Variance of the cartesian coordinates x or y.
    % var_pq - Variance of the momenta in transverse direction q.
    % var_q_pq - Covariance of q and pq.
    % mean_pz - Mean of the momenta in longitudinal direction z.

    %% OUT
    % emittance - float - If the mean of p_z is given, return the geometric rms emittance, else return the normalized rms emittance.
    
%     m_e = 9.1093837015e-31;
%     c = 299792458;

    radicand = var_q * var_pq - var_q_pq .^ 2;
    if radicand >= 0  % rounding error might produce negative (unphysical) radicand
        if nargin < 4  % if p_z is not given
            emittance = sqrt(radicand) / (Constants.eMass * Constants.c);
        else  % if p_z is given
            emittance = sqrt(radicand) / mean_pz;
        end
    else
        emittance = 0;
    end
end



function var = population_variance(a, b)
    % Compute the second central moment (population variance).
    %
    % Math here https://en.wikipedia.org/wiki/Variance#Population_variance
    % or here 10.1103/physrevstab.6.034202
    
    n = size(a, 1);
    if nargin < 2
        var = sum(a .^ 2) / n - (sum(a) / n) .^ 2;
    else
        var = sum(a .* b) / n - sum(a) * sum(b) / n ^ 2;
    end
end