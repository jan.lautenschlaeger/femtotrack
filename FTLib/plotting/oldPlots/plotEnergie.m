function [] = plotEnergy(simState, plotOptions, fig)
    disp('PLOT ENERGIE');

    % https://virtuelle-experimente.de/en/kanone/relativistisch/berechnung.php

    % set default values, when not given
    if nargin < 3
        figure();
    else
        figure(fig);
    end  

    unitEnergie = 'J';
    xLableString = 'sim Step';

    Nt = max(size(simState));
    N = simState(1).global.bunchInfo.N;
    xSteps = 1:Nt;
     
    energy = zeros(N, 1, Nt);
    for iii = 1:Nt
        momentum = sqrt(sum((simState(iii).phaseSpace.mom).^2,2));
        energy(:, :, iii) = sqrt((momentum .* Constants.c).^2 + (Constants.eMass * Constants.c^2)^2);
    end

    %% plotOptions
    if plotOptions.energyIneV
        energy = energy .* -1/Constants.eCharge;
        unitEnergie = 'eV';
    end

    if plotOptions.plotOverTime
        for iii = 1:Nt
            xSteps(iii) = simState(iii).curTime;
        end
        xLableString = 'Time in sec';
    end

    %% PLOTTING
    energy = permute(energy, [3 2 1]);

    clf;
    hold on;
    % plot trajectory
    for jjj = 1:N
        plot(xSteps, energy(:,1,jjj), '-');
    end
    
    hold off;
    grid on;
    xlabel(xLableString);
    ylabel(sprintf('Energy in %s', unitEnergie));
    %ylim([6.70287011e-27, 6.702870125e-27]);

    pos = zeros(Nt, 3);
    for iii = 1:Nt
        pos(iii,:) = simState(iii).phaseSpace.pos(1,:);
    end
    maxPos = max(pos);
    minPos = min(pos);
    durch = maxPos-minPos;

end