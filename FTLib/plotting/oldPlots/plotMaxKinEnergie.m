function [] = plotMaxKinEnergy(simState, plotOptions, fig)
    fprintf('PLOT MAX KIN ENERGIE ... ');

    % https://virtuelle-experimente.de/en/kanone/relativistisch/berechnung.php

    % set default values, when not given
    if nargin < 3
        figure();
    else
        figure(fig);
    end  

    xLableString = 'sim Step';

    Nt = max(size(simState));
    N = simState(1).global.bunchInfo.N;
    xSteps = 1:Nt;
    
    %% get data from simState
    energy = zeros(N, 1, Nt);
    for iii = 1:Nt
        momentum = sqrt(sum((simState(iii).phaseSpace.mom).^2,2));
        energy(:, :, iii) = sqrt((momentum .* Constants.c).^2 + (Constants.eMass * Constants.c^2)^2) - Constants.eMass * Constants.c^2;
    end

    energy = energy .* -1/Constants.eCharge * 1/1000;
    unitEnergie = 'keV';

    if plotOptions.plotOverTime
        for iii = 1:Nt
            xSteps(iii) = simState(iii).curTime;
        end
        xLableString = 'Time in sec';
    end

%     if plotOptions.removeDeadParticles
%         energy = energy(:,1,simState(end).phaseSpace.alive);
%     end

    %% Plot Max Energy Electron
    energy = permute(energy, [3 2 1]);
    [~,maxEnergyIndex] = max(energy(end,1,:));
    plot(xSteps, energy(:,1,maxEnergyIndex), '-');
    grid on;
    xlabel(xLableString);
    ylabel(sprintf('kin. Energy in %s', unitEnergie));
    title('Max Energy Electron');
    % title('Max Energy Electron', sprintf('pos_0=[%g,%g,%g], mom_0=[%g,%g,%g]', simState(1).phaseSpace.pos(maxEnergyIndex,1), simState(1).phaseSpace.pos(maxEnergyIndex,2), simState(1).phaseSpace.pos(maxEnergyIndex,3), simState(1).phaseSpace.mom(maxEnergyIndex,1), simState(1).phaseSpace.mom(maxEnergyIndex,2), simState(1).phaseSpace.mom(maxEnergyIndex,3)));

    fprintf('END\n');
end