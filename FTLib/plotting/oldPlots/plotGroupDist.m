function [] = plotGroupDist(bunchInfo, plotOptions, fig)
    fprintf('PLOT GROUP DIST ... ');

    % set default values, when not given
    if nargin < 3
        figure();
    else
        figure(fig);
    end   

    diffs = bunchInfo.grouping(:,2) - bunchInfo.grouping(:,1) +1;
    edges = unique(diffs);
    counts = histcounts(diffs(:), [edges;edges(end)+1]);

    % plot(edges, counts,'x-','DisplayName','Bunches');
    h = histogram(bunchInfo.bunchSizes);

    grid on;
    xlabel('Bunch Size');
    ylabel('Amount');
    legend;

    fprintf('END\n');
end