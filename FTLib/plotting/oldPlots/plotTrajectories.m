function [myfig] = plotTrajectories(simState, fieldList, plotOptions, fig, plot_how_many_max)

%% INFO
% Plot the kin, energy and the number of alive particles.

%% IN
% simState - struct - simulation particl data
% fieldList - list(stucts) - list of fields
% plotOptions - strcut - additional ploting options
% fig - int - figure number
% plot_how_many_max - float - percentage of plotted graphs between 0 and 1

%% OUT
% myfig - Figure - Figure info object

% AUTHOR: JL 2022


    fprintf('PLOT TRAJEC ... ');

    %% COLORS
    boxAlpha = 0.05;
    elecFieldCol = [0,0,1];
    magFieldCol = [0,1,0];
    visBoxCol = [1,1,1];
    killBoxCol = [1,0,0];

    % set default values, when not given
    if nargin < 4
        myfig = figure();
    else
        myfig = figure(fig);
    end 
    if nargin < 5,  plot_how_many_max = 0.1; end  % how many particle trajectories at maximum to plot

    hardMaxNumPlots = 1000;

    Nt = max(size(simState));
    N = simState(1).global.bunchInfo.N;
    NB = size(simState(1).global.bunchInfo.grouping, 1);

    plot_how_many_max = max(min(plot_how_many_max,1),0);
    plot_N = fix(N*plot_how_many_max);
    if plot_how_many_max < 1
        fprintf(2,'INFO: Plot only %i from %i 3D Trajectories! ', plot_N, N)
    end

    cols = {'r','g','b','c','m','y','k','w'};
    cols = {[1,0,0],[0,1,0],[0,0,1],[0,1,1],[1,0,1],[1,1,0]};
    colorMinMax = [0.3, 1];

    savedParPoss = zeros(N, 3,Nt);
    for iii = 1:Nt
        savedParPoss(:, :, iii) = simState(iii).phaseSpace.pos;
    end

    savedParPoss = permute(savedParPoss, [3 2 1]);
    
    % trajectories
    clf;
    hold on;
    % plot trajectory
    for jjj = 1:plot_N
        plot3(savedParPoss(:,1,jjj), savedParPoss(:,2,jjj), savedParPoss(:,3,jjj), '-', 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols,2))+1} *(rand()*(colorMinMax(2)-colorMinMax(1))+colorMinMax(1)) );
        % plot3(savedParPoss(end,1,jjj), savedParPoss(end,2,jjj), savedParPoss(end,3,jjj), 'x', 'Color', cols{mod(fix((jjj-1)/NB)+1-1, size(cols,2))+1});
    end
    
    %% plot field boxes
    for field = fieldList
        meshBound = field{1}.meshBounds;    
        plotcube([meshBound(1,2)-meshBound(1,1), meshBound(2,2)-meshBound(2,1), meshBound(3,2)-meshBound(3,1)],[meshBound(1,1), meshBound(2,1),meshBound(3,1)],.01,[1 0 0])
    
        switch field{1}.type
            case 'StaticE'
                plotcube([meshBound(1,2)-meshBound(1,1), meshBound(2,2)-meshBound(2,1), meshBound(3,2)-meshBound(3,1)],[meshBound(1,1), meshBound(2,1),meshBound(3,1)],boxAlpha,elecFieldCol)
            case 'StaticB'
                plotcube([meshBound(1,2)-meshBound(1,1), meshBound(2,2)-meshBound(2,1), meshBound(3,2)-meshBound(3,1)],[meshBound(1,1), meshBound(2,1),meshBound(3,1)],boxAlpha,magFieldCol)
            case 'DynamicE'
                plotcube([meshBound(1,2)-meshBound(1,1), meshBound(2,2)-meshBound(2,1), meshBound(3,2)-meshBound(3,1)],[meshBound(1,1), meshBound(2,1),meshBound(3,1)],boxAlpha,elecFieldCol)
            case 'DynamicB'
                plotcube([meshBound(1,2)-meshBound(1,1), meshBound(2,2)-meshBound(2,1), meshBound(3,2)-meshBound(3,1)],[meshBound(1,1), meshBound(2,1),meshBound(3,1)],boxAlpha,magFieldCol)
            otherwise
                error('ERROR.');
        end        
    end

    %% plot visual aid boxes
    for visBox = plotOptions.visBoxList
        boxBounds = visBox{1};
        plotcube([boxBounds(1,2)-boxBounds(1,1), boxBounds(2,2)-boxBounds(2,1), boxBounds(3,2)-boxBounds(3,1)],[boxBounds(1,1), boxBounds(2,1),boxBounds(3,1)],boxAlpha,visBoxCol)
    end

    %% plot killboxes
    for killBox = plotOptions.killBoxList
        boxBounds = killBox{1};
        plotcube([boxBounds(1,2)-boxBounds(1,1), boxBounds(2,2)-boxBounds(2,1), boxBounds(3,2)-boxBounds(3,1)],[boxBounds(1,1), boxBounds(2,1),boxBounds(3,1)],boxAlpha,killBoxCol)
    end
    
    view(30,45);
    hold off;
    grid on;
    xlabel('x in m');
    ylabel('y in m');
    zlabel('z in m');
%     xlim([min(x), max(x)]*3);
%     zlim([min(y), max(y)]*3);
%     ylim([min(z), max(z)]*3);

    fprintf('END\n');
end