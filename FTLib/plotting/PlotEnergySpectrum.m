function [MyFig,result] = PlotEnergySpectrum(PhaseSpace,FigNo)

AbsMomSq=PhaseSpace.mom(:,1).^2+PhaseSpace.mom(:,2).^2+PhaseSpace.mom(:,3).^2;

Energy=(AbsMomSq*Constants.c^2+ Constants.eMass^2*Constants.c^4).^0.5;

EkineV=-(Energy-Constants.eMass*Constants.c^2)/Constants.eCharge; 


MyFig=figure(FigNo)
%plot(EkineV)


Espec=histogram(EkineV,53e3: 50: 55e3 );
xlabel('Kinetic Energy [eV]')
ylabel('# particles')



result=nansum(Espec.Values);



end