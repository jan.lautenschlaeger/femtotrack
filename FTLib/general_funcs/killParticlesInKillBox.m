function phasespace = killParticlesInKillBox(phasespace, killBoxList)

%% INFO 
% Kill Particles inside any kill box. Kill boxes are
% given as x,y,z min/max matrix in meters.

%% IN
% phasespace    - struct - current phasespace
% fieldList     - struct - current fields
% killBoxList   - list{(3,2) float} - list of boxes

%% OUT
% phasespace - struct - position and momentum information of all particles

% AUTHOR: JL 2022

    if sum(phasespace.wasEmitted) == 0
        return
    end

    oldAlive = phasespace.alive;
    alive = phasespace.alive;

    % kill particle is in atleast one killBox
    for killBox = killBoxList 
        rel = and(alive,phasespace.wasEmitted);
        alive(rel) = and(alive(rel), ~checkInCube(phasespace.pos(rel,:), killBox{1}));
    end

    phasespace.mom(~alive,:) = NaN;
    phasespace.pos(~alive,:) = NaN;
    phasespace.alive = alive;

    % mark cause of death: -1 == killed kill box
    phasespace.killReason(xor(~alive, ~oldAlive)) = -3;
end