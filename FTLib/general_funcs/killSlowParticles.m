function phasespace = killSlowParticles(phasespace,  slowVelCutOffFactor, numberOfFastPart, numberOfAxis, partMass)
    
%% INFO

% This function remove/ kills all partiles with a vel lower than the a 
% fraction of the mean vel of the N fastes particels. 

%% IN

% phasespace - struct - input phase space
% numberOfFastPart - int - number of fastes particles which get averaged for the cut of vel
% slowVelCutOffFactor - float - factor the cut of vel is smaler than the averaged vel
% numberOfAxis - int - number containing the indices of the axies in consideration
%                       1 - X, 2 - Y, 3 - Z
%                       EX: 12 or 21 - X and Y
%                           13 or 31 - X and Z
%                           123 or 132 or 312 - X and Y and Z
% partMass - float - partilce mass in kg

%% OUT

% phasespace - struct - phase space with remaining particles

% AUTHOR: JL 2022

    % set default values, when not given
    if nargin < 3, numberOfFastPart = 1; end
    if nargin < 4, numberOfAxis = 123; end
    if nargin < 5, partMass = Constants.eMass; end

    slowVelCutOffFactor = abs(slowVelCutOffFactor);

    assert(numberOfAxis <= 321, 'ASSERT. Axis selection number to large.');
    assert(slowVelCutOffFactor <= 1, 'ASSERT. slowVelCutOffFactor has to be smaller 1.');

    oldAlive = phasespace.alive;
    aliveEmitted = and(phasespace.alive,phasespace.wasEmitted);

    if sum(aliveEmitted) <= numberOfFastPart
        return
    end

    switch mod(numberOfAxis,9) % == digit sum
        case 1  % X
            mom = phasespace.mom(aliveEmitted, 1);
        case 2  % Y
            mom = phasespace.mom(aliveEmitted, 2);
        case 3  % Z or XY
            if numberOfAxis > 10    % XY
                mom = phasespace.mom(aliveEmitted, 1:2);
            else    % Z
                mom = phasespace.mom(aliveEmitted, 3);
            end
        case 4  % XZ
            mom = phasespace.mom(aliveEmitted, [true, false,true]);
        case 5  % YZ
            mom = phasespace.mom(aliveEmitted, 2:3);
        case 6  % XYZ
            mom = phasespace.mom(aliveEmitted, :);
        otherwise
            error('ERROR. Axis selection number does not match.')
    end
    
    momMag = sqrt(sum(mom.^2,2));
    
    gammas = sqrt(1 + momMag.^2./(partMass * Constants.c).^2);
    velMag = momMag ./ (partMass * gammas);

    [velMag,ids] = sort(velMag, 'descend');
    fastMeanVelMag = sum(velMag(1:numberOfFastPart))./numberOfFastPart;

    cutOffVel = fastMeanVelMag * slowVelCutOffFactor;

    cutOffIndex = getClosestIndex(cutOffVel, velMag);
    if velMag(cutOffIndex) > cutOffVel
        cutOffIndex = cutOffIndex +1;
    end

    % determin to slow particles
    toSlowParts = false(size(velMag));
    toSlowParts(ids(cutOffIndex:end)) = true;

    % remove them from the alive particles
    alive = oldAlive;
    alive(aliveEmitted) = ~toSlowParts;

    % remove coresponding pos and mom, update alive vector
    phasespace.mom(~alive,:) = NaN; % TODO: only update/chage newly kill values
    phasespace.pos(~alive,:) = NaN;
    phasespace.alive = alive;

    % mark cause of death: -2 == killed by vel cut of
    phasespace.killReason(xor(~alive, ~oldAlive)) = -2;
end