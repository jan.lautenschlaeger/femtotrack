function scalFac = convPrefixToFactor(prefix)    
    
%% INFO
% Converts a string unit to its corresponding factor.
% Range from Yotta ('Y') = 10^24 to Yokto ('y') = 10^-24
% An empty string ('') returns the factor 1.

%% IN
% prefix - string - unit prefix

%% OUT
% scalFac  - int - factor

% AUTHOR: JL 2022

%% EX
% convPrefixToFactor('T') = 1e12
% convPrefixToFactor('') = 1e0
% convPrefixToFactor('u') = 1e-6

    if isempty(prefix)
        scalFac = 1;
        return
    end

    unitName =   [ 'Y',  'Z',  'E',  'P',  'T', 'G', 'M', 'k', 'h',  'd',  'c',  'm',  'u',  'n',   'p',   'f',   'a',   'z',   'y'];
    unitFactor = [1e24, 1e21, 1e18, 1e15, 1e12, 1e9, 1e6, 1e3, 1e2, 1e-1, 1e-2, 1e-3, 1e-6, 1e-9, 1e-12, 1e-15, 1e-18, 1e-21, 1e-24];

    pos = find(unitName == prefix(1));
    scalFac = unitFactor(pos);
end