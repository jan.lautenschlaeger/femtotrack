function closestIndex = getClosestIndex(pos, meshPos)

%% INFO
% Retuns the closest index in MESHPOS to POS.

%% IN
% pos       - float - position if intrest
% meshPos   - (n,1) float - mesh spacing

%% OUT
% closestIndex - int - closest index in meshPos

% AUTHOR: JL 2022


    diff = abs(meshPos - pos);
    [~, minI] = min(diff);
    closestIndex = minI(1);
end