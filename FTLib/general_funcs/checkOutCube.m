function outside = checkOutCube(points, cubeBounds)

%% INFO
% Check if points are outside a cube and returns a per point boolean.

%% IN
% points        - (n,3) - n 3-dim points
% cubeBounds    - (3,2) - min-max-limits of m-dim cube

%% OUT
% outside - boolean - per point boolean true if outside

% AUTHOR: JL 2022

    outside = or(points < cubeBounds(:,1)', points > cubeBounds(:,2)');
    outside = outside(:,1)|outside(:,2)|outside(:,3);
end