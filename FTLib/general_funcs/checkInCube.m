function inside = checkInCube(points, cubeBounds)

%% INFO
% Check if points are inside a cube and returns a per point boolean.

%% IN
% points        - (n,3) - n 3-dim points
% cubeBounds    - (3,2) - min-max-limits of m-dim cube

%% OUT
% inside - boolean - per point boolean true if inside

% AUTHOR: JL 2022

%% WARNING
% points with NaN are allways inside!

    inside = or(points < cubeBounds(:,1)', points > cubeBounds(:,2)');
    inside = inside(:,1)|inside(:,2)|inside(:,3);
    inside = ~inside;
end