function phasespace = killParticlesOutsideFields(phasespace, fieldList)

%% INFO 
% Kill Particles outside all fields.

%% IN
% phasespace    - struct - current phasespace
% fieldList     - struct - current fields
% killBoxList   - list{(3,2) float} - list of boxes

%% OUT
% phasespace - struct - position and momentum information of all particles

% AUTHOR: JL 2022

    if sum(phasespace.wasEmitted) == 0
        return
    end

    oldAlive = phasespace.alive;
    outsideAllField = false(size(oldAlive));
    
    % kill particle if outside all fields
    for field = fieldList
        outsideAllField = or(outsideAllField, checkInCube(phasespace.pos, field{1}.meshBounds));
    end
    alive = and(oldAlive, outsideAllField);

    phasespace.mom(~alive,:) = NaN;
    phasespace.pos(~alive,:) = NaN;
    phasespace.alive = alive;

    % mark cause of death: -1 == killed by leaving all field
    phasespace.killReason(xor(~alive, ~oldAlive)) = -1;
end