function dt = equalTimeSteps(varargin)

% varargin == {phasespace, bunchInfo, fieldList, windowInfo, stepCounter, olddt, dtVarargin}

%% INFO
% Return the constnst user given time step.

%% IN
%% Order in varargin:
% varargin - list - containing following in this order:
%   phasespace - struc - current partile pos, mom and alive information
%   bunchInfo - struc - information about the particle grouping
%   fieldList - list of struc - list of all fields
%   windowInfo - struc - information about the curretn window, if no window
%                      is used the window matches the size of the field
%   stepCounter - int - counter/index of the current simulation step; starting with 1
%   currentTime - float - current time of the simulation
%   olddt - float - previous time step 
%   dtVarargin - cell array/list - user given additonal input (via timeInfo.dtVarargin)

%% OUT
% dt - float - length of the next time step    

% AUTHOR: JL 2022

    dt = varargin{8}{1};
end