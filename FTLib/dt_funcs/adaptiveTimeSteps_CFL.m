function dt = adaptiveTimeSteps_CFL(CFL, varargin)

% varargin == {phasespace, bunchInfo, fieldList, windowInfo, stepCounter, currentTime, olddt, dtVarargin}

%% INFO
% Calculate the next time step interval from the current state of the
% simulation using the CFL (Courant–Friedrichs–Lewy) condition.

%% IN
% CFL - float - Courant–Friedrichs–Lewy number
% varargin - list - containing following in this order:
%   phasespace - struc - current particle information
%   bunchInfo - struc - information about the particle grouping
%   fieldList - list{fields} - list of all fields
%   windowInfo - struc - information about the curretn window, if no window
%                      is used the window matches the size of the field
%   stepCounter - int - counter/index of the current simulation step; starting with 1
%   currentTime - float - current time of the simulation
%   olddt - float - previous time step 
%   dtVarargin - cell array/list - user given additonal input (via timeInfo.dtVarargin)


%% OUT
% dt - float - length of the next time step

% AUTHOR: AB & JL 2022
    
    phasespace = varargin{1}{1};
    fieldList = varargin{1}{3};
    dtVarargin = varargin{1}{8};

    m_e = 9.1093837015e-31;
    c = 299792458;
    min_cell_size = inf;
    max_p = max(sqrt(phasespace.mom(:, 1) .^ 2 + phasespace.mom(:, 2) .^ 2 + phasespace.mom(:, 3) .^ 2));
    max_v = max_p / (m_e * sqrt(1 + (max_p / (m_e * c)) ^ 2));
    
    counter = 1;
    for f = fieldList
        % check if any particle is inside the field, emitted, alive and is not ignored
        insideField = checkInCube(phasespace.pos, f{1}.meshBounds); % remove outside field % TODO: check against window 
        insideField = and(insideField, phasespace.wasEmitted);          % remove no yet spawned particles
        insideField = and(insideField, phasespace.alive);               % remove dead particles
        for ignorBox = f{1}.ignoreBoxList                           % remove particles inside ignoreBox
            insideField = and(insideField, checkOutCube(phasespace.pos, ignorBox{1}));
        end

        if sum(insideField) ~= 0
            % calc size of "window"
            maxPos = max(phasespace.pos(insideField,:), [], 1);
            minPos = min(phasespace.pos(insideField,:), [], 1);
            closestIndex(1,1) = max(1, getClosestIndex(minPos(1), f{1}.mesh.x) -1);
            closestIndex(2,1) = max(1, getClosestIndex(minPos(2), f{1}.mesh.y) -1);
            closestIndex(3,1) = max(1, getClosestIndex(minPos(3), f{1}.mesh.z) -1);
            closestIndex(1,2) = min(f{1}.nx, getClosestIndex(maxPos(1), f{1}.mesh.x) +1);
            closestIndex(2,2) = min(f{1}.ny, getClosestIndex(maxPos(2), f{1}.mesh.y) +1);
            closestIndex(3,2) = min(f{1}.nz, getClosestIndex(maxPos(3), f{1}.mesh.z) +1);
    
            if f{1}.meshIsEquiDist.x && f{1}.meshIsEquiDist.y && f{1}.meshIsEquiDist.z
                % calc mean cell size in "window"
                meanCellSize = [sum(diff(f{1}.mesh.x(closestIndex(1,1):closestIndex(1,2))),2)/(closestIndex(1,2)-closestIndex(1,1)), ...
                                sum(diff(f{1}.mesh.y(closestIndex(2,1):closestIndex(2,2))),2)/(closestIndex(2,2)-closestIndex(2,1)), ...
                                sum(diff(f{1}.mesh.z(closestIndex(3,1):closestIndex(3,2))),2)/(closestIndex(3,2)-closestIndex(3,1))];
                
                min_cell_size = min(min_cell_size, min(meanCellSize));
            else  % using mean cell size is bogus with non-equidistant field maps!
                min_cell_size = min(min_cell_size, min([min(diff(f{1}.mesh.x(closestIndex(1,1):closestIndex(1,2)))), min(diff(f{1}.mesh.y(closestIndex(2,1):closestIndex(2,2)))), min(diff(f{1}.mesh.z(closestIndex(3,1):closestIndex(3,2))))]));
            end
        end
        counter = counter +1;
    end

    if min_cell_size ~= inf && ~isnan(max_v)
        assert(min_cell_size ~= inf, 'ASSERT. Something went wrong calculating the minimum cell size.');
        dt = CFL * min_cell_size / max_v;
    else
        dt = dtVarargin{1};
    end
    
%     fprintf('min z: %s, max_p: %s, max_v: %s, dt: %s, min_cell_size: %s\n', min(phasespace.pos(:, 3)), max_p, max_v, dt, min_cell_size)
end