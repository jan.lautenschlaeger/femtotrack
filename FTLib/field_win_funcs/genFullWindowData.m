function windowInfo = genFullWindowData(fieldList)

%% INFO
% This function generates window of the field with the same size as the
% field.

%% IN
% fieldList - list of m stucteds representing fields

%% OUT
% windowInfo - (3,2,m) int - min,max indices for x,y,z for each field

    windowInfo = zeros(3,2,0);
    counter = 1;

    for field = fieldList
        windowInfo(:,:,counter) = [1,field{1}.nx; 1,field{1}.ny; 1,field{1}.nz];     
        counter = counter +1;
    end
end