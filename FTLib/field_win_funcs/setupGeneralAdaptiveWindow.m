function window = setupGeneralAdaptiveWindow()

%% INFO
% This function set up a generly good working window for the solve if it
% need one.

    window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
    window.updateRate = 1;
    window.velMargin = 0;
    window.margin = [2,2; 2,2; 2,2];
    window.maxWindowSize = [inf, inf, inf]; 
end