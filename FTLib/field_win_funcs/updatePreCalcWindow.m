function windowInfo = updatePreCalcWindow(window, currentTime)

%% INFO
% This function updates the window information for the fields when using a
% pre calculated window. 
% 'window' struct has to contain the fields '.timeStamps' and '.windows'! 

% window
%   .timeStamps - List{float} - point in time the corresponding window
%                               becomes active; List has K elements
%   .windows - List{(3,2,nFields)} - List with windows; List has K elements

%% IN
% window - window struct - Contains all info about the window
% currentTime - float - Current simulation time 

%% OUT
% windowInfo - (3,2,nFields) int - min/max indizes for x,y,z for each field


    windowInfo = [];
    assert(all(diff(window.timeStamps)>=0), 'ASSERT. timeStamp has to be a monotonic increasing vector.')
    windowInfo = window.windows{find(diff(window.timeStamps > currentTime))};
end