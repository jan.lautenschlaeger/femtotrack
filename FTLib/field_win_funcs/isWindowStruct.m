function isWindow = isWindowStruct(window)
%% INFO
% Check if input is a valid field struct for femtoTrack, aka the struct has all
% necessary fields. 

%% IN
% field - struct - field to check

%% OUT
% isField - boolean - true if it is a struct representig a field in femtoTrack

% AUTHOR: JL 2022

    isWindow = true;
    isWindow = isWindow & isfield(window, 'mode');
    isWindow = isWindow & isfield(window, 'updateRate');
    assert(any(strcmp(possibleOptions.windowMode, window.mode)), 'ASSERT. Winodw.mode option does not exist.');
    if window.mode ==  "Adaptive"
        isWindow = isWindow & isfield(window, 'velMargin');
        isWindow = isWindow & isfield(window, 'margin');
        isWindow = isWindow & isfield(window, 'maxWindowSize');
    elseif  window.mode == "PreCalc"
        isWindow = isWindow & isfield(window, 'timeStamps');
        isWindow = isWindow & isfield(window, 'windows');
    end
end