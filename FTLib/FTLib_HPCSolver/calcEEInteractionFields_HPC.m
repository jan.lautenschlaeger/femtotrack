function [pPE, pPB] = calcEEInteractionFields_HPC(pos, mom, NB)
    
%% INFO
% This function calculates a simplified electron-electron field interaction
% by assuming a static situation and thus only calculating the pairwise
% electric field.

%% IN
% pos - (m,3) float - position of m particles
% mom - (m,3) float -  momentum of m particles
% NB - int - number of particles in bunch

%% OUT
% pPE - (NB,3) flaot - per partical resulting electric field
% pPB - (NB,3) float - per partical resulting magnetic field, currently always zero!


    pPE = zeros(NB,3);
    pPB = zeros(NB,3);

    dx = pos(:, 1) - pos(:,1)';
    dy = pos(:, 2) - pos(:,2)';
    dz = pos(:, 3) - pos(:,3)';

    dist = sqrt((dx.^2+dy.^2+dz.^2));

    ppEFieldx = dx ./ dist.^3;
    ppEFieldy = dy ./ dist.^3;
    ppEFieldz = dz ./ dist.^3;

    pPE(:,1) = Constants.eCharge * Constants.kFac * sum(ppEFieldx, 2, 'omitnan');
    pPE(:,2) = Constants.eCharge * Constants.kFac * sum(ppEFieldy, 2, 'omitnan');
    pPE(:,3) = Constants.eCharge * Constants.kFac * sum(ppEFieldz, 2, 'omitnan');
end