function [pos, mom] = doBorisPush_HPC(pos, mom, partCharge, partMass, partE, partB, dt)
    
%% INFO
% This function calculates the evolution of the positon and the momentum of 
% charged particles under the influence of a electric and a magnetic field.
% The Boris Push method is used: https://www.particleincell.com/2011/vxb-rotation/

%% IN
% pos - (m,3) float - position of m particles
% mom - (m,3) float -  momentum of m particles
% partCharge - float - charge of the particles
% partMass - float - mass of the particles
% partE - (m,3) flaot - per particle acting electric field
% partB - (m,3) flaot - per particle acting magnetic field
% dt - flaot - time step

%% OUT
% pos - (m,3) float - new position of m particles
% mom - (m,3) float -  new momentum of m particles

    % TODO: add phasespace.alive parameter?

    % adding half magnetic impulse 
    partMonMinus = mom + (partCharge * partE * dt) / 2;
    
    % rotation of speed according to B
    gamma = sqrt(1 + sum(partMonMinus.^2,2) /(partMass.^2 * (Constants.c)^2));
    t = partCharge * dt * partB ./ (2 * partMass * gamma); % /Constants.c ?
    pd = partMonMinus + cross(partMonMinus, t, 2);
    s = 2 .* t ./ (1 + sum(t.^2,2));
    partMonPlus = partMonMinus + cross(pd, s, 2);

    %adding second half of magnetic impulse
    partMon = partMonPlus + (partCharge * partE * dt) / 2;

    pos = pos + dt * (partMon ./ (gamma * partMass));
    mom = partMon;
end