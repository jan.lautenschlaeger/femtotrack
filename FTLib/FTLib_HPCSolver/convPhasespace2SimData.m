function [pos, mom, alive] = convPhasespace2SimData(phasespace, bunchInfo)

    pos = zeros(max(bunchInfo.bunchSizes), 3, bunchInfo.numBunches);
    mom = zeros(max(bunchInfo.bunchSizes), 3, bunchInfo.numBunches);
    alive = zeros(max(bunchInfo.bunchSizes), 1, bunchInfo.numBunches);

    for ii = 1:bunchInfo.numBunches
%         startBuchIdx = sum(bunchInfo.bunchSizes(1:ii-1))+1;
%         endBuchIdx = sum(bunchInfo.bunchSizes(1:ii));
%         pos(1:bunchInfo.bunchSizes(ii), :, ii) = phasespace.pos(startBuchIdx:endBuchIdx,:);
%         mom(1:bunchInfo.bunchSizes(ii), :, ii) = phasespace.mom(startBuchIdx:endBuchIdx,:);

        pos(1:bunchInfo.bunchSizes(ii), :, ii) = phasespace.pos(bunchInfo.grouping(ii,1):bunchInfo.grouping(ii,2),:);
        mom(1:bunchInfo.bunchSizes(ii), :, ii) = phasespace.mom(bunchInfo.grouping(ii,1):bunchInfo.grouping(ii,2),:);
        alive(1:bunchInfo.bunchSizes(ii), :, ii) = phasespace.alive(bunchInfo.grouping(ii,1):bunchInfo.grouping(ii,2),:);
    end
end