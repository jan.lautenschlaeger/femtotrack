function [pPE, pPB] = calcEEInteractionFields_wLorentz(pos, mom, bunchInfo)

%% INFO
% This function calculates the electron-electron field interaction with
% Lorentz boost. For this the electrons get transforamt to an mean stationary
% frame, in which the static electric field gets calculated. Then the field
% gets transforamted back to the global frame.

%% IN
% pos - (m,3) float - position of m particles
% mom - (m,3) float -  momentum of m particles
% NB - int - number of particles in bunch

%% OUT
% pPE - (NB,3) flaot - per partical resulting electric field
% pPB - (NB,3) float - per partical resulting magnetic field


    pPE = zeros(NB,3);
    pPB = zeros(NB,3);

    %% CALC gamma & co
    meanMom = sum(mom,1)./NB; % == mean() but faster
    gamma = sqrt(1 + sum(meanMom.^2,2) /(partMass*Constants.c)^2);
    velFromMom = meanMom ./ (partMass * gamma);
    boostMatrix = calcBoostMatrix(velFromMom);
    scalingFactor = diag(boostMatrix);

    ceneterPos = sum(pos,1)./NB; % == mean() but faster
    trafoPos = (pos - ceneterPos).*scalingFactor(2:4)';

    %% calc 'static' e field in moving frame
    dx = pos(:, 1) - pos(:,1)';
    dy = pos(:, 2) - pos(:,2)';
    dz = pos(:, 3) - pos(:,3)';

    dist = sqrt((dx.^2+dy.^2+dz.^2));

    ppEFieldx = dx ./ dist.^3;
    ppEFieldy = dy ./ dist.^3;
    ppEFieldz = dz ./ dist.^3;

    pPE(:,1) = Constants.eCharge * Constants.kFac * sum(ppEFieldx, 2, 'omitnan');
    pPE(:,2) = Constants.eCharge * Constants.kFac * sum(ppEFieldy, 2, 'omitnan');
    pPE(:,3) = Constants.eCharge * Constants.kFac * sum(ppEFieldz, 2, 'omitnan');

    %% does the same as loop ... but with const time
    % for n = 1 or 2 loop is sometime faster
    bM = boostMatrix;
    c = Constants.c;
    pPE(:, 1) = ((bM(2,1)*(-pPE(:,1)/c*bM(2,1)-pPE(:,2)/c*bM(3,1)-pPE(:,3)/c*bM(4,1))+bM(2,2)*(pPE(:,1)/c*bM(1,1))+bM(2,3)*(pPE(:,2)/c*bM(1,1))+bM(2,4)*(pPE(:,3)/c*bM(1,1))))*c;
    pPE(:, 2) = ((bM(3,1)*(-pPE(:,1)/c*bM(2,1)-pPE(:,2)/c*bM(3,1)-pPE(:,3)/c*bM(4,1))+bM(3,2)*(pPE(:,1)/c*bM(1,1))+bM(3,3)*(pPE(:,2)/c*bM(1,1))+bM(3,4)*(pPE(:,3)/c*bM(1,1))))*c;
    pPE(:, 3) = ((bM(4,1)*(-pPE(:,1)/c*bM(2,1)-pPE(:,2)/c*bM(3,1)-pPE(:,3)/c*bM(4,1))+bM(4,2)*(pPE(:,1)/c*bM(1,1))+bM(4,3)*(pPE(:,2)/c*bM(1,1))+bM(4,4)*(pPE(:,3)/c*bM(1,1))))*c;
    
    pPB(:, 1) = (bM(4,1)*(-pPE(:,1)/c*bM(2,3)-pPE(:,2)/c*bM(3,3)-pPE(:,3)/c*bM(4,3))+bM(4,2)*(pPE(:,1)/c*bM(1,3))+bM(4,3)*(pPE(:,2)/c*bM(1,3))+bM(4,4)*(pPE(:,3)/c*bM(1,3)));
    pPB(:, 2) = (bM(2,1)*(-pPE(:,1)/c*bM(2,4)-pPE(:,2)/c*bM(3,4)-pPE(:,3)/c*bM(4,4))+bM(2,2)*(pPE(:,1)/c*bM(1,4))+bM(2,3)*(pPE(:,2)/c*bM(1,4))+bM(2,4)*(pPE(:,3)/c*bM(1,4)));
    pPB(:, 3) = (bM(3,1)*(-pPE(:,1)/c*bM(2,2)-pPE(:,2)/c*bM(3,2)-pPE(:,3)/c*bM(4,2))+bM(3,2)*(pPE(:,1)/c*bM(1,2))+bM(3,3)*(pPE(:,2)/c*bM(1,2))+bM(3,4)*(pPE(:,3)/c*bM(1,2)));

end

function boostMatrix = calcBoostMatrix(vel)
    c = Constants.c;
    absVel = sqrt(sum(vel.^2,2));
    gamma = 1/sqrt(1-(absVel/c)^2);
    boostMatrix = [gamma, -gamma*vel(1)/c, -gamma*vel(2)/c, -gamma*vel(3)/c; ...
        -gamma*vel(1)/c, 1+(gamma-1)*(vel(1)/absVel)^2, (gamma-1)*vel(1)*vel(2)/absVel^2 (gamma-1)*vel(1)*vel(3)/absVel^2; ... 
        -gamma*vel(2)/c, (gamma-1)*vel(2)*vel(1)/absVel^2, 1+(gamma-1)*(vel(2)/absVel)^2, (gamma-1)*vel(2)*vel(3)/absVel^2; ...
        -gamma*vel(3)/c, (gamma-1)*vel(3)*vel(1)/absVel^2, (gamma-1)*vel(3)*vel(2)/absVel^2, 1+(gamma-1)*(vel(3)/absVel)^2];
end

function fieldMatrix = genRelFieldMatrix(xE, yE, zE, xB, yB, zB)
    c = Constants.c;
    fieldMatrix = [0, -xE/c, -yE/c, -zE/c; ... 
        xE/c,   0, -zB,  yB; ... 
        yE/c,  zB,   0, -xB; ...
        zE/c, -yB,  xB,   0];
end