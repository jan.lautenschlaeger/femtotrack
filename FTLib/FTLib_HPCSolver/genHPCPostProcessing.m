function [simState, debugTimes] = genHPCPostProcessing(savePos, saveMom, saveDebugTime, bunchInfo, fieldList, window, timeInfo, genSettings)

    if genSettings.saveFields
        simState(1).global.fields = fieldList; 
    else
        simState(1).global.fields = false;
    end
    simState(1).global.timeInfo = timeInfo;
    simState(1).global.bunchInfo = bunchInfo;
    simState(1).global.genSettings = genSettings;
    simState(1).global.window = window;

    pos = permute(trafoHPCData2BulkData(savePos, bunchInfo), [1,2,4,3]);
    mom = permute(trafoHPCData2BulkData(saveMom, bunchInfo), [1,2,4,3]);

    for ii = 1:timeInfo.Nt
        simState(ii).phaseSpace = trafoPosMom2Phasespace(pos, mom, ii);
        simState(ii).curTime = (ii-1) * timeInfo.dt;
        simState(ii).dt = timeInfo.dt;
        % simState(ii).windowInfo = windowInfo;

        simState(ii).debugTimes(1) = mean(saveDebugTime.FieldInt(ii,:)); 
        simState(ii).debugTimes(2) = mean(saveDebugTime.eeI(ii,:));  
        simState(ii).debugTimes(3) = mean(saveDebugTime.Boris(ii,:));  
        simState(ii).debugTimes(5) = mean(saveDebugTime.upWindow(ii,:));  
        simState(ii).debugTimes(7) = mean(saveDebugTime.saveData(ii,:));  
        debugTimes(ii,:) = simState(ii).debugTimes;
    end
end


function phasespace = trafoPosMom2Phasespace(pos,mom,step)
    phasespace.pos = pos(:,:,step);
    phasespace.mom = mom(:,:,step);

    assert(size(pos,1) == size(mom,1), 'ASSRT. Number of POS and MOM do not match.');
end

function xOut = trafoHPCData2BulkData(xIn, bunchInfo)
    xOut = [];
    counter = 1;

    for ii = 1:bunchInfo.numBunches
        xOut(counter:counter+bunchInfo.bunchSizes(ii)-1,:,1,:) = xIn(1:bunchInfo.bunchSizes(ii),:,ii,:);
        counter = counter +bunchInfo.bunchSizes(ii);
    end
end