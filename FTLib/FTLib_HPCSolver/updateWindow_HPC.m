function [windowInfo] = updateWindow_HPC(fieldList, pos, mom, margin, velMargin, maxWindowSize, partMass)
    
    %% INFO
    % This funktion updates the indices for the windows for each field
    % based on the particles position and momentum.

    %% IN
    % fieldList - list of m stucteds representing fields
    % pos - position of m particle
    % mom - momentum of m particle
    % margin - (3,1) int - added cells in min and max direction
    % velMargin - float - if greater 0, add additonal margin in mean
    %                     direction of partical movment
    % maxWindowSize - (3,1) int - maximum extend of window in x,y,z direction
    %                             in numbner of cells
    % partMass - float - partical mass, needed for velocity calculation

    %% OUT
    % windowInfo - (3,2,m) int - min,max indices for x,y,z for each field


    windowInfo = zeros(3,2,0);
    counter = 1;
    
    %% checks and fix
    margin = fix(margin);

    % mean vel. vector
    meanMom = sum(mom,1)./size(mom,1); % == mean() but faster
    gamma = sqrt(1 + sum(meanMom.^2,2) /(partMass*Constants.c)^2);
    meanVel = meanMom ./ (partMass * gamma);

    % max/min partical pos.
    maxPos = max(pos, [], 1);
    minPos = min(pos, [], 1);

    for field = fieldList

        addmargin = fix(velMargin * meanVel ./ field{1}.meanCellSize);

        closestIndex(1,1) = max(1, getClosestIndex(minPos(1), field{1}.mesh.x) - margin(1));
        closestIndex(2,1) = max(1, getClosestIndex(minPos(2), field{1}.mesh.y) - margin(2));
        closestIndex(3,1) = max(1, getClosestIndex(minPos(3), field{1}.mesh.z) - margin(3));
        closestIndex(1,2) = min(field{1}.nx, getClosestIndex(maxPos(1), field{1}.mesh.x) + margin(1));
        closestIndex(2,2) = min(field{1}.ny, getClosestIndex(maxPos(2), field{1}.mesh.y) + margin(2));
        closestIndex(3,2) = min(field{1}.nz, getClosestIndex(maxPos(3), field{1}.mesh.z) + margin(3));
        
%         if addmargin(1) < 0
%             closestIndex(1,1) = closestIndex(1,1) + addmargin(1);
%         else
%             closestIndex(1,2) = closestIndex(1,2) + addmargin(1);
%         end
% 
%         if addmargin(2) < 0
%             closestIndex(2,1) = closestIndex(2,1) + addmargin(2);
%         else
%             closestIndex(2,2) = closestIndex(2,2) + addmargin(2);
%         end
% 
%         if addmargin(3) < 0
%             closestIndex(3,1) = closestIndex(3,1) + addmargin(3);
%         else
%             closestIndex(3,2) = closestIndex(3,2) + addmargin(3);
%         end

         windowInfo(:,:,counter) = closestIndex;
         counter = counter +1;
    
         % fprintf('[%g,%g][%g,%g][%g,%g]\n',closestIndex(1,1), closestIndex(1,2) ,closestIndex(2,1), closestIndex(2,2) ,closestIndex(3,1), closestIndex(3,2));
    end

end