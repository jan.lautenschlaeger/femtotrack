function [E, B] = interpolateFields_wWindow_HPC(fieldList, pos, NB, time, windowInfo)
    
    %% INFO
    % Interpolate 3D fields at each particle positon and returen the
    % combinde E and B field for each particle. This function als uses a
    % window to limit the field and thus speed up the interpolation. 
    % Overlapping field of the same type are added. 

    %% IN
    % fieldList - list of m stucteds representing fields
    % pos - position of m particle
    % mom - momentum of m particle
    % time - float - current time in sec
    % windowInfo - (3,2,m) int - min,max indices for x,y,z for each field

    %% OUT
    % E - (m,3) - x,y,z E field info for each particle
    % B - (m,3) - x,y,z B field info for each particle

    % per step setup
    E = zeros(NB,3);
    B = zeros(NB,3);
    interpoField = zeros(NB, 3);

    counter = 1;

    for field = fieldList
        insideField = checkInCube(pos, field{1}.meshBounds); % TODO: check against window 

        if sum(insideField) ~= 0
            xRange = windowInfo(1,1,counter):windowInfo(1,2,counter);
            yRange = windowInfo(2,1,counter):windowInfo(2,2,counter);
            zRange = windowInfo(3,1,counter):windowInfo(3,2,counter);
    
            F = griddedInterpolant(field{1}.meshX(xRange, yRange, zRange), ...
                                   field{1}.meshY(xRange, yRange, zRange), ...
                                   field{1}.meshZ(xRange, yRange, zRange), ...
                                   field{1}.data.x(xRange, yRange, zRange), ...
                                   'linear', 'none');
            interpoField(insideField,1) = F(pos(insideField,1), pos(insideField,2), pos(insideField,3));
            F.Values = field{1}.data.y(xRange, yRange, zRange);
            interpoField(insideField,2) = F(pos(insideField,1), pos(insideField,2), pos(insideField,3));
            F.Values = field{1}.data.z(xRange, yRange, zRange);
            interpoField(insideField,3) = F(pos(insideField,1), pos(insideField,2), pos(insideField,3));
            
            
            switch field{1}.type
                case 'StaticE'
                    E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
                case 'StaticB'
                    B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
                case 'DynamicE'
                    interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*time + angle(interpoField));
                    E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
                case 'DynamicB'
                    interpoField = abs(interpoField) .* cos(2*pi*field{1}.frq*time + angle(interpoField));
                    B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
                otherwise
                    error('ERROR.');
            end
        end

        counter = counter +1;
    end

end