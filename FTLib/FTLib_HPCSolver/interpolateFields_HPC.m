function [E, B] = interpolateFields_HPC(fieldList, pos, NB, time)
    
    %% INFO
    % Interpolate 3D fields at each particle positon and returen the
    % combinde E and B field for each particle. Overlapping field of the 
    % same type are added. 

    %% IN
    % fieldList - list of m stucteds representing fields
    % phasespace - struct containign pos and mom info of m particle
    % time - float - current time in sec

    %% OUT
    % E - (m,3) - x,y,z E field info for each particle
    % B - (m,3) - x,y,z B field info for each particle


    % per step setup
    E = zeros(NB,3);
    B = zeros(NB,3);
    interpoField = zeros(NB, 3);

    for field = fieldList
        insideField = checkInCube(pos, field{1}.meshBounds);

        F = griddedInterpolant(field{1}.meshX, field{1}.meshY, field{1}.meshZ, field{1}.data.x, 'linear', 'none');
        interpoField(insideField,1) = F(pos(insideField,1), pos(insideField,2), pos(insideField,3));
        F.Values = field{1}.data.y;
        interpoField(insideField,2) = F(pos(insideField,1), pos(insideField,2), pos(insideField,3));
        F.Values = field{1}.data.z;
        interpoField(insideField,3) = F(pos(insideField,1), pos(insideField,2), pos(insideField,3));

        switch field{1}.type
            case 'StaticE'
                E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
            case 'StaticB'
                B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
            case 'DynamicE'
                interpoField = abs(interpoField) * cos(2*pi*field{1}.frq*time + angle(interpoField));
                E(insideField,:) = E(insideField,:) + interpoField(insideField,:);
            case 'DynamicB'
                interpoField = abs(interpoField) * cos(2*pi*field{1}.frq*time + angle(interpoField));
                B(insideField,:) = B(insideField,:) + interpoField(insideField,:);
            otherwise
                error('ERROR.');
        end
    end
end