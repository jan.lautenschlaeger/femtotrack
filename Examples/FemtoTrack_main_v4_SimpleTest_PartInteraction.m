restoredefaultpath
clear
%close all

path([pwd '\..\'],path);
absFTLibPath = importFTLib([pwd, '\..\']);
path([absFTLibPath, '\plotting\oldPlots'], path); 

path([pwd '\FTLib\field_funcs'],path);
path([pwd '\FTLib\window_funcs'],path);
path([pwd '\FTLib\runs_and_checks'],path);

%% FemtoTrack V4 %%
%%%%%%%%%%%%%%%%%%%

%% ======== SETTINGS ======================================================

% - - - - - PARTICLES - - - - - 
bunchInfo = genEqualParticleNumberDist(50, 3);     % e- per bunch; number of bunches
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,0;0,0.1;0,0.0001],[-100,100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N,[-0.1e-1,0.1e-1; -0.1e-1,0.1e-1; -0.1e-1,0.1e-1],[-100,-100; 0,0; 0,0]*Constants.eMass);
phasespace = genRandInitPhasespace(bunchInfo.N, [-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6, [-0.001,-0.001; -0.001,-0.001; -0.001,-0.001]*Constants.c*Constants.eMass);

% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = true;
genSettings.interactionType = 'BunchRel';   % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -
filepath = [pwd '\..\TestFields\twoPlates_EField_nonUni.h5'];
fieldE = importHDF5_CST(filepath, 'StaticE', 'emitter', [0, 0, 0], [1, 1, 1]);

fieldList = {fieldE};

genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TBT

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = true;        
genSettings.killBoxList = {};%{[-3,0; -3,3; -3,-0.5]*1e-6, [-3,3; -3,3; 1.5,2]*1e-6};  % min/max x,y,z of box in m 
genSettings.visBoxList = {};{[-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6}; % min/max x,y,z of box in m
genSettings.visBoxList = [fieldE.ignoreBoxList, genSettings.visBoxList];

% - - - - - PLOTTING - - - - - 
plotOptions.plotOverTime = false;
plotOptions.energyIneV = true;

% - - - - - TIME SETTINGS - - - - - 
timeInfo.dt = 1e-14;     % sec
timeInfo.Nt = 1000;
genSettings.dt_update_res = 1;  % resolution of time step updates (give step width), applies to adaptive time steps


genSettings.showInfo = false;
genSettings.saveEverNStep = 1;
genSettings.solver = 'General';    %'HPC', 'General', 'General_CInterpo'

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
window.margin = [2,2; 2,2; 2,2];
window.maxWindowSize = [inf, inf, inf];   % TBT
% ========================================================================= 
%% Advanced Settings - better do not touch, when new

% time settings
timeInfo.dtFunc = @equalTimeSteps;  % for General
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
plotOptions.visBoxList = genSettings.visBoxList;
plotOptions.killBoxList = genSettings.killBoxList;

plotOptions.solver = genSettings.solver;
if genSettings.doInteraction
    plotOptions.eeInter = genSettings.interactionType;
else
    plotOptions.eeInter = 'noInteraction';
end
plotOptions.modeWin = window.mode;

%% CHECK & FIX
window.updateRate = abs(max(1,fix(window.updateRate)));
genSettings.dt_update_res = abs(max(1,fix(genSettings.dt_update_res)));
pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
if pass, fprintf('INFO. Checks all passed.\n'); end

%% SOLVER
switch genSettings.solver
    case 'General'
        [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'General_CInterpo'
        [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC'
        [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC_CInterpo'
        [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'None'
        warning('Solver Skipped.')
        doPlotDebugTime = false;
        doPlotTrajectories = false;
        doEnergyPlot = false;
    otherwise
        error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
end

%% PLOTTING
plotDebugTimes(debugTimes, plotOptions, 1);
plotParticleDebug(simState, plotOptions, 2);
plotTrajectories(simState, fieldList, plotOptions, 3);
plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
plotBeamProps(simState, 5);
% plotGroupDist(bunchInfo, plotOptions, 6);
plotKinEnergy(simState, plotOptions, 7);
plotDt(simState, plotOptions, 8);

disp('STOP');