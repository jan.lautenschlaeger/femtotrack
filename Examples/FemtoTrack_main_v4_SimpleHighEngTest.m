restoredefaultpath
clear
%close all

path([pwd '\..\'],path);
absFTLibPath = importFTLib([pwd, '\..\']);
path([absFTLibPath, '\plotting\oldPlots'], path); 

%% FemtoTrack V4 %%
%%%%%%%%%%%%%%%%%%%

%% ======== SETTINGS ======================================================

% - - - - - PARTICLES - - - - - 
bunchInfo = genEqualParticleNumberDist(500, 10);     % e- per bunch; number of bunches
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,0;0,0.1;0,0.0001],[-100,100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N,[-0.1e-1,0.1e-1; -0.1e-1,0.1e-1; -0.1e-1,0.1e-1],[-100,-100; 0,0; 0,0]*Constants.eMass);
phasespace = genRandInitPhasespace(bunchInfo.N, [-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6, [-0.001,-0.001; -0.001,-0.001; -0.001,-0.001]*Constants.c*Constants.eMass, [0,50*1e-14]);


% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = true;
genSettings.interactionType = 'BunchRel';   % 'Class', 'BunchRel'
genSettings.interactionEnergieCutoff_eV = 1000; % in eV

% - - - - - FIELDS - - - - -
filepath = [pwd '\..\TestFields\twoPlates_EField_nonUni.h5'];
x = [-10:1:10]*1e-5;
fieldE = genFieldFromFunc(x, x, x, @ones, 'StaticE', [0, 0, 0], [1, 1, 1]*1e7, false);
% fieldE = importHDF5_CST(filepath, 'StaticE', 'emitter', [0, 0, 0], [1, 1, 1]);

fieldList = {fieldE};

% - - - - - SELECTIVELY KILL OF PARTICLES - - - - -   
genSettings.killPartOutsideFields = true;

genSettings.killPartInKillBox = true;  
genSettings.killBoxList = {};

genSettings.killSlowParts = false;            % turn on/off vel based killing
genSettings.slowVelCutOffFactor = 0.5;       % cutOffVel = slowVelCutOffFactor * fastVel
genSettings.numberOfFastPart = 2;            % number of fastes particles contributing to mean fastVe
genSettings.axisNumber = 123;                % limit to axis: 1 = X, 2 = Y, 3 = Z, 12 = XY, 13 = XZ, 23 = YZ, 123 = XYZ

% - - - - - VIS BOX - - - - - 

genSettings.visBoxList = {}; %{[-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6}; % min/max x,y,z of box in m
genSettings.visBoxList = [fieldE.ignoreBoxList, genSettings.visBoxList];

% - - - - - PLOTTING - - - - - 
plotOptions.plotOverTime = false;
plotOptions.energyIneV = true;

% - - - - - TIME SETTINGS - - - - - 
timeInfo.dt = 1e-14;     % sec
timeInfo.Nt = 1000;
genSettings.dt_update_res = 1;  % resolution of time step updates (give step width), applies to adaptive time steps


genSettings.showInfo = false;
genSettings.saveEverNStep = 1;
genSettings.solver = 'General_CInterpo';    %'HPC', 'General', 'General_CInterpo'

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
window.margin = [2,2; 2,2; 2,2];
window.maxWindowSize = [inf, inf, inf];   % TBT
% ========================================================================= 
%% Advanced Settings - better do not touch, when new

% time settings
timeInfo.dtFunc = @equalTimeSteps;  % for General
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 
filePath = "";
jsonStr = saveSettingsAsJSONFile(filePath, phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
disp("HI")
return

%% SOLVER
[simState, debugTimes] = runFTSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);


% vis/ kil box stuff
plotOptions.visBoxList = genSettings.visBoxList;
plotOptions.killBoxList = genSettings.killBoxList;

plotOptions.solver = genSettings.solver;
if genSettings.doInteraction
    plotOptions.eeInter = genSettings.interactionType;
else
    plotOptions.eeInter = 'noInteraction';
end
plotOptions.modeWin = window.mode;


%% PLOTTING
plotDebugTimes(debugTimes, plotOptions, 1);
plotParticleDebug(simState, plotOptions, 2);
plotTrajectories(simState, fieldList, plotOptions, 3);
plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
plotBeamProps(simState, 5);
% plotGroupDist(bunchInfo, plotOptions, 6);
plotKinEnergy(simState, 7);
% plotDt(simState, plotOptions, 8);

disp('STOP');