% restoredefaultpath 
clc  % clean up cmd
clear  % clean up vars
% close all  % clean up figs

path([pwd '\..\'],path);
absFTLibPath = importFTLib([pwd, '\..\']);
path([absFTLibPath, '\..\Sonstiges'], path); 
path([absFTLibPath, '\PS_Bunch_gen\Anna'], path); 
path([absFTLibPath, '\plotting\oldPlots'], path); 

%% FemtoTrack V4 %%
run_time = tic;

%% ======== SETTINGS ======================================================  % SC1_-_w_orig_params_-_on_opt_axis
% - - - - - PARTICLES - - - - -
%eauily spaced particles on icosphere
radius = 1e-6; % in m
[points,~] = icosphere(0);
points = points * radius;
n = size(points,1);

% % add center particle
% points = [points;0,0,0];
% n = n+1;
% 
% % only two particles
% r = 0.1; % in m
% points = [r,0,0;-r,0,0];
% n = 2;

%n = 100;
% radius = 1e-6; % in m
% points = fibonacci_sphere(1, n);

bunchInfo = genEqualParticleNumberDist(n, 1);
phasespace = pointCloud2Phasespace(points);

% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = true;
genSettings.interactionType = 'Class';  % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -
% no fields
fieldList = {};

genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TBT

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = false; 
genSettings.killBoxList = {};
genSettings.visBoxList = {};

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
% window.mode = 'Adaptive';  % 'Adaptive', 'PreCalc', 'None'
% window.updateRate = 1;
% window.velMargin = 0;
% window.margin = [2, 2; 2, 2; 2, 2] / 2;  % in indices (), min 1 in each
% window.maxWindowSize = [inf, inf, inf];  % TBT

% - - - - - PLOTTING - - - - - 
plotOptions.plotOverTime = false;
plotOptions.energyIneV = true;

% - - - - - TIME SETTINGS - - - - -
timeInfo.dt = 1e-13;  % with adaptive time steps, this value is used outside of any field mesh
timeInfo.Nt = 1000;
genSettings.dt_update_res = 1;  % resolution of time step updates (give step width), applies to adaptive time steps

genSettings.showInfo = false;
genSettings.saveEverNStep = 1;
genSettings.solver = 'General_CInterpo';  %'General', 'General_CInterpo', 'HPC'

% ========================================================================= 
% Advanced Settings - better do not touch, when new

% time settings
% timeInfo.dtFunc = @ (varargin) adaptiveTimeSteps_CFL(0.1, varargin); % adaptiveTimeSteps or equalTimeSteps;  % for General
timeInfo.dtFunc = @equalTimeSteps;
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
plotOptions.visBoxList = genSettings.visBoxList;
plotOptions.killBoxList = genSettings.killBoxList;

plotOptions.solver = genSettings.solver;
if genSettings.doInteraction
    plotOptions.eeInter = genSettings.interactionType;
else
    plotOptions.eeInter = 'noInteraction';
end 

if ~exist('window','var')
    window.mode = 'noWindow';
end
plotOptions.modeWin = window.mode;

%% CHECK & FIX
if exist('window.mode','var')
    if isfield( window , 'mode')   
        window.updateRate = abs(max(1, fix(window.updateRate)));
    end
end
genSettings.dt_update_res = abs(max(1, fix(genSettings.dt_update_res)));
pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
if pass, fprintf('INFO. Checks all passed.\n'); end

%% SOLVER
switch genSettings.solver
    case 'General'
        [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'General_CInterpo'
        [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC'
        [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC_CInterpo'
        [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'None'
        warning('Solver Skipped.')
    otherwise
        error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
end
fprintf('INFO. run time: %g sec\n', toc(run_time));

%% PLOTTING
% close all
plotDebugTimes(debugTimes, plotOptions, 1);
% plotParticleDebug(simState, plotOptions, 2);
plotTrajectories(simState, fieldList, plotOptions, 3);
plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
% plotBeamProps(simState, 5);
% plotGroupDist(bunchInfo, plotOptions, 6);
plotKinEnergy(simState, 7);
% plotDt(simState, plotOptions, 8);

fprintf('INFO. run time: %g sec\n', toc(run_time));
disp('FINISHED');