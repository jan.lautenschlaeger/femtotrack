restoredefaultpath
clear
%close all

path([pwd '\..\FTLib'],path);
path([pwd '\..\FTLib\plotting'],path);
path([pwd '\..\FTLib\dt_funcs'],path);
path([pwd '\..\FTLib\PS_Bunch_gen'],path);

%%%%%%%%%%%%%%%%%%%
%% FemtoTrack V4 %%
%%%%%%%%%%%%%%%%%%% 

%% BENCHMARK Settings

nMeshs = [10,50,100];
numberPartBunches = [1,10,100];
solvers = {'General', 'General_CInterpo'};

saveTimes = zeros(numel(nMeshs), numel(numberPartBunches), numel(solvers));

solverCounter = 1;
for solver = solvers
    meshCounter = 1;
    for nMesh = nMeshs
        partCounter = 1;
        for numberPartBunch = numberPartBunches
    
            %% ======== SETTINGS ======================================================
            
            % - - - - - PARTICLES - - - - - 
            bunchInfo = genEqualParticleNumberDist(50, numberPartBunch);     % e- per bunch; number of bunches
            phasespace = genRandInitPhasespace(bunchInfo.N, [-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6, [-0.001,-0.001; -0.001,-0.001; -0.001,-0.001]*Constants.c*Constants.eMass, [100, 200]*1e-14);
            
            % - - - - - INTERACTION - - - - - 
            genSettings.doInteraction = true;
            genSettings.interactionType = 'BunchRel';   % 'Class', 'BunchRel'
            
            % - - - - - FIELDS - - - - -
            %nMesh = 10;
            fieldE = genFieldFromFunc(linspace(-1e-5,1e-5,nMesh), linspace(-1e-5,1e-5,nMesh), linspace(-1e-5,1e-5,nMesh), @ones, 'StaticE', [0, 0, 0], [1, 1, 1],  false);
            fieldList = {fieldE};
            
            genSettings.saveFields = false;
            genSettings.saveFieldInWindow = false; %TODO
            
            % - - - - - VIS/KILL BOX - - - - - 
            genSettings.killPartOutsideFields = true;        
            genSettings.killBoxList = {};%{[-3,0; -3,3; -3,-0.5]*1e-6, [-3,3; -3,3; 1.5,2]*1e-6};  % min/max x,y,z of box in m 
            genSettings.visBoxList = {};{[-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6}; % min/max x,y,z of box in m
            genSettings.visBoxList = [fieldE.ignoreBoxList, genSettings.visBoxList];
            
            % - - - - - PLOTTING - - - - - 
            doPlotDebugTime = true;
            doPlotParticleDebug = true; 
            doPlotTrajectories = false;
            doPlotTrajectoriesXYZ = false;
            doPlotGroupSizeDist = false; 
            doEnergyPlot = false;
            plotOptions.plotOverTime = false;
            plotOptions.energyIneV = true;
            
            % - - - - - TIME SETTINGS - - - - - 
            timeInfo.dt = 1e-14;     % sec
            timeInfo.Nt = 1000;
            
            genSettings.showInfo = false;
            genSettings.saveEverNStep = 1;
            genSettings.solver = solver{1}; %'General_CInterpo';    % 'General', 'General_CInterpo', 'HPC'
            
            % - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
            window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
            window.updateRate = 1;
            window.velMargin = 0;
            window.margin = [2,2; 2,2; 2,2];
            window.maxWindowSize = [inf, inf, inf];   % TBT
            % ========================================================================= 
            %% Advanced Settings - better do not touch, when new
            
            % time settings
            timeInfo.dtFunc = @equalTimeSteps;  % for General
            timeInfo.dtVarargin = {timeInfo.dt};  % for General
            % ========================================================================= 
            
            % vis/ kil box stuff
            plotOptions.visBoxList = genSettings.visBoxList;
            plotOptions.killBoxList = genSettings.killBoxList;
            
            plotOptions.solver = genSettings.solver;
            plotOptions.eeInter = genSettings.interactionType;
            plotOptions.modeWin = window.mode;
            plotOptions.plotRes = 1;   % reduce the points used for plotting, higher number == coarser plot
            
            %% CHECK & FIX
            window.updateRate = abs(max(1,fix(window.updateRate)));
            pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
            if pass, fprintf('INFO. Checks all passed.\n'); end
            
            startSolverTime = tic;
             
            %% SOLVER
            switch genSettings.solver
                case 'General'
                    [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
                case 'General_CInterpo'
                    [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
                case 'HPC'
                    [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
                case 'HPC_CInterpo'
                    [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
                case 'None'
                    warning('Solver Skipped.')
                    doPlotDebugTime = false;
                    doPlotTrajectories = false;
                    doEnergyPlot = false;
                otherwise
                    error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
            end
            
            saveTimes(meshCounter, partCounter, solverCounter) = toc(startSolverTime);

            %% PLOTTING
            if doPlotDebugTime
                plotDebugTimes(debugTimes, plotOptions, 1);
            end
            
            if doPlotParticleDebug
                plotParticleDebug(simState, plotOptions, 2);
            end
            
            if doPlotTrajectories
                plotTrajectories(simState, fieldList, plotOptions, 3);
            end
            
            if doPlotTrajectoriesXYZ
                plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
            end
            
            if doPlotGroupSizeDist
                plotGroupDist(bunchInfo, plotOptions, 5);
            end
            
            if doEnergyPlot
                % plotEnergy(simState, plotOptions, 6);
                plotKinEnergy(simState, plotOptions, 7);
            end

            partCounter = partCounter +1;
        end
        meshCounter =  meshCounter +1;
    end
    solverCounter = solverCounter +1;
end

%% PLOT BENCHMARK Data

solverCounter = 1;
for solver = solvers
    figure();
    h = heatmap(numberPartBunches,nMeshs,saveTimes(:,:,solverCounter));
    
    h.Title = sprintf('Solver: %s', solver{1});
    h.XLabel = 'Number of 50 Particle Bunches';
    h.YLabel = 'Mesh x,y,z size';
    solverCounter = solverCounter +1;
end

disp('STOP');