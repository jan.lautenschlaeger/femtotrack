clear
close all

absFTLibPath = importFTLib(pwd);

%% FemtoTrack V4 %%

scalingFac = 10^(0);
x = -1:0.03:1;
y = -1:0.025:1;
z = -1:0.02:1;
x = x * scalingFac;
y = y * scalingFac;
z = z * scalingFac;



% - - - - - PARTICLES - - - - - 
% bunchInfo = genEqualParticleNumberDist(50, 3);     % e- per bunch; number of bunches
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,0;0,0.1;0,0.0001],[-100,100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N,[-0.1e-1,0.1e-1; -0.1e-1,0.1e-1; -0.1e-1,0.1e-1],[-100,-100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N, [-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6, [0,0; 0,0; 0,0]*Constants.eMass);

% bunchInfo = genEqualParticleNumberDist(5, 1);     % e- per bunch; number of bunches
% beta0=0.31;
% gamma0=1/sqrt(1-beta0^2);
% vel = [-10^-19,10^-19; -10^-19,10^-19; 1,1]*beta0*Constants.c;
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,30; -10, 10; -580,-420]*1e-9,gamma0.*vel.*Constants.eMass);

%[phasespace, bunchInfo] = importPIT("C:/UN/FemtoTrack");

%bunchInfo = genEqualParticleNumberDist(100, 1); 
[phasespace,bunchInfo] = genGaussianBunch(100);

%[phasespace, bunchInfo] = importPIT("C:/UN/myFemtoTrack");


% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = false;
genSettings.interactionType = 'BunchRel';   % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -
filePath = 'C:\UN\FemtoTrack_SOI_fields\';
fieldFrq = 149.896229e12;
%fieldAmp=6.40e8;
%fieldE = importHDF5_CST([filePath,'e-field (f=f0) [pw].h5'], 'DynamicE', 'SOIEField', [0,0,0], [1,1,1]*fieldAmp, fieldFrq,0);
%fieldB = importHDF5_CST([filePath, 'h-field (f=f0) [pw].h5'], 'DynamicH', 'SOIBField', [0,0,0], [1,1,1]*fieldAmp*Constants.mu0, fieldFrq,0);

%fieldList = {fieldE,fieldB};


genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TODO

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = true;        
genSettings.killBoxList = {};%{[-3,0; -3,3; -3,-0.5]*1e-6, [-3,3; -3,3; 1.5,2]*1e-6};  % min/max x,y,z of box in m 
%genSettings.visBoxList = {[-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6}; % min/max x,y,z of box in m
%genSettings.visBoxList = [fieldInjector.ignoreBoxList, genSettings.visBoxList];


% - - - - - TIME SETTINGS - - - - - 
timeInfo.dt = 6.6e-15 /80;     % sec
timeInfo.Nt = 500 *80;

genSettings.dt_update_res = min(timeInfo.Nt, 1);  % resolution of time step updates (give step width), applies to adaptive time steps


genSettings.showInfo = false;
genSettings.saveEverNStep = 1;
genSettings.solver = 'General'; % 'General_CInterpo';    %'HPC', 'General', 'General_CInterpo'

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
window.margin = [2,2; 2,2; 2,2];
window.maxWindowSize = [inf, inf, inf];   % TBT
% ========================================================================= 
%% Advanced Settings - better do not touch, when new

% time settings
timeInfo.dtFunc = @equalTimeSteps;  % for General
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
%plotOptions.visBoxList = genSettings.visBoxList;
%plotOptions.killBoxList = genSettings.killBoxList;

% %% CHECK & FIX
% window.updateRate = abs(max(1,fix(window.updateRate)));
% pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
% if pass, fprintf('INFO. Checks all passed.\n'); end

%% SOLVER
% switch genSettings.solver
%     case 'General'
%         [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'General_CInterpo'
%         [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'HPC'
%         [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'HPC_CInterpo'
%         [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'None'
%         warning('Solver Skipped.')
%         doPlotDebugTime = false;
%         doPlotTrajectories = false;
%         doEnergyPlot = false;
%     otherwise
%         error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
% end


totalRuns=1
result=zeros(1,totalRuns);
fieldAmp=zeros(1,totalRuns);
for run=1:totalRuns

    fieldAmp(1,run)=6.4e8;%+ run*2.5e6;

    fieldE = importHDF5_CST([filePath,'e-field (f=f0) [pw].h5'], 'DynamicE', 'SOIEField', [0,0,0], [1,1,1]*fieldAmp(1,run), fieldFrq,0);
    fieldB = importHDF5_CST([filePath, 'h-field (f=f0) [pw].h5'], 'DynamicH', 'SOIBField', [0,0,0], [1,1,1]*fieldAmp(1,run)*Constants.mu0, fieldFrq,0);

    fieldList = {fieldE,fieldB};

    %% CHECK & FIX
    window.updateRate = abs(max(1,fix(window.updateRate)));
    pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
    if pass, fprintf('INFO. Checks all passed.\n'); end

    [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    simStateReduced=simState(1:80:end);
    %plotKinEnergy(simStateReduced,11)
    [MyFig,alive]=plotTrajectories(simStateReduced,12);
    result(1,run)=alive(end,end);
    pause(10)
    saveas(MyFig,['./out/Trajectories' num2str(run) '.png'])
end

figure(44)
plot(fieldAmp*10^-6,result/phasespace.N*100)
xlabel('Laser Amplitude [MV/m]')
ylabel('Particle survival [%]')

%av=StatisticsFromSimState(simStateReduced,true);

disp('STOP');