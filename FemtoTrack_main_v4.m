restoredefaultpath 
% clc  % clean up cmd
clear  % clean up vars
% close all  % clean up figs

absFTLibPath = importFTLib(pwd);
path([absFTLibPath, '\plotting\oldPlots'], path); 
inDir = [pwd '\in'];
outDir = [pwd '\out'];

%%%%%%%%%%%%%%%%%%%
%% FemtoTrack V4 %%
%%%%%%%%%%%%%%%%%%% 

scalingFac = 10^(0);
x = -1:0.03:1;
y = -1:0.025:1;
z = -1:0.02:1;
x = x * scalingFac;
y = y * scalingFac;
z = z * scalingFac;

%% ======== SETTINGS ======================================================

% - - - - - PARTICLES - - - - - 
bunchInfo = genEqualParticleNumberDist(50, 3);     % e- per bunch; number of bunches
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,0;0,0.1;0,0.0001],[-100,100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N,[-0.1e-1,0.1e-1; -0.1e-1,0.1e-1; -0.1e-1,0.1e-1],[-100,-100; 0,0; 0,0]*Constants.eMass);
phasespace = genRandInitPhasespace(bunchInfo.N, [-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6, [0,0; 0,0; 0,0]*Constants.eMass);

% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = true;
genSettings.interactionType = 'BunchRel';   % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -
emitterfilepath = [inDir '\emitter_meshres50.h5'];
injectorfilepath = [inDir '\injector_meshres50.h5'];
fieldEmitter = importHDF5_CST(emitterfilepath, 'StaticE', 'emitter', [0, 0, 0], [1, 1, 1]);
fieldInjector = importHDF5_CST(injectorfilepath, 'StaticE', 'injector', [0, 0, 0], [1, 1, 1]);
fieldInjector.ignoreBoxList = {[
    min(h5read(emitterfilepath, '/Mesh line x')), max(h5read(emitterfilepath, '/Mesh line x'));
    min(h5read(emitterfilepath, '/Mesh line y')), max(h5read(emitterfilepath, '/Mesh line y'));
    min(h5read(emitterfilepath, '/Mesh line z')), max(h5read(emitterfilepath, '/Mesh line z'))]};

fieldList = {fieldEmitter, fieldInjector};

genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TBT

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = true;        
genSettings.killBoxList = {};%{[-3,0; -3,3; -3,-0.5]*1e-6, [-3,3; -3,3; 1.5,2]*1e-6};  % min/max x,y,z of box in m 
genSettings.visBoxList = {[-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6}; % min/max x,y,z of box in m
genSettings.visBoxList = [fieldInjector.ignoreBoxList, genSettings.visBoxList];

% - - - - - PLOTTING - - - - - 
plotOptions.plotOverTime = false;
plotOptions.energyIneV = true;

% - - - - - TIME SETTINGS - - - - - 
timeInfo.dt = 1e-14;     % sec
timeInfo.Nt = 1000;
genSettings.dt_update_res = 1;  % resolution of time step updates (give step width), applies to adaptive time steps

genSettings.showInfo = false;
genSettings.saveEverNStep = 1;
genSettings.solver = 'General_CInterpo';    % 'General', 'General_CInterpo', 'HPC'

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
window.margin = [2,2; 2,2; 2,2];
window.maxWindowSize = [inf, inf, inf];   % TBT
% ========================================================================= 
%% Advanced Settings - better do not touch, when new

% time settings
timeInfo.dtFunc = @equalTimeSteps;  % for General
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
plotOptions.visBoxList = genSettings.visBoxList;
plotOptions.killBoxList = genSettings.killBoxList;

plotOptions.solver = genSettings.solver;
if genSettings.doInteraction
    plotOptions.eeInter = genSettings.interactionType;
else
    plotOptions.eeInter = 'noInteraction';
end
plotOptions.modeWin = window.mode;

%% CHECK & FIX
window.updateRate = abs(max(1,fix(window.updateRate)));
genSettings.dt_update_res = abs(max(1,fix(genSettings.dt_update_res)));
pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
if pass, fprintf('INFO. Checks all passed.\n'); end

%% SOLVER
switch genSettings.solver
    case 'General'
        [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'General_CInterpo'
        [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC'
        [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'HPC_CInterpo'
        [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    case 'None'
        warning('Solver Skipped.')
        doPlotDebugTime = false;
        doPlotTrajectories = false;
        doEnergyPlot = false;
    otherwise
        error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
end

%% PLOTTING
plotDebugTimes(debugTimes, plotOptions, 1);
plotParticleDebug(simState, plotOptions, 2);
plotTrajectories(simState, fieldList, plotOptions, 3);
plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
plotBeamProps(simState, 5);
% plotGroupDist(bunchInfo, plotOptions, 6);
plotKinEnergy(simState, plotOptions, 7);
plotDt(simState, plotOptions, 8);

disp('STOP');