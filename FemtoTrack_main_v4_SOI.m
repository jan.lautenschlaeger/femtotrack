clear
close all

absFTLibPath = importFTLib(pwd);

%% FemtoTrack V4 %%

% scalingFac = 10^(0);
% x = -1:0.03:1;
% y = -1:0.025:1;
% z = -1:0.02:1;
% x = x * scalingFac;
% y = y * scalingFac;
% z = z * scalingFac;



% - - - - - PARTICLES - - - - - 
% bunchInfo = genEqualParticleNumberDist(50, 3);     % e- per bunch; number of bunches
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,0;0,0.1;0,0.0001],[-100,100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N,[-0.1e-1,0.1e-1; -0.1e-1,0.1e-1; -0.1e-1,0.1e-1],[-100,-100; 0,0; 0,0]*Constants.eMass);
% phasespace = genRandInitPhasespace(bunchInfo.N, [-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6, [0,0; 0,0; 0,0]*Constants.eMass);

% bunchInfo = genEqualParticleNumberDist(5, 1);     % e- per bunch; number of bunches
% beta0=0.31;
% gamma0=1/sqrt(1-beta0^2);
% vel = [-10^-19,10^-19; -10^-19,10^-19; 1,1]*beta0*Constants.c;
% phasespace = genRandInitPhasespace(bunchInfo.N,[0,30; -10, 10; -580,-420]*1e-9,gamma0.*vel.*Constants.eMass);

%[phasespace, bunchInfo] = importPIT("C:/UN/FemtoTrack");

%bunchInfo = genEqualParticleNumberDist(100, 1); 
%[phasespace,bunchInfo] = genGaussianBunch(1000);

%[phasespace, bunchInfo] = importPIT("C:/UN/myFemtoTrack");


% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = false;
genSettings.interactionType = 'BunchRel';   % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -
filePath = 'C:\UN\FemtoTrack_SOI_fields\'; 
fieldFrq = 149.896229e12;

fieldAmp=6.375e8;
fieldE = importHDF5_CST([filePath,'e-field (f=f0) [pw].h5'], 'DynamicE', 'SOIEField', [0,0,0], [1,1,1]*fieldAmp, fieldFrq,0);
fieldB = importHDF5_CST([filePath, 'h-field (f=f0) [pw].h5'], 'DynamicH', 'SOIBField', [0,0,0], [1,1,1]*fieldAmp*Constants.mu0, fieldFrq,0);
fieldList = {fieldE,fieldB};


genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TODO

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = true;        
genSettings.killBoxList = {};%{[-3,0; -3,3; -3,-0.5]*1e-6, [-3,3; -3,3; 1.5,2]*1e-6};  % min/max x,y,z of box in m 
%genSettings.visBoxList = {[-0.1,0.1; -0.1,0.1; -0.1,0.1]*1e-6}; % min/max x,y,z of box in m
%genSettings.visBoxList = [fieldInjector.ignoreBoxList, genSettings.visBoxList];



% - - - - - TIME SETTINGS - - - - - 
timeInfo.dt = 6.6e-15 /80;     % sec
timeInfo.Nt = 500 *80;          %DLA cells * steps per cell

genSettings.dt_update_res = min(timeInfo.Nt, 1);  % resolution of time step updates (give step width), applies to adaptive time steps


genSettings.showInfo = false;
genSettings.saveEverNStep = 80;
genSettings.solver =  'General_CInterpo';    %'HPC', 'General', 'General_CInterpo'

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';    % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
genSettings.killSlowParts = true;    % turn on/off vel based killing
genSettings.slowVelCutOffFactor = 0.95;     % cutOffVel = slowVelCutOffFactor * fastVel
genSettings.numberOfFastPart = 10;     % number of fastes particles contributing to mean fastVel
genSettings.axisNumber = 3;       % limit to axis: 1 = X, 2 = Y, 3 = Z, 12 = XY, 13 = XZ, 23 = YZ, 123 = XYZ
window.margin = [1,1; 1,1; 1,1]; %[2,2; 2,2; 2,2];
window.maxWindowSize = [inf, inf, inf];   % TBT
% ========================================================================= 
%% Advanced Settings - better do not touch, when new

% time settings
timeInfo.dtFunc = @equalTimeSteps;  % for General
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
%plotOptions.visBoxList = genSettings.visBoxList;
%plotOptions.killBoxList = genSettings.killBoxList;

% %% CHECK & FIX
% window.updateRate = abs(max(1,fix(window.updateRate)));
% pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
% if pass, fprintf('INFO. Checks all passed.\n'); end

%% SOLVER
% switch genSettings.solver
%     case 'General'
%         [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'General_CInterpo'
%         [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'HPC'
%         [simState, debugTimes] = runHPCSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'HPC_CInterpo'
%         [simState, debugTimes] = runHPCSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
%     case 'None'
%         warning('Solver Skipped.')
%         doPlotDebugTime = false;
%         doPlotTrajectories = false;
%         doEnergyPlot = false;
%     otherwise
%         error(['ERROR. Selected solver does not exist. Try :', char(join(possibleOptions.solver, ', ')), '.'])
% end


Scan.para1=1e-5/(0.3261) * 5*(-4:10);
Scan.para2=1e-9* 3*(-4:6);
%Scan.para1=6.2e8+ 5e6 *(1:8);
%Scan.para2=1*(-20:10);
Scan.totalRuns=length(Scan.para1)*length(Scan.para2);
fprintf('TotalRuns: %d \n', Scan.totalRuns)
Scan.result=zeros(Scan.totalRuns,1);
run=0;
for i2=1:length(Scan.para2)
    for i1=1:length(Scan.para1)
        run=run+1;
        Scan.runs(run,1)=run;
        Scan.para1vec(run,1)=Scan.para1(i1);
        Scan.para2vec(run,1)=Scan.para2(i2);
    end
end
Scan.Sweeptable= table; % (runs, para1vec, para2vec, result  );
Scan.Sweeptable.runs=Scan.runs;
Scan.Sweeptable.para1vec=Scan.para1vec;
Scan.Sweeptable.para2vec=Scan.para2vec;
Scan.Sweeptable.result=Scan.result;
%fieldAmp=zeros(1,totalRuns);



for run=1:Scan.totalRuns
    fprintf('Run: %d \n', run)

    x_angle=Scan.Sweeptable.para1vec(run);
    x_position=Scan.Sweeptable.para2vec(run);
    dE_eV=35;
    dphi_deg=-6;%Scan.Sweeptable.para2vec(run);


    clear phasespace;
    [phasespace,bunchInfo] = genGaussianBunch(1000,x_angle,x_position,dE_eV,dphi_deg);

    %fieldAmp=Scan.Sweeptable.para1vec(run);
    %fieldE = importHDF5_CST([filePath,'e-field (f=f0) [pw].h5'], 'DynamicE', 'SOIEField', [0,0,0], [1,1,1]*fieldAmp, fieldFrq,0);
    %fieldB = importHDF5_CST([filePath, 'h-field (f=f0) [pw].h5'], 'DynamicH', 'SOIBField', [0,0,0], [1,1,1]*fieldAmp*Constants.mu0, fieldFrq,0);
    %fieldList = {fieldE,fieldB};

    %% CHECK & FIX
    window.updateRate = abs(max(1,fix(window.updateRate)));
    pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
    if pass, fprintf('INFO. Checks all passed.\n'); end

    [simState, debugTimes] = runGeneralSolver(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    %simStateReduced=simState(1:80:end);
    MyFig=plotKinEnergy(simState,11);
    %[MyFig,alive_]=plotTrajectories(simStateReduced,12);
    saveas(MyFig,['./out/Trajectories' num2str(run) '.png'])
    %pause(3)

    try
        [MyFig,alive]=PlotEnergySpectrum(simState(475).phaseSpace,13);
        saveas(MyFig,['./out/Spectrum' num2str(run) '.png'])
        Scan.result(run,1)=alive;
        fprintf('Result: %d \n', Scan.result(run,1))
    catch
        Scan.result(run,1)=0;
        fprintf('Result: %d \n', Scan.result(run,1))
    end
    %Scan.result(run,1)=alive(end,end);
    Scan.Sweeptable.result(run)=Scan.result(run,1);

    
end

run=0;
for i2=1:length(Scan.para2)
    for i1=1:length(Scan.para1)
           run=run+1;
           Scan.Resultmatrix(i1,i2)=Scan.Sweeptable.result(run);
    end
end

save('./out/Scan.mat','Scan')

figure(20)
colormap jet
imagesc(Scan.para1vec,Scan.para2vec,Scan.Resultmatrix'/phasespace.N*100)
set(gca,'YDir','normal')
%caxis([0 100]);
cb=colorbar;
cb.Label.String='Transmission [%]' 
xlabel(' x_0'' [rad] ')
ylabel('x_0 [m]')
%xlabel(' Laser Amplitude [V/m] ')
%ylabel('Injection offset [eV]')
%ylabel('\Delta\phi [°]')
set(findall(gcf,'-property','FontSize'),'FontSize',16)
saveas(gcf,'./out/Survival2D.png')

figure(44)
plot(1:Scan.totalRuns,Scan.result'/phasespace.N*100)
xlabel('run #')
%xlabel('Laser Amplitude [MV/m]')
ylabel('Particle survival [%]')
saveas(gcf,'./out/Survival.png')

%simStateStart=simState(1:10:300);
%avKinEn=calcAvKinEn([simStateStart.phaseSpace],2);


%av=StatisticsFromSimState(simStateReduced,true);

%ttt=[simStateReduced.phaseSpace];
%calcAvKinEn([simStateReduced.phaseSpace],2)

%PlotEnergySpectrum(simState(38000).phaseSpace)



%plotKinEnergy(simStateStart,10);


disp('STOP');