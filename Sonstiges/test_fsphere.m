
n = 10000;
radius = 1;
points = fibonacci_sphere(1, n);

% [points,ff] = icosphere(5);

figure(1);

plot3(points(:,1),points(:,2),points(:,3),'x');
xlabel('x');
ylabel('y');
zlabel('z');
grid on;


figure(2);
[k1,av1] = convhull(points(:,1),points(:,2),points(:,3));
fprintf("Vol: %g; (theo: %g)\n", av1, 4/3*pi*radius^3);
trisurf(k1,points(:,1),points(:,2),points(:,3),'FaceColor','cyan');

xlabel('x');
ylabel('y');
zlabel('z');
grid on;


nTri = size(k1,1);
areas = zeros(nTri,1);
dists = zeros(nTri,2);
for ii = 1: nTri
    pA = points(k1(ii,1),:);
    pB = points(k1(ii,2),:);
    pC = points(k1(ii,3),:);
    vecAB = pA-pB;
    vecAC = pA-pC;
    vecBC = pA-pC;

    distAB = sqrt(sum(vecAB.^2,2));
    distAC = sqrt(sum(vecAC.^2,2));
    distBC = sqrt(sum(vecBC.^2,2));
    
    dists(ii,1) = distAB;
    dists(ii,2) = distAC;
    dists(ii,3) = distBC;

    areas(ii) = 0.5 * sqrt(sum(cross(vecAB,vecAC).^2,2));
end

figure(3);
histogram(areas);
title('Area Histo');
xlabel('Area');
ylabel('#');
grid on;
fprintf("Area: std = %g\n", std(areas));


figure(4);
histogram(dists);
title('Point Dist Histo');
xlabel('Dist');
ylabel('#');
grid on;
fprintf("Dist: std = %g\n", std(dists));
