function points = fibonacci_sphere(radius, n)

    % set default values, when not given
    if nargin < 2, n = 100; end

    points = zeros(n, 3); 
    phi = pi * (3 - sqrt(5));           % golden angle in rad

    for ii = 1:n
        y = 1 - (ii / n) * 2;       % y goes from 1 to -1 
        if y < -1
            disp("HI")
        end
        rho = radius * sqrt(1-y*y);     % radisu at y
        theta = phi * ii;               % golden angle increment

        points(ii,1) = cos(theta) * rho;    % x
        points(ii,2) = radius * y;          % y
        points(ii,3) = sin(theta) * rho;    % z
    end

end
