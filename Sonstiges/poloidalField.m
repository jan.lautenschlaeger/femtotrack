function [bpFieldX,bpFieldY,bpFieldZ] = poloidalField(meshX,meshY,meshZ,R,a,IP)
    BP = Constants.mu0 * IP / (2*pi*(0.4*a)^2);
    bpFieldX = zeros(size(meshX));
    bpFieldY = zeros(size(meshX));
    bpFieldZ = zeros(size(meshX));
    
    bpFieldX = -BP .* meshZ .* meshX ./ sqrt(meshX.^2+meshY.^2);
    bpFieldY = -BP .* meshZ .* meshY ./ sqrt(meshX.^2+meshY.^2);
    bpFieldZ = BP .* (meshX.^2 + meshY.^2 - R.^2);
end