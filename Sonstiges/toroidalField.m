function [btFieldX,btFieldY,btFieldZ] = toroidalField(meshX,meshY,meshZ,n,N,IT)
    BT = Constants.mu0 * n * N * IT;
    btFieldX = zeros(size(meshX));
    btFieldY = zeros(size(meshX));
    btFieldZ = zeros(size(meshX));
    
    btFieldX = -BT .* meshY ./ (2*pi*(meshX.^2+meshY.^2));
    btFieldY =  BT .* meshX ./ (2*pi*(meshX.^2+meshY.^2));
end