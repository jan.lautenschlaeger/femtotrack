%% TEST Field Vis
clear all
close all


path([pwd '\..\FTLib'],path);

% filePath = 'I:\Benutzer\Jan\Dokumente\Studium\TU_Job\FemtoTrack\sandbox\test_h5_files\TotalField_rect_efield(f=f0).h5';
% filePath = 'I:\Benutzer\Jan\Dokumente\Studium\TU_Job\FemtoTrack\sandbox\test_h5_files\twoPlates_EField_nonUni.h5';
% fieldType = 'StaticE';
% field = importHDF5(filePath, fieldType);

x = -3:0.3:3;
y = -3:0.3:3;
z = -0.7:0.1:0.7;
fieldBT = genFieldFromFuncAdv(x,y,z, @(mX,mY,mZ) toroidalField(mX,mY,mZ,2048,18,1400), 'StaticB');
fieldBP = genFieldFromFuncAdv(x,y,z, @(mX,mY,mZ) poloidalField(mX,mY,mZ,2.4,0.72,1.5e6), 'StaticB');

field = fieldBP;

%% PLOT
[X,Y,Z] = meshgrid(field.mesh.y,field.mesh.x,field.mesh.z);

figure(1);
quiver3(X,Y,Z,field.data.x,field.data.y,field.data.z);
xlabel('x in m');
ylabel('y in m');
zlabel('z in m');
fileName = split(filePath,{'//','\\','/','\'});
fileName = fileName(end);
title(sprintf('%s - %s', fileName{1},fieldType), 'Interpreter', 'none');
grid on;
