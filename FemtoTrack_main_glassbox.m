% restoredefaultpath 
clc  % clean up cmd
clear  % clean up vars
close all  % clean up figs

absFTLibPath = importFTLib(pwd);
path([absFTLibPath, '\PS_Bunch_gen\Anna'], path); 
%path([absFTLibPath, '\plotting\oldPlots'], path); 
inDir = [pwd '\in'];
outDir = [pwd '\out'];

%% FemtoTrack V4 %%
run_time = tic;

%% ======== SETTINGS ======================================================  % SC1_-_w_orig_params_-_on_opt_axis
% - - - - - PARTICLES - - - - -
%N_p = 4;  % e- per bunch
%N_b = 1000;  % number of bunches (bunches do not interact with each other by definition)



% - - - - - INTERACTION - - - - - 
genSettings.doInteraction = true;
genSettings.interactionType = 'BunchRel';  % 'Class', 'BunchRel'

% - - - - - FIELDS - - - - -

field_emitter_1 = importHDF5_CST('C:\UN\GlassboxForFT\EmitterField_tet\Export\3d\E-Field [Es]_1.h5', 'StaticE', 'emitter_1', [0, 0, 0], [1,1,1], 0, 0, 10^-6);
field_emitter_2 = importHDF5_CST('C:\UN\GlassboxForFT\EmitterField_tet\Export\3d\E-Field [Es]_2.h5', 'StaticE', 'emitter_1', [0, 0, 0], [1,1,1], 0, 0, 10^-6);
field_emitter_2 = addLocalFieldOverwrite(field_emitter_1, field_emitter_2); 
field_emitter_3 = importHDF5_CST('C:\UN\GlassboxForFT\EmitterField_tet\Export\3d\E-Field [Es]_3.h5', 'StaticE', 'emitter_1', [0, 0, 0], [1,1,1], 0, 0, 10^-6);
field_emitter_3 = addLocalFieldOverwrite(field_emitter_2, field_emitter_3);

%field_emitter_2 = importHDF5_CST([inDir '\emitter_2.h5'], 'StaticE', 'emitter_2', [0, 0, 0], scaling, 0, 0, 10^-6);
% field_emitter_2 = addLocalFieldOverwrite(field_emitter_1, field_emitter_2); 
% field_emitter_3 = importHDF5_CST([inDir '\emitter_3.h5'], 'StaticE', 'emitter_3', [0, 0, 0], scaling, 0, 0, 10^-6);
% field_emitter_3 = addLocalFieldOverwrite(field_emitter_2, field_emitter_3); 
% field_injector_1 = importHDF5_CST([inDir '\injector_1.h5'], 'StaticE', 'injector_1', [0, 0, 0], scaling, 0, 0, 10^-3);
% field_injector_1 = addLocalFieldOverwrite(field_emitter_3, field_injector_1); 
% field_injector_2 = importHDF5_CST([inDir '\injector_2.h5'], 'StaticE', 'injector_2', [0, 0, 0], scaling, 0, 0, 10^-3);
% field_injector_2 = addLocalFieldOverwrite(field_injector_1, field_injector_2); 
field_injector_3 = importHDF5_CST('C:\UN\GlassboxForFT\InjectorField\Export\3d\E-Field [Es].h5', 'StaticE', 'injector_3', [0, 0, 0], [1,1,1], 0, 0, 10^-3);
field_injector_3 = addLocalFieldOverwrite(field_emitter_3, field_injector_3);

%fieldbox=genZeroField([-1e-4,1e-4;-1e-4,1e-4;0,35e-3]);

%fieldList = {field_emitter_1, field_emitter_2, field_emitter_3, field_injector_1, field_injector_2, field_injector_3};
fieldList = {field_emitter_1,field_emitter_2, field_emitter_3,field_injector_3};

genSettings.saveFields = false;
genSettings.saveFieldInWindow = false; %TBT

% - - - - - VIS/KILL BOX - - - - - 
genSettings.killPartOutsideFields = false;
genSettings.killPartInKillBox=true;
genSettings.killBoxList ={[50e-6, Inf; -Inf,Inf; -Inf, Inf],[-Inf,-50e-6; -Inf,Inf; -Inf, Inf],...
                          [-Inf,Inf; 50e-6,Inf; -Inf, Inf], [-Inf,Inf; -Inf, -50e-6; -Inf, Inf],...
                            [-Inf, Inf;-Inf,Inf;-Inf,0],[-Inf, Inf;-Inf,Inf;35e-3,Inf] };


genSettings.visBoxList = {};

% - - - - -  ADAPTIVE WINDOW SETTINGS - - - - - 
window.mode = 'Adaptive';  % 'Adaptive', 'PreCalc', 'None'
window.updateRate = 1;
window.velMargin = 0;
window.margin = [2, 2; 2, 2; 2, 2] / 2;  % in indices (), min 1 in each
window.maxWindowSize = [inf, inf, inf];  % TBT

% - - - - - PLOTTING - - - - - 
plotOptions.plotOverTime = true;
plotOptions.energyIneV = true;


% - - - - - TIME SETTINGS - - - - -
timeInfo.dt = 1e-14;  % with adaptive time steps, this value is used outside of any field mesh
timeInfo.Nt = 55000;
genSettings.dt_update_res = 1;  % resolution of time step updates (give step width), applies to adaptive time steps

genSettings.showInfo = false;
genSettings.saveEverNStep = 10;
genSettings.solver = 'General_CInterpo';  %'General', 'General_CInterpo', 'HPC'

genSettings.killPartVelBased = false;
genSettings.killSlowParts = false;    % turn on/off vel based killing
genSettings.slowVelCutOffFactor = 0.65;     % cutOffVel = slowVelCutOffFactor * fastVel
genSettings.numberOfFastPart = 10;     % number of fastes particles contributing to mean fastVel
genSettings.axisNumber = 3;  


% ========================================================================= 
% Advanced Settings - better do not touch, when new

% time settings
% timeInfo.dtFunc = @ (phasespace, windowedField, dtVarargin) adaptiveTimeSteps(phasespace, windowedField, dtVarargin, 0.1); % adaptiveTimeSteps or equalTimeSteps;  % for General
timeInfo.dtFunc = @ (varargin) adaptiveTimeSteps_CFL(0.05, varargin); % adaptiveTimeSteps or equalTimeSteps;  % for General
% timeInfo.dtFunc = @equalTimeSteps;
timeInfo.dtVarargin = {timeInfo.dt};  % for General
% ========================================================================= 

% vis/ kil box stuff
plotOptions.visBoxList = genSettings.visBoxList;
plotOptions.killBoxList = genSettings.killBoxList;

plotOptions.solver = genSettings.solver;
if genSettings.doInteraction
    plotOptions.eeInter = genSettings.interactionType;
else
    plotOptions.eeInter = 'noInteraction';
end
plotOptions.modeWin = window.mode;


Scan.para1=1:2:23;%:12;%:20;%0.65;%0.56:0.02:0.72; %ConeFactor
Scan.para2=0.2;%0.2:0.05:0.45; %Initial energy spread [eV]

Scan.totalRuns=length(Scan.para1)*length(Scan.para2);
fprintf('TotalRuns: %d \n', Scan.totalRuns)
Scan.result=zeros(Scan.totalRuns,1);
run=0;
for i2=1:length(Scan.para2)
    for i1=1:length(Scan.para1)
        run=run+1;
        Scan.runs(run,1)=run;
        Scan.para1vec(run,1)=Scan.para1(i1);
        Scan.para2vec(run,1)=Scan.para2(i2);
    end
end
Scan.Sweeptable= table; % (runs, para1vec, para2vec, result  );
Scan.Sweeptable.runs=Scan.runs;
Scan.Sweeptable.para1vec=Scan.para1vec;
Scan.Sweeptable.para2vec=Scan.para2vec;
Scan.Sweeptable.result=Scan.result;

for run=1:Scan.totalRuns
    fprintf('Run: %d \n', run)

    N_p = Scan.Sweeptable.para1vec(run);  % e- per bunch
    N_b = 500;%round(10000/Scan.Sweeptable.para1vec(run));  % number of bunches (bunches do not interact with each other by definition)

    coneFactor=0.65;%Scan.Sweeptable.para1vec(run);
    sigma_E=0.2;%Scan.Sweeptable.para2vec(run);

    clear phasespace;
    clear bunchInfo;
    bunchInfo = genEqualParticleNumberDist(N_p, N_b);
    phasespace = genPhasespaceOnNanotip([outDir '\particles'], N_p * N_b, coneFactor, sigma_E);
    
    %% CHECK & FIX
    window.updateRate = abs(max(1, fix(window.updateRate)));
    genSettings.dt_update_res = abs(max(1, fix(genSettings.dt_update_res)));
    pass = checkAllInputs(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings, genSettings.showInfo);
    if pass, fprintf('INFO. Checks all passed.\n'); end
    
    %% SOLVER
    [simState, debugTimes] = runGeneralSolver_CInterpo(phasespace, bunchInfo, fieldList, window, timeInfo, genSettings);
    fprintf('INFO. run time: %g sec\n', toc(run_time));
    simStateArray{run}=simState;

    GLassboxScatterplot(simState,100+run)
    saveas(gcf,['./out/GlassboxScatter' num2str(run) '.png'])
end




%% PLOTTING
% close all
plotDebugTimes(debugTimes, plotOptions, 1);
% plotParticleDebug(simState, plotOptions, 2);
% plotTrajectories(simState, fieldList, plotOptions, 3);
%plotTrajectoriesXYZ(simState, fieldList, plotOptions, 4);
%plotBeamProps(simState, 5);
% plotGroupDist(bunchInfo, plotOptions, 6);
%plotKinEnergy(simState, 7);
plotDt(simState, plotOptions, 10);

%simStateReduced=simState(1:10:end);
%plotTrajectories(simStateReduced,111,false);

%GLassboxScatterplot(simState,222)

%GLassboxScatterplot(simStateArray{1},221)
%GLassboxScatterplot(simStateArray{end},222)
%saveas(gcf,'./out/GlassboxScatter6.png')

%simState6=simStateArray{end};
%save('./out/simState6.mat','simState6')

Final=GlassboxAveragesPlot(simStateArray, Scan, 301)

%saveas(gcf,'./out/GlassboxAverage.png')



fprintf('INFO. run time: %g sec\n', toc(run_time));
disp('FINISHED');